const mix = require('laravel-mix');
const webpack = require("webpack");
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/


mix.scripts([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/moment/min/moment-with-locales.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
        'node_modules/select2/dist/js/select2.min.js',
        'resources/assets/admin/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', //npm-de yoxdu
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'node_modules/datatables.net-responsive/js/dataTables.responsive.js',
        'node_modules/datatables.net-buttons/js/dataTables.buttons.min.js',
        'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
        'node_modules/datatables.net-responsive-bs/js/responsive.bootstrap.js',
        'node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
        'node_modules/datatables.net-rowreorder/js/dataTables.rowReorder.min.js',
        'node_modules/datatables.net-rowreorder-bs/js/rowReorder.bootstrap.min.js',
        'node_modules/fullcalendar/dist/fullcalendar.min.js',
        'node_modules/toastr/build/toastr.min.js'
        //'public/js/admin/config.js'
    ], 'public/js/app.js')

    .styles([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'resources/assets/admin/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', //npm-de yoxdu
        'node_modules/font-awesome/css/font-awesome.min.css',
        'node_modules/select2/dist/css/select2.min.css',
        'node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
        'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
        'node_modules/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
        'node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
        'node_modules/datatables.net-rowreorder-bs/css/rowReorder.bootstrap.min.css',
        'node_modules/fullcalendar/dist/fullcalendar.min.css',
        'node_modules/dropzone/dist/min/dropzone.min.css',
        'node_modules/toastr/build/toastr.min.css',
        'resources/assets/admin/dist/css/fonts.css',
        'resources/assets/admin/plugins/Ionicons/css/ionicons.min.css',
        'resources/assets/admin/morris.css',
        'resources/assets/admin/dist/css/AdminLTE.css',
        'resources/assets/admin/dist/css/custom.css'
    ], 'public/css/app.css')

    //************WEB*************//

    .styles([
        'resources/assets/css/fonts.css',
        'resources/assets/css/bootstrap.min.css',
        'resources/assets/css/libs.css',
        'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
        'node_modules/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
        'node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
        'resources/assets/css/intlTelInput-jquery.min.js',
        'node_modules/toastr/build/toastr.min.css',
        'node_modules/dropzone/dist/min/dropzone.min.css',
        'resources/assets/css/intlTelInput-jquery.min.css',
        'resources/assets/css/style.css',
        'resources/assets/css/responsive.css'
    ], 'public/css/style.css')

    .scripts([
        'resources/assets/js/jquery.min.js',
        'resources/assets/js/jquery-migrate-1.4.1.min.js',
        'resources/assets/js/jquery.magnific-popup.min.js',
        'resources/assets/js/jquery.equalheights.min.js',
        'resources/assets/js/jquery.jcarousel.min.js',
        'resources/assets/js/jcarousel.connected-carousels.js',
        'resources/assets/js/bootstrap.min.js',
        'resources/assets/js/owl.carousel.js',
        'resources/assets/js/validate.min.js',
        'resources/assets/js/jquery.maskedinput.js',
        //'resources/assets/js/intlTelInput-jquery.min.js',
        'node_modules/toastr/build/toastr.min.js',
        'resources/assets/js/site.js'
    ], 'public/js/script.js')

    //.copy('node_modules/datatables.net-buttons/js/dataTables.buttons.min.js', 'public/js/admin')
    //.copy('node_modules/datatables.net-buttons/js/buttons.print.min.js', 'public/js/admin')
    //.copy('resources/assets/admin/plugins/iCheck/icheck.min.js', 'public/js/admin/icheck.js')
    //.copy('resources/assets/admin/dist/js/adminlte.min.js', 'public/js/admin/adminlte.min.js')
    //.copy('resources/assets/admin/dist/js/demo.js', 'public/js/admin/demo.js')
    //.copy('resources/assets/admin/plugins/iCheck/square/blue.css', 'public/css/admin/blue.css')
    //.copy('node_modules/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css', 'public/css/admin/bootstrap-editable.css')
    //.copy('resources/assets/admin/dist/css/skins/skin-blue.min.css', 'public/css/admin/skin-blue.css')
    //.copy('resources/assets/admin/plugins/iCheck/square/blue@2x.png', 'public/css/admin')

    //.copy('node_modules/font-awesome/fonts/', 'public/fonts')
    //.copy('node_modules/bootstrap/fonts/', 'public/fonts')
    //.copy('resources/assets/admin/plugins/Ionicons/fonts/', 'public/fonts')

    .webpackConfig({
        plugins: [
            new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/)
        ]
    })
    .version();
