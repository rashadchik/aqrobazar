<?php

Auth::routes();

// patient routes
/*Route::prefix('user')->group(function (){
    Route::post('/logout', 'Auth\LoginController@logout')->name('user.logout');
});*/

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

// admin routes
Route::prefix('admin')->group(function (){
    Route::get('/login', 'Auth\AdminLoginController@showloginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
   // Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    // Password reset routes
    Route::prefix('password')->group(function (){
        Route::post('/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        Route::get('/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('/reset', 'Auth\AdminResetPasswordController@reset');
        Route::get('/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    });
});


//Route::get('/dp', function () {
//
//    $cars = \App\Lib\AbstractFactory\VehicleFactory::getVehicles('luxury');
//
//    return $cars->call();
//});

// site routes

Route::get('/payment/complete', 'PaymentController@completeCard');
Route::post('/sms/complete', 'PaymentController@completeSms');

Route::get('/payment/show/{ref}', 'PaymentController@show')->name('payment.show');

Route::post('file/upload', 'UserProductUploadController@upload')->name('file.upload');
Route::delete('file/delete', 'UserProductUploadController@deleteFile')->name('file.delete');
Route::get('file/get/{modelId}', 'UserProductUploadController@get')->name('file.get');

Route::get('/', 'Web\SiteController@index')->name('home');
Route::get('/all', 'Web\SiteController@all')->name('ads.all');
Route::get('/vip', 'Web\SiteController@vip')->name('vip.all');
Route::get('/search', 'Web\SearchController@search')->name('search');

Route::get('/stores', 'Web\StoreController@all')->name('store.all');
Route::get('/stores/{slug}', 'Web\StoreController@show')->name('store.show');
Route::get('/stores/{slug}/ads/{cat}', 'Web\StoreController@show')->name('store.showByCat');
Route::post('/stores/apply', 'Web\StoreController@postApply')->name('store.apply');

Route::resource('ads', 'UserProductController')->except(['destroy']);
Route::put('{id}/ads/delete', 'UserProductController@delete')->name('ads.delete');
Route::post('{id}/ads/confirm', 'UserProductController@confirm')->name('ads.confirm');

Route::get('ads/user/{id}', 'Web\SiteController@showUserProducts')->name('user.products');

Route::get('/favourite/{id}', 'PaymentController@paymentForm')->name('favourite.modal');
Route::get('/vip/{id}', 'PaymentController@paymentForm')->name('vip.modal');
Route::get('/premium/{id}', 'PaymentController@paymentForm')->name('premium.modal');

Route::resource('bookmarks', 'BookmarkController')->only(['store', 'index']);

Route::get('/banner/{id}','BannerController@show')->name('show.banner');

Route::get('/aqromarket', function () {
    return view('web.aqromarket.index');
});
Route::get('{slug1}/{slug2?}/{slug3?}/{slug4?}', 'Web\SiteController@showPage')->name('showPage');

Route::post('/contact', 'Web\SiteController@postContact')->name('web.contact');
Route::post('/subcategory', 'Web\SiteController@fetchSubCategory')->name('category.fetch');
Route::post('/city', 'CityController@getCitiesByCountry')->name('city.fetch');

Route::post('/pay', 'PaymentController@pay');





