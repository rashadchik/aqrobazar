<?php
use App\Logic\Breadcrumb;

Breadcrumbs::register('breadcrumb', function($breadcrumbs, $page, $homePageTitle, $data)
{
    $breadcrumbs->push($homePageTitle, route('home'));

    $breadcrumb = new Breadcrumb;

    if(!is_null($page)) {
        $bc = $breadcrumb->get($page);

        foreach ( $bc as $key => $item) {

            $breadcrumbs->push(title_case($item['name']), route("showPage", $item['slug']));
        }
    }

    if(!is_null($data)) {
        $breadcrumbs->push(title_case($data['title']));
    }

});


