<?php

Route::get('/', 'DashboardAdminController@index')->name('admin.dashboard');
Route::get('/profile', 'AdminController@profile')->name('admin.profile');
Route::put('updatePassword', 'AdminController@updatePassword')->name('admins.updatePassword');
Route::put('updatePhoto', 'AdminController@updatePhoto')->name('admins.updatePhoto');
Route::put('updateProfile', 'AdminController@updateProfile')->name('admins.updateProfile');

Route::resource('analytic', 'GoogleAnalyticsController')->only(['index', 'store']);

Route::resource('admins', 'AdminController')->except('destroy');
Route::group(['prefix'=>'admins'], function()
{
    Route::put('{id}/trash', 'AdminController@trash')->name('admins.trash');
    Route::put('{id}/restore', 'AdminController@restore')->name('admins.restore');
});

Route::resource('user', 'UserController')->except('destroy');
Route::group(['prefix'=>'users'], function()
{
    Route::put('{id}/trash', 'UserController@trash')->name('user.trash');
    Route::put('{id}/restore', 'UserController@restore')->name('user.restore');
    Route::put('{id}/fastUpdate', 'UserController@fastUpdate')->name('user.fastUpdate');
});

Route::resource('page', 'PageController', ['except' => ['show']]);
Route::group(['prefix'=>'page'], function()
{
    Route::get('order', 'PageController@order')->name('page.order');

    Route::post('storeTrans', 'PageController@storeTrans')->name('page.storeTrans');
    Route::post('postOrder', 'PageController@postOrder')->name('post.page.order');

    Route::put('{id}/updateSingle', 'PageController@updateSingle')->name('page.updateSingle');
    Route::put('{id}/updateTrans', 'PageController@updateTrans')->name('page.updateTrans');

    Route::put('{id}/trash', 'PageController@trash')->name('page.trash');
    Route::put('{id}/restore', 'PageController@restore')->name('page.restore');
});


Route::resource('articles', 'ArticleController')->except(['show']);
Route::group(['prefix'=>'articles'], function()
{
    Route::post('storeTrans', 'ArticleController@storeTrans')->name('articles.storeTrans');

    Route::put('{id}/updateSingle', 'ArticleController@updateSingle')->name('articles.updateSingle');
    Route::put('{id}/updateTrans', 'ArticleController@updateTrans')->name('articles.updateTrans');

    Route::put('{id}/trash', 'ArticleController@trash')->name('articles.trash');
    Route::put('{id}/restore', 'ArticleController@restore')->name('articles.restore');
});


Route::resource('product', 'ProductController')->except(['show', 'destroy']);
Route::group(['prefix'=>'product'], function()
{
    Route::put('{id}/updateSingle', 'ProductController@updateSingle')->name('product.updateSingle');
    Route::put('{id}/fastUpdate', 'ProductController@fastUpdate')->name('product.fastUpdate');
    Route::put('{id}/trash', 'ProductController@trash')->name('product.trash');
    Route::put('{id}/restore', 'ProductController@restore')->name('product.restore');

    Route::get('{id}/vip', 'ProductVipController@create')->name('vip.create');
    Route::get('{id}/vip/edit', 'ProductVipController@edit')->name('vip.edit');
    Route::post('{id}/vip', 'ProductVipController@store')->name('vip.store');
    Route::put('{id}/vip/update', 'ProductVipController@update')->name('vip.update');
    Route::delete('{id}/vip/destroy', 'ProductVipController@destroy')->name('vip.destroy');

    Route::get('{id}/fav', 'ProductFavouriteController@create')->name('fav.create');
    Route::get('{id}/fav/edit', 'ProductFavouriteController@edit')->name('fav.edit');
    Route::post('{id}/fav', 'ProductFavouriteController@store')->name('fav.store');
    Route::put('{id}/fav/update', 'ProductFavouriteController@update')->name('fav.update');
    Route::delete('{id}/fav/destroy', 'ProductFavouriteController@destroy')->name('fav.destroy');

});



Route::resource('product/category', 'ProductCategoryController')->except(['show']);
Route::group(['prefix'=>'product/category'], function()
{
    Route::post('storeTrans', 'ProductCategoryController@storeTrans')->name('category.storeTrans');

    Route::put('{id}/updateSingle', 'ProductCategoryController@updateSingle')->name('category.updateSingle');
    Route::put('{id}/updateTrans', 'ProductCategoryController@updateTrans')->name('category.updateTrans');

    Route::put('{id}/trash', 'ProductCategoryController@trash')->name('category.trash');
    Route::put('{id}/restore', 'ProductCategoryController@restore')->name('category.restore');
});

Route::resource('store', 'StoreController')->except(['show']);
Route::group(['prefix'=>'store'], function()
{
    Route::post('storeTrans', 'StoreController@storeTrans')->name('store.storeTrans');

    Route::put('{id}/trash', 'StoreController@trash')->name('store.trash');
    Route::put('{id}/restore', 'StoreController@restore')->name('store.restore');
    Route::put('{id}/updateTrans', 'StoreController@updateTrans')->name('store.updateTrans');
});

Route::resource('price', 'PriceListController')->except(['show']);
Route::resource('invoice', 'InvoiceController');

Route::resource('banner', 'BannerController');
Route::resource('country', 'CountryController', ['except' => ['show']]);
Route::group(['prefix'=>'country'], function()
{
    Route::put('{id}/trash', 'CountryController@trash')->name('country.trash');
    Route::put('{id}/restore', 'CountryController@restore')->name('country.restore');
});

Route::resource('city', 'CityController', ['except' => ['show']]);
Route::resource('faq', 'FaqController', ['except' => ['show']]);
Route::resource('slider', 'SliderController', ['except' => ['show']]);
Route::resource('legislation', 'LegislationController', ['except' => ['show']]);
Route::resource('block', 'BlockController', ['except' => ['show']]);

Route::group(['prefix'=>'slider'], function()
{
    Route::post('reorder', 'SliderController@reorder')->name('slider.reorder');
    Route::put('{id}/updateImage', 'SliderController@updateImage')->name('update_image_slider');
    Route::put('{id}/trash', 'SliderController@trash')->name('slider.trash');
    Route::put('{id}/restore', 'SliderController@restore')->name('slider.restore');
});

Route::resource('invoices', 'InvoiceController')->only(['index', 'show']);
Route::get('invoices/{id}/pdf', 'InvoiceController@pdf')->name('invoices.pdf');

Route::resource('post-terminal', 'PostTerminalController', ['except' => ['show', 'destroy']]);
Route::resource('partners', 'PartnerController');

Route::resource('{model}/{id}/gallery', 'GalleryController')->only(['index', 'store']);
Route::delete('gallery/{id}', 'GalleryController@destroy')->name('gallery.destroy');

Route::resource('subscribers', 'SubscriberController')->except(['edit']);
Route::resource('config', 'ConfigController', ['only' => ['index', 'edit', 'update']]);
Route::resource('dictionary', 'DictionaryController',['only' => ['index', 'edit', 'update', 'create', 'store']]);
Route::resource('sitemap', 'SitemapController')->only(['index', 'store']);


