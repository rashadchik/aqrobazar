<?php

use Illuminate\Http\Request;


// user routes
Route::get('/profile', 'Web\UserController@show')->name('user.profile');
Route::get('/stat', 'UserProductController@stat')->name('user.stat');
Route::get('/{type}', 'UserProductController@products')->name('product.list');

Route::put('/profile/update/{id}', 'Web\UserController@update')->name('user.update');
Route::post('/profile/passwordUpdate', 'Web\UserController@updatePassword')->name('user.password.update');