<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event liste ner mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\AddCreated' => [
            'App\Listeners\SendTelegramNotification',
            'App\Listeners\SendEmailToUser',
        ],
        'App\Events\AddModified' => [
            'App\Listeners\SendEmailToUserOnUpdate',
        ],
        'App\Events\AddModifiedByUser' => [     //from website
            'App\Listeners\TelegramNotificationOnUpdateByUser',
            'App\Listeners\SendEmailOnUpdateByUser',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
