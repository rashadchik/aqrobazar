<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Logic\Configs;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (env('APP_ENV') === 'production') {
            \URL::forceScheme('https');
        }


        Validator::extend('current_password_match', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, auth()->user()->password);
        });

        Validator::extend('images_count', function($attribute, $value, $parameters) {
            return ($parameters[1] <= $parameters[0]) ? true : false;
        });

        Validator::extend('price_list_amount', function($attribute, $value, $parameters, $validator) {
            if($validator->getData()['type'] == 1){
                return $value > 12 ? false : true;
            }
            else{
                return true;
            }

        });

        Configs::set();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
