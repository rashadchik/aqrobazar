<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\City;
use App\Models\Page;

class StoreCrud extends RenderCrud
{
    public function fields($action, $data = [] )
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Loqo",
                "db" => "file",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 500x500'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if(isset($data['data'])){
                        $img = '<div class="input-group"><img src="'.asset($data['data']->getFirstMedia('logo')->getUrl('sm')).'" style="max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Cover",
                "db" => "cover",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 1100x293'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if(isset($data['data']) && $data['data']->getFirstMedia('cover')){
                        $img = '<div class="input-group"><img src="'.asset($data['data']->getFirstMedia('cover')->getUrl('md')).'" style="max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Telefon",
                "db" => "phone",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Whatsapp",
                "db" => "whatsapp",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Şəhər",
                "db" => "city_id",
                "type" => "select",
                "data" => City::getCities(),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            /*[
                "label" => 'Geolokasiya',
                "db" => "location",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],*/
            [
                "label" => 'Ünvan',
                "db" => "address",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => 'İş saatı',
                "db" => "schedule",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => 'Kataloq',
                "db" => "catalog_link",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Balans",
                "db" => "balance",
                "type" => "number",
                "attr" => ['class'=>'form-control'],
                "design" => function($input){
                    $group_btn = '<span class="input-group-addon">AZN</span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
        ];

        if(!isset($data['lang'])){
            $paramFields = (new StoreTransCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}


