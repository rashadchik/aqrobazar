<?php

namespace App\Crud;

class PriceListCrud extends RenderCrud
{
    public function fields($action, $data = array() )
    {
        $fields = [
            [
                "label" => "Növ",
                "db" => "type",
                "type" => "select",
                "data" => trans('locale.price-list'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Məbləğ",
                "db" => "amount",
                "type" => "number",
                "attr" => ['class'=>'form-control', 'min' => 1, 'required'],
            ],
            [
                "label" => "Gün",
                "db" => "day",
                "type" => "number",
                "attr" => ['class'=>'form-control', 'min' => 1, 'max' => 99, 'required', 'title' => 'Saytda əks olunmayacaq. Gün sayı dəyişildikdə əvvəlki elanlara şamil olunmayacaq.'],
            ],
            [
                "label" => "Təsvir",
                "db" => "note",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required', 'title' => 'Saytda əks olunacaq. Məs: 5 gün'],
            ],
        ];

        return $this->render($fields, $action, $data);
    }

}


