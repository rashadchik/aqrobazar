<?php

namespace App\Crud;

use App\Models\CountryTr;

class CountryCrud extends RenderCrud
{
    private function words($catId)
    {
        return CountryTr::where('country_id', $catId)->pluck('name', 'lang');
    }

    public function fields($action, $data = [] )
    {
        $fields = [
            [
                "label" => 'Ölkə adı',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control'],
                "multiLang" => true,
                "rows" => isset($data['data']) ? $this->words($data['data']->id) : [],
                "multiRow" => true,
            ],
            [
                "label" => 'Ölkə kodu',
                "db" => "c_code",
                "type" => 'number',
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => 'Prefix',
                "db" => "prefix",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => 'Status',
                "db" => "published",
                "type" => 'select',
                "data" => config('config.status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
        ];

       return $this->render($fields, $action, $data);
    }
}