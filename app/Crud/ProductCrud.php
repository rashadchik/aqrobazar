<?php

namespace App\Crud;

use App\Models\City;
use App\Models\Page;

class ProductCrud extends RenderCrud
{
    private function categories()
    {
        $categories = Page::productCategories(app()->getLocale())
            ->whereNull('pages.parent_id')
            ->pluck('pt.name', 'pages.id');

        return $categories;
    }

    public function fields($action, $data = null, $lang = null)
    {
        $fields = [
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
                "edit" => false,
                "divClass" => "language-form"
            ],
            [
                "label" => 'Elan adı',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Kateqoriya",
                "db" => "main_category_id",
                "type" => "select",
                "data" => $this->categories(),
                "selected" => null,
                "attr" => ['class'=>'form-control category_list', 'data-dependent' => 'property']
            ],
            [
                "label" => "Alt kateqoriya",
                "db" => "category_id",
                "type" => "select",
                "data" => [],
                "selected" => null,
                "attr" => ['class'=>'form-control', 'id' => 'property', 'data-route' => route('category.fetch'), 'data-selected' => $data['data']->category_id ?? '', 'required']
            ],
            [
                "label" => "Xidmət növü",
                "db" => "type",
                "type" => "select",
                "data" => trans("locale.ad-type"),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Qısa məzmun",
                "db" => "summary",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5],
            ],
            [
                "label" => "Qiymət",
                "db" => "price",
                "type" => "number",
                "attr" => ['class'=>'form-control', 'min' => '0.01', 'max' => '10000000', 'step' => '0.01'],
            ],
            [
                "label" => "Əlaqədar Şəxs",
                "db" => "user_name",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "E-mail",
                "db" => "user_email",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Telefon",
                "db" => "user_phone",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Şəhər",
                "db" => "city_id",
                "type" => "select",
                "data" => City::getCities(),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Fb Link",
                "db" => "fb_link",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title' => 'Fb-da Like hesablanması üçün köhnə link'],
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => array_map(function ($arr) {return $arr['title'];}, trans("locale.product-status")),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);

    }
}


