<?php

namespace App\Crud;

use App\Models\CountryTr;

class CityCrud extends RenderCrud
{
    public function fields($action, $data = [] )
    {
        $fields = [
            [
                "label" => 'Ölkə',
                "db" => "country_id",
                "type" => 'select',
                "data" => CountryTr::getLists(),
                "selected" => null,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => 'Şəhər adı',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control'],
                "multiLang" => true
            ],

        ];

       return $this->render($fields, $action, $data);
    }
}