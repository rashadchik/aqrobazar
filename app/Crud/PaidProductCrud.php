<?php

namespace App\Crud;

class PaidProductCrud extends RenderCrud
{
    public function fields($action, $data = [])
    {
        $fields = [
            [
                "label" => "Müddət",
                "db" => "count_day",
                "type" => "number",
                "edit" => false,
                "attr" => ['class'=>'form-control', 'min' => 1, 'max' => 366]
            ],
            [
                "label" => "Başlama Tarixi",
                "db" => "start_date",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'readonly']
            ],
            [
                "label" => "Bitmə Tarixi",
                "db" => "end_date",
                "type" => "text",
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}


