<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class PageCrud extends RenderCrud
{
    private function category()
    {
        $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
            ->select('pages.id', 'page_translations.name')
            ->where('pages.template_id', '<>', 5)
            ->orderBy('page_translations.name', 'asc')
            ->groupBy('page_translations.page_id')
            ->pluck('page_translations.name', 'pages.id')
            ->prepend('---', '');

        return $query;
    }

    public function fields($action, $data = array() )
    {
        $fields = [
            [
                "label" => "Template",
                "db" => "template_id",
                "type" => "select",
                "data" => config('config.template'),
                "selected" => 0,
                "attr" => ['class'=>'form-control', 'id' => 'template_id'],
            ],

            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category(),
                "selected" => null,
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => "Görünüş",
                "db" => "visible",
                "type" => "select",
                "data" => config('config.menu-visibility'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Linkin quruluşu",
                "db" => "target",
                "type" => "select",
                "data" => config('config.menu-target'),
                "selected" => 1,
                "hide" => [2],
                "attr" => ['class'=>'form-control'],
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}


