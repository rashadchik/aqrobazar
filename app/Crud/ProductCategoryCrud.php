<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class ProductCategoryCrud extends RenderCrud
{

    private function category()
    {
        $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
            ->select('pages.id', 'page_translations.name')
            ->orderBy('page_translations.name', 'asc')
            ->groupBy('page_translations.page_id')
            ->pluck('page_translations.name', 'pages.id')
            ->prepend('---', '');

        return $query;
    }

    public function fields($action, $data = array() )
    {
        $fields = [
            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category(),
                "selected" => null,
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => "Şəkil",
                "db" => "file",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 150x150'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if(isset($data['data']) && $data['data']->getFirstMediaUrl('icon')){
                        $img = '<div class="input-group"><img src="'.asset($data['data']->getFirstMediaUrl('icon')).'" style="min-width:150px; max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Görünüş",
                "db" => "visible",
                "type" => "select",
                "data" => config('config.menu-visibility-boolean'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}


