<?php

namespace App\Crud;


class StoreTransCrud extends RenderCrud
{
    public function fields($action, $data = [] )
    {
        $fields = [
            [
                "label" => "Qısa məlumat",
                "db" => "summary",
                "type" => "textarea",
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);

    }
}


