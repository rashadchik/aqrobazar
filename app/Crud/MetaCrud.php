<?php

namespace App\Crud;


class MetaCrud
{
    public function get($data = array())
    {
        $fields = [
            [
                "label" => "Meta description",
                "db" => "meta_description",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək']
            ],
            [
                "label" => "Meta keywords",
                "db" => "meta_keywords[]",
                "type" => "select",
                "data" => [],
                "selected" => null,
                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords'.@$data['lang'], 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
            ],
        ];

        return $fields;
    }
}


