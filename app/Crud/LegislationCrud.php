<?php

namespace App\Crud;

class LegislationCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Başlıq",
                "db" => "title",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 3, 'required'],
            ],
            [
                "label" => "PDF",
                "db" => "file",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>';

                    return '<div class="input-group">'.$group_btn.'</div>';
                },
            ],
        ];

        return $this->render($fields, $action, $data);
    }

}


