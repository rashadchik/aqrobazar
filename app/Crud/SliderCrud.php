<?php

namespace App\Crud;


class SliderCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Başlıq',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 1920x480'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $img = '<div class="input-group"><img src="'.asset("storage/$data->image").'" style="max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => 'Qısa məzmun',
                "db" => "summary",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Link",
                "db" => "link",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
                "design" => function($input, $data){
                    $group_btn =
                        '<label class="input-group-addon">
                        http://
                    </label>';

                    return '<div class="input-group">'.$group_btn.$input.'</div>';
                }
            ],
        ];
        return $this->render($fields, $action, $data);
    }
}


