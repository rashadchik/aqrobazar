<?php

namespace App\Crud;

use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class ArticletransCrud extends RenderCrud
{
    private function category($lang)
    {
        if(is_null($lang )){

            $select = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->whereIn('pages.template_id', [4])
                ->select("pt.id", "pt.name", 'pt.lang')
                ->orderBy("pt.name", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{

            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereIn('pages.template_id', [4])
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');
        }


        return $query;
    }

    public function fields($action, $data = [] )
    {
        $fields = [
            [
                "label" => 'Başlıq',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
                "edit" => false,
                "divClass" => "language-form"
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Kateqoriya",
                "db" => "page_id",
                "type" => "select",
                "data" => $this->category(!isset($data['lang']) ? null : $data['lang']),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Qısa məzmun",
                "db" => "summary",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5, 'required'],
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.@$data['lang']]
            ],

        ];

        if(!isset($data['lang'])){
            $paramFields = (new ArticleParameterCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}


