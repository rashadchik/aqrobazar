<?php

namespace App\Crud;

use App\Models\Store;

class BannerCrud extends RenderCrud
{
    public function fields($action, $data = [])
    {
        $fields = [
            [
                "label" => "Başlıq",
                "db" => "title",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Bannerin növü",
                "db" => "type",
                "type" => "select",
                "data" => config('config.banner-type'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Device",
                "db" => "device",
                "type" => "select",
                "data" => config('config.banner-devices'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Mağaza",
                "db" => "store_id",
                "type" => "select",
                "data" => Store::lists(),
                "selected" => '',
                "attr" => ['class'=>'form-control', 'placeholder' => '---']
            ],
            [
                "label" => "Banner",
                "db" => "file",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>';
                }
            ],
            [
                "label" => "Iframe uzunluğu",
                "db" => "width",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title' => 'px və ya %-lə yazın.'],
            ],
            [
                "label" => "Iframe hündürlüyü",
                "db" => "height",
                "type" => "number",
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Link",
                "db" => "link",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
                "design" => function($input){
                    $group_btn = '<span class="input-group-addon">https://</span>';

                    return '<div class="input-group">'.$group_btn.$input.'</div>';
                }
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}