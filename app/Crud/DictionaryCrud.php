<?php

namespace App\Crud;


use App\Models\Dictionary;

class DictionaryCrud extends RenderCrud
{
    private function words($word)
    {
        return Dictionary::where('keyword', $word)->pluck('content', 'lang_id');
    }

    public function fields($action, $data = array() )
    {
        $fields = [
            [
                "label" => "Açar söz",
                "db" => "keyword",
                "type" => "text",
                "edit" => false,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Mətn',
                "db" => "content",
                "type" => 'textarea',
                "attr" => ['class'=>'form-control', 'rows' => 3],
                "multiRow" => $action == 'edit' ? $this->words($data['data']->keyword) : false,
                "multiLang" => true
            ],
            [
                "label" => "Editor",
                "db" => "editor",
                "type" => "checkbox",
                "value" => 1,
                "checked" => false,
                "edit" => false,
                "attr" => []
            ],
        ];
        return $this->render($fields, $action, $data);
    }
}


