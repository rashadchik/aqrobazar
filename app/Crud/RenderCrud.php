<?php

namespace App\Crud;

use Collective\Html\FormFacade;

abstract class RenderCrud
{
    protected function render($fields, $action, $data)
    {
        if(isset($data['meta']) && $data['meta'] == true){
            $metaFields = (new MetaCrud)->get($data);
            $fields = array_merge($fields, $metaFields);
        }

        if($action == 'show'){
            return $this->show($fields, $action, $data);
        }
        elseif($action == 'profile'){
            return $this->profile($fields, $action, $data);
        }
        elseif($action == 'get'){
            //gets field as array
            return $fields;
        }
        else{
            return $this->form($fields, $action, $data);
        }
    }


    private function renderForm($field, $data, $multiLang = null)
    {
        if(isset($data['disabled'])){
            $field['attr']['disabled'] = 'disabled';
        }

        if($field['type'] == 'select'){
            if(isset($field['attr']['multiple'])){
                $form = FormFacade::select($field['db'], $field['data'], $field['selected'], $field['attr']);
            }
            else{
                $form = FormFacade::select($field['db'], $field['data'], isset($field['forceSelected']) ? $field['forceSelected'] : (!isset($data['data']) ? $field['selected'] : old($field['db'], $data['data']->{$field['db']})), $field['attr']);
            }
        }
        else if($field['type'] == 'file'){
            $form = FormFacade::file($field['db'], $field['attr']);
        }
        else if($field['type'] == 'checkbox'){
            $form = FormFacade::checkbox($field['db'], $field['value'], !isset($data['data'])? false : $data['data']->{$field['db']} , $field['attr']);
        }
        else if($field['type'] == 'password'){
            $form = FormFacade::password($field['db'], $field['attr']);
        }
        else if($field['type'] == 'hidden'){
            $form = FormFacade::hidden($field['db'], $field['value'], $field['attr']);
        }
        else{
            if(isset($field['multiLang']) && $field['multiLang'] == true){

                if(isset($field['multiRow']) && $field['multiRow'] == true){
                    $form = FormFacade::{$field['type']}($field['db'].'['.$multiLang.']', $field['rows'][$multiLang] ?? '', $field['attr']);
                }
                else{
                    $form = FormFacade::{$field['type']}($field['db'].'['.$multiLang.']', !isset($data['data']) ? null : old($field['db'], $data['data']->{$field['db'].'_'.$multiLang}), $field['attr']);
                }
            }
            else{
                $form = FormFacade::{$field['type']}($field['db'], !isset($data['data']) ? null : old($field['db'], $data['data']->{$field['db']}), $field['attr']);
            }
        }

        if(isset($field['design'])){
            $render = call_user_func($field['design'], $form, $data);
        }
        else{
            $render = $form;
        }

        return $render;
    }


    private function show($fields, $action, $data)
    {
        $html = '';

        foreach ($fields as $field){

            $html.='<tr>
                        <td><label for="">'.$field['label'].'</label> </td>
                        <td>'.$data[$field['db']].'</td>
                </tr>';

        }

        return $html;
    }


    private function profile($fields, $action, $data)
    {
        $html = '';

        foreach ($fields as $field)
        {
            if(!isset($field[$action]) || $field[$action] == true){
                $label = '<label for="'.$field['db'].'" class="col-sm-2 control-label">'.$field['label'].'</label>';
                $input = $this->renderForm($field, $data);

                $html.='<div class="form-group">'.$label.'<div class="col-sm-10">'.$input.'</div></div>';
            }
        }

        return $html;
    }


    private function form($fields, $action, $data)
    {
        $output = '';

        foreach ($fields as $field){

            if(!isset($field[$action]) || $field[$action] == true){

                if(isset($field['multiLang']) && $field['multiLang'] == true){
                    $output .= $this->buildNavTab($field, $data);
                }
                else{
                    $form = $this->renderForm($field, $data);
                    $output .= $this->build($field, $form);
                }
            }
        }

        return $output;
    }


    private function build($field, $form)
    {
        $class = 'form-group';
        $class2 = 'col-md-8';
        $class3 = 'col-md-3 control-label';

        $output = '<div class="'.$class.' '.@$field['divClass'].' hh'.@implode(' hh', $field['hide']).'">';

        $label = FormFacade::label($field['db'], $field['label'], ['class' => $class3]);

        $output.=$label;

        $output.='<div class="'.$class2.'">';

        $output.=$form;

        if(isset($field['attr']['title'])){
            $output.='<p class="help-block">'.$field['attr']['title'].'</p>';
        }

        $output.='</div></div>';

        return $output;
    }


    private function buildNavTab($field, $data)
    {
        $multiLangOutput = '<ul class="nav nav-tabs">';

        foreach (config('app.locales') as $key => $locale){

            $key == 'az' ? $activeTab = 'active' : $activeTab = '';

            $multiLangOutput .= '<li class="'.$activeTab.'"><a href="#'.$key.$field['db'].'" data-toggle="tab"> '.$key.'</a></li>';
        }

        $multiLangOutput .= '</ul><div class="tab-content" style="margin-top:15px">';

        foreach (config('app.locales') as $key => $locale){

            $key == 'az' ? $activeTab = 'in active' : $activeTab = '';

            $form = $this->renderForm($field, $data, $key);
            $output = $this->build($field, $form);

            $multiLangOutput .= '<div id="'.$key.$field['db'].'" class="tab-pane fade '.$activeTab.'">'.$output.'</div>';
        }

        $multiLangOutput .= '</div>';

        return $multiLangOutput;
    }
}