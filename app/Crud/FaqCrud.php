<?php

namespace App\Crud;

class FaqCrud extends RenderCrud
{
    public function fields($action, $data = array() )
    {
        $fields = [
            [
                "label" => "Sual",
                "db" => "title",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Cavab",
                "db" => "text",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5, 'required'],
            ],
        ];

        return $this->render($fields, $action, $data);
    }

}


