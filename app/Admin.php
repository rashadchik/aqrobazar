<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
class Admin extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static function rules($id = null){
        is_null($id) ? $required = 'required|' : $required = null;
        return [
            'name' => 'required',
            'email' => 'required|email|unique:admins,email,'.$id,
            'password' => $required.'min:6',
        ];
    }


    public static $messages = [
        'required' => 'Bütün xanaların doldurulması zəruridir!',
    ];

}
