<?php

namespace App\Logic;

use App\Models\Country;
use App\Models\Dictionary;
use App\Models\Page;

trait Share
{
    public $lang, $config, $menu, $device;

    protected function loadData()
    {
        $deviceType = array_first(explode('.', request()->getHost()));

        $deviceType == 'm' ? $device = 'mobile' : $device = $deviceType;

        $webCache = new WebCache;
        $menu = new Menu;

        $this->lang = app()->getLocale();
        $this->menu = $menu;
        $this->config = getConfig();
        $this->device = $device;
        $productCategories = Page::mainCategories($this->lang);
        $productCategoriesTotal = [];

        foreach ($productCategories as $pc) {
            $num = $pc->products_count;

            foreach ($productCategories as $child) {
                if($child->parent_id == $pc->id) {
                    $num = $num + $child->products_count;
                }
            }

            $productCategoriesTotal[$pc->id] = $num;
        }


        view()->share('topMenu', Menu::topMenu());
        view()->share('footerMenu', Menu::footerMenu());
        view()->share('mobileMenu', Menu::mobileMenu($productCategories));
        view()->share('mainCategories', $productCategories);
        view()->share('productCategoriesTotal', $productCategoriesTotal);
        view()->share('device', $device);
        view()->share('config', $this->config);
        view()->share('dictionary', Dictionary::getLists(['lang_id' => $this->lang]));
        view()->share('partners', $webCache->getPartner());
        view()->share('lang', $this->lang);
        view()->share('countries', $webCache->getCountries());
        view()->share('countryCodes', Country::getCountryCodes());

    }
}
