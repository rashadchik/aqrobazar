<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\Page;
use Cache;
use Spatie\Menu\Link;
use Spatie\Menu\Menu as SpatieMenu;
use Spatie\Menu\Html;
class Menu
{

    public static function all($lang = null)
    {
        if(is_null($lang)){
            $lang = app()->getLocale();
        }

        $select = ['pages.visible', 'pages.target', 'pages.template_id', 'pages.parent_id', 'pt.id', 'pt.page_id', 'pt.name', 'pt.slug', 'pt.lang'];

        $menu = Cache::remember('pages_'.$lang, 1440, function () use($select, $lang) {
            return Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
                ->where('pt.lang', $lang)
                ->whereNull('pt.deleted_at')
                ->orderBy('pages.order', 'asc')
                ->select($select)
                ->withCount('menuChildren')
                ->get();
        });

        return $menu;
    }


    protected function nestedBuild($pages, $thisPage, $nestedSlug = false)
    {
        $nested = [];

        foreach ($pages as $page){
            if($page->id == $thisPage->parent_id){
                $nested = $this->nestedBuild($pages, $page, $nestedSlug);
            }
        }

        if($nestedSlug == false){
            $nested[$thisPage->slug] = ['breadcrumb' => ['slug' => $thisPage->slug, 'name' => $thisPage->name], 'data' => $thisPage];
        }
        else{
            $nested[] = $thisPage->slug;
        }


        return $nested;
    }


    public function getData($thisPage, $splice)
    {
        $pages = $this->all();
        $nested = $this->nestedBuild($pages, $thisPage);

        $data = array_splice($nested, $splice);

        $get = array_first($data);

        return $get['data'];
    }


    /**
     * sehifenin diger diller uchun URL-u: meselen: site.com/haqqimizda, site.com/en/about
     * Hemchinin parent-id-si null olmayan sehifenin URL-larini da nezere alir: site.com/biz-kimik bu formada generasiya olunur: site.com/haqqimizda/biz-kimik, site.com/en/about/who-are-we
     */

    public function relatedPages($thisPage, $join = false)
    {
        $relatedUrl = [];

        if(count(config('app.locales')) > 1){

            $related = $thisPage->relatedPages;

            foreach ($related as $rel){
                $pages = $this->all($rel->lang);
                $url = $rel->lang.'/'.implode('/', $this->nestedBuild($pages, $rel, true));

                $relatedUrl[$rel->lang] = $url;
            }

            if($join == true){
                $joinRelated = $join->relatedPages;

                foreach ($joinRelated as $jrel){
                    if(isset($relatedUrl[$jrel->lang])){
                        $relatedUrl[$jrel->lang] = $relatedUrl[$jrel->lang].'/'.$jrel->slug;
                    }
                }
            }
        }

        return $relatedUrl;
    }


    /**
     * axtarish ve sitemap-de istifade olunacaq. URL-u full nested formada generasiya edecek: site.com/biz-kimik => site.com/haqqimizda/biz-kimik
     * Menyularda da istifade oluna biler, eger ehtiyac olarsa, menyularda adeten birbasha sonuncu slug gelir, meselen: site.com/biz-kimik
     */

    public static function fullNestedSlug($parentId, $slug, $join = null)
    {
        $nested = [];
        $pages = self::all();

        foreach ($pages as $page){
            if($page->id == $parentId){
                $nested = (new self())->nestedBuild($pages, $page, true);
            }
        }

        if ($slug) {
            $nested[] = $slug;
        }

        if(!is_null($join)){
            $nested[] = $join;
        }

        $url = implode('/', $nested);

        return url($url);
    }


    public static function mobileMenu($productCategories = null)
    {
        if(is_null($productCategories)) {
            $productLogic = new ProductsList();
            $productCategories = $productLogic->mainCategories(app()->getLocale());
        }


        $mobileMenu = SpatieMenu::build($productCategories, function ($menu, $data) use($productCategories) {
            $menu->addClass('list-unstyled');

            if(is_null($data->parent_id)) {

                if($data->menu_children_count > 0) {

                    $menu->submenu(Html::raw('<a href="#">'.$data->name.'</a>'), function ($menu) use($productCategories, $data) {
                        $menu->addClass('list-unstyled clearfix');

                        if(in_array($data->id, Page::subCategories)) {
                            foreach ($productCategories as $child) {
                                if($child->parent_id == $data->id) {
                                    $menu->addParentClass('submenu');

                                    if($child->menu_children_count > 0) {
                                        $menu->submenu(Link::to('#', $child->name), function ($menu) use($productCategories, $child) {
                                            $menu->addClass('list-unstyled');

                                            $menu->add(Link::to(route('showPage', [$child->slug]), 'Hamısı'));

                                            foreach ($productCategories as $subChild) {
                                                if($subChild->parent_id == $child->id) {
                                                    $menu->add(Link::to(route('showPage', [$child->slug, $subChild->slug]), $subChild->name.'<span class="count">'.$subChild->products_count.'</span>'))->addParentClass('sub');
                                                }
                                            }
                                            $menu->add(Link::to(route('showPage', [$child->slug, 'other']), 'Digər<span class="count">'.$child->products_count.'</span>'))->addParentClass('sub');
                                        });
                                    }
                                    else {
                                        if($child->parent_id == $data->id) {
                                            $menu->add(Link::to(route('showPage', [$data->slug, $child->slug]), $child->name.'<span class="count">'.$child->products_count.'</span>'))->addParentClass('submenu');
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            foreach ($productCategories as $child) {
                                if($child->parent_id == $data->id) {
                                    $menu->add(Link::to(route('showPage', [$data->slug, $child->slug]), $child->name.'<span class="count">'.$child->products_count.'</span>'))->addParentClass('submenu');
                                }
                            }
                            $menu->add(Link::to(route('showPage', [$data->slug, 'other']), 'Digər<span class="count">'.$data->products_count.'</span>'))->addParentClass('submenu');
                        }
                    });
                }
                else {
                    $menu->add(Menu::renderLink($data));
                }
            }

            $menu->wrap('div', ['class' => 'mobile_category_body']);
        });

        return $mobileMenu;

    }


    public static function topMenu()
    {
        $menuItems = self::all();

        $topMenu = SpatieMenu::build($menuItems, function ($menu, $data) {
            $menu->withoutWrapperTag();
            $menu->withoutParentTag();

            if($data->template_id != 5 && !in_array($data->visible, [0, 3]) && is_null($data->parent_id)) {
                $menu->add(Menu::renderLink($data));
            }

            $menu->wrap('nav', ['class' => 'topmenu']);
        });

        return $topMenu;
    }


    public static function footerMenu()
    {
        $menuItems = self::all();

        $footerMenu = SpatieMenu::build($menuItems, function ($menu, $data) {
            $menu->withoutWrapperTag();
            $menu->withoutParentTag();

            if($data->template_id != 5 && !in_array($data->visible, [0, 2] ) && is_null($data->parent_id)) {
                $menu->add(Menu::renderLink($data));
            }

            $menu->wrap('nav', ['class' => 'footer_menu']);
        });

        return $footerMenu;
    }


    public static function renderLink($data)
    {
        return Link::to(route('showPage', $data->slug), $data->name)->setAttribute('target', $data->target == 0 ? '_blank' : '_self');
    }
}