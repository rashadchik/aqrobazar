<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/5/19
 * Time: 00:46
 */

namespace App\Logic;

class Millikart
{
    private $mid			= "aqrobazar";
    private $secretkey		= "O8EBC0AQFLLETAUI05HXOZCUKI2989W5";
    private $currency		= "944"; //AZN
    private $language	    = "az";
    private $test_url		= "https://test.millikart.az:7444";
    private $pro_url		= "https://pay.millikart.az";
    private $status;
    public 	$description;
    public 	$amount;
    public 	$reference;


    public function __construct()
    {
        $this->status = config('app.env');
    }


    private function signature()
    {
        $data = strlen($this->mid);
        $data .= $this->mid;
        $data .= strlen($this->amount);
        $data .= $this->amount;
        $data .= strlen($this->currency);
        $data .= $this->currency;
        if(!empty($this->description)) {
            $data .= strlen($this->description);
            $data .= $this->description;
        }
        else{
            $data .= "0";
        }

        $data .= strlen($this->reference);
        $data .= $this->reference;
        $data .= strlen($this->language);
        $data .= $this->language;
        $data .= $this->secretkey;
        $data = md5($data);
        $data = strtoupper($data);
        return $data;
    }


    public function getURL($amount, $reference, $description)
    {
        $this->amount = $amount*100;
        $this->description = $description;
        $this->reference = $reference;

        $api_url ="/gateway/payment/register?mid=".$this->mid."&amount=".$this->amount."&currency=".$this->currency."&description=".$this->description."&reference=".$this->reference."&language=".$this->language."&signature=".$this->signature();

        $url = $this->url($api_url);

        return $url.'&redirect=1';
    }


    public function request($reference)
    {
        $api_url = "/gateway/payment/status?mid=".$this->mid."&reference=".$reference;

        $url = $this->url($api_url);

        return $url;
    }


    private function url($url)
    {
        if($this->status == "local") {
            $url = $this->test_url.$url;
        }
        else {
            $url = $this->pro_url.$url;
        }

        return $url;
    }
}