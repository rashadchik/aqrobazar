<?php

namespace App\Logic;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Block;
use App\Models\Cart;
use App\Models\Legislation;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\PriceList;
use App\Models\Product;
use App\Models\Question;
use App\Models\Vacancy;
use DB;

trait Pages
{
    protected function about($getPage, $relatedPages)
    {
        $blocks = Block::orderBy('id', 'asc')->get();

        $banners = Banner::fetch($this->device);

        return view("web.about", ['page' => $getPage, 'pageTitle' => $getPage->name, 'blocks' => $blocks, 'banners' => $banners]);
    }


    protected function staticPage($getPage, $relatedPages)
    {
        $banners = Banner::fetch($this->device);
        return view("web.static", ['page' => $getPage, 'relatedPages' => $relatedPages, 'banners' => $banners]);
    }


    protected function news($getPage, $relatedPages)
    {
        $articles = $this->articles(16);

        if (request()->ajax()) {

            $view = view('web.elements.news-grid', ['page' => $getPage, 'articles' => $articles] )->render();
            return response()->json(['html'=>$view]);
        }

        $banners = Banner::fetch($this->device);

        return view("web.news", ['articles' => $articles, 'page' => $getPage, 'relatedPages' => $relatedPages, 'banners' => $banners]);
    }


    protected function articleSingle($checkPages, $slug)
    {
        $category = $checkPages->first();

        $getArticle = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->where('at.slug', $slug)
            ->whereNull('at.deleted_at')
            ->firstOrFail();

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getArticle);
        $banners = Banner::fetch($this->device);

        return view('web.article-single', ['article' => $getArticle, 'page' => $category, 'relatedPages' => $relatedPages, 'banners' => $banners]);
    }


    protected function products($getPage, $relatedPages, $other = false)
    {
        $mainCategories = Page::mainCategories($this->lang);


        $children = Page::where('parent_id', $getPage->id)->orWhere('id', $getPage->id)->pluck('id')->toArray();

        $vipProducts = Product::get($this->lang,1, 2)
            ->where('is_vip', 1)
            ->byCategories($children, $getPage->id, $other)
            ->limit(16)
            ->get();

        $products = Product::get($this->lang)
            ->where('is_vip', 0)
            ->byCategories($children, $getPage->id, $other)
            ->simplePaginate(32);


        if (request()->ajax()) {
            $view = view('web.elements.products-ajax', ['products' => $products] )->render();
            return response()->json(['html'=>$view]);
        }

        $banners = Banner::fetch($this->device);

        return view("web.products", ['other' => $other, 'products' => $products, 'page' => $getPage, 'relatedPages' => $relatedPages, 'mainCategories' => $mainCategories, 'vipProducts' => $vipProducts, 'banners' => $banners]);
    }


    protected function productSingle($checkPages, $slug)
    {
        $category = $checkPages->last();

        $cat = $this->menu->getData($category, 0);

        $product = Product::where('slug', $slug)->where('status', '<>', 100)->firstOrFail();

        $nestedSlug = Menu::fullNestedSlug($product->category->id, null, $product->slug);

        if ($nestedSlug != request()->url()) {
            return redirect()->to($nestedSlug);
        }

        $similarProducts = Product::get($this->lang)
            ->where('products.category_id', $product->category_id)
            ->where('products.id', '<>', $product->id)
            ->limit(12)
            ->get();

        Product::where('id', $product->id)->update(['seen' => DB::raw('seen + 1')]);

        $minAmount = PriceList::groupBy('type')->pluck('amount', 'type');

        $banners = Banner::fetch($this->device);

        $data = [
            'product' => $product,
            'cat' => $cat,
            'page' => $category,
            'similarProducts' => $similarProducts,
            'minAmount' => $minAmount,
            'banners' => $banners,
            'fbLink' => $product->fb_link ?? str_replace('m.', '', request()->url())
        ];

        return view('web.product-single', $data);
    }


    public function vacancies($page, $relatedPages){
        $vacancies = Vacancy::where('lang',$this->lang)->get();
        $banners = Banner::fetch($this->device);

        return view("web.vacancies", ['page' => $page,'vacancies' => $vacancies, 'banners' => $banners]);
    }


    protected function contact($getPage, $relatedPages)
    {
        $banners = Banner::fetch($this->device);

        return view("web.contact", ['page' => $getPage, 'banners' => $banners]);
    }


    protected function faq($getPage, $relatedPages)
    {
        $faq = Question::orderBy('id', 'desc')->limit(50)->get();
        $banners = Banner::fetch($this->device);

        return view("web.faq", ['page' => $getPage, 'faq' => $faq, 'relatedPages' => $relatedPages, 'banners' => $banners]);
    }


    protected function legislation($getPage, $relatedPages)
    {
        $legislation = Legislation::orderBy('id', 'desc')->limit(50)->get();
        $banners = Banner::fetch($this->device);

        return view("web.legislation", ['page' => $getPage, 'legislation' => $legislation, 'relatedPages' => $relatedPages, 'banners' => $banners]);
    }





    protected function getPageByTemplateId($templateId)
    {
        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.lang', $this->lang)
            ->where('pages.template_id', $templateId)
            ->whereNull('pt.deleted_at')
            ->select('pt.slug', 'pt.name', 'pt.id')
            ->first();

        return $page;
    }


    private function getChildrenPage($page)
    {
        $children = $page->children;

        if($children->count() && $page->id != $children->first()->id){

            return $children->first();
        }
        else{
            return $page;
        }
    }



    private function articles($paginate)
    {
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->select('at.title', 'at.slug', 'at.summary', 'articles.id', 'articles.published_at')
            ->where('at.lang', $this->lang)
            ->where('articles.status', 1)
            ->whereNull('at.deleted_at')
            ->with('media')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->paginate($paginate);

        return $articles;
    }
}
