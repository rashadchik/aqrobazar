<?php

namespace App\Logic;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class Dropzone extends ImageRepo
{
    public function upload($data, $file, $collection)
    {
        $limit = 30;

        $rules = [
            'file' => 'required|mimes:png,gif,jpeg,jpg|max:10000'
        ];

        $messages = [
            'file.mimes' => 'Şəkil :values formatda yüklənməlidir',
        ];


        $validator = Validator::make(request()->all(), $rules, $messages);

        if ($validator->fails()) {

            $response = ['error' => true, 'message' => $validator->messages()->first(), 'code' => 400];

            return  Response::json($response, 400);
        }

        $count = $data->getMedia('gallery')->count();

        if($count >= $limit){
            $response = ['error' => true, 'message' => 'Albom üçün şəkil limiti: '.$limit, 'code' => 400];

            return  Response::json($response, 400);
        }

        $media = $data
            ->addMedia($file)
            ->toMediaCollection($collection);

        $response = ['error' => false, 'message' => 'Success', 'code' => 200, 'deleteRoute' => route('gallery.destroy', $media->id)];

        return Response::json($response, $response['code']);
    }
}