<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\City;
use App\Models\Config;
use App\Models\Country;
use App\Models\Dictionary;
use App\Models\Slider;
use App\Models\Partner;
use DB;
use Cache;

class WebCache
{

    public function getCountries()
    {
        $countries = Cache::rememberForever("countries", function () {
            return Country::with('tr', 'cities')
                ->select('id')
                ->get();
        });

        return $countries;
    }

    public function getPartner()
    {
        $partners = Cache::rememberForever("partners", function () {
            return Partner::orderBy('order', 'desc')->limit(40)->get();
        });

        return $partners;
    }

    public static function config()
    {
        $config = Cache::rememberForever('config', function () {
               return Config::all();
        });

        return $config;
    }
}