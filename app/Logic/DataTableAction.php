<?php

namespace App\Logic;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Hash;

trait DataTableAction
{
    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => "Yeni $this->name", 'route' => "$this->route.store", 'editor' => $this->editor ?? false, 'meta' => $this->meta ?? false]);
    }


    public function store(Request $request)
    {
        $model = $this->model;

        // check Validation
        $validation = Validator::make($request->all(), $this->rules($model), $model::$messages);
        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        if($request->has('password'))
        {
            $this->requests['password'] = Hash::make($request->password);
        }

        $created = $model::create($this->requests);

        if($request->hasFile('file')){

            $created->addMedia($request->file)
                ->toMediaCollection();
        }

        return $this->responseJson(0, "$this->name əlavə edildi", $this->route, "myModal");
    }


    public function edit($id)
    {
        $data = $this->model->findOrFail($id);

        $fields = $this->crud->fields('edit', ['data' => $data]);

        return view('admin.dt.edit', ['title' => $data->name ?? $data->title ?? 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ["$this->route.update", $id] ]);
    }


    public function update(Request $request, $id)
    {
        $model = $this->model;

        $data = $model->findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(), $this->rules($model, $id), $model::$messages);
        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        DB::beginTransaction();


        try{
            foreach($this->requests as $key => $put){
                $data->$key = $put;
            }

            $data->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        if($request->has('file')){

            try{
                $data->clearMediaCollection();  //delete old file
                $data->addMedia($request->file)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->responseJson(1, $e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }

    public function trash($id)
    {
        $data = $this->model->findOrFail($id);
        $data->delete();

        return $this->responseJson(0, $this->name." silindi, bərpası mümkündür.", $this->route);
    }


    public function restore($id)
    {
        $member = $this->model->onlyTrashed()->findOrFail($id);
        $member->restore();

        return $this->responseJson(0, $this->name." bərpa olundu.", $this->route);
    }


    public function destroy($id)
    {
        $slider = $this->model->findOrFail($id);
        $slider->forceDelete();

        return $this->responseJson(0, $this->name." birdəfəlik silindi.", $this->route, "modal-confirm");
    }


    private function rules($model, $id = null)
    {
        if(isset($this->rules) && $this->rules == 'func'){
           $rules = $model::rules($id);
        }
        else{
            if(array_key_exists('file', $model::$rules)) {
                $model::$rules['file'] = 'sometimes';
            }

            $rules = $model::$rules;
        }

        return $rules;
    }
}
