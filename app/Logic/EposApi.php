<?php

namespace App\Logic;

/* Версия скрипта 1.0.1 (update 11.10.2018)
 *
 * Пример использования Api на сайте https://epos.az , также можно использовать как готовый класс в ваших проектах
 * При создании обьекта принимает 3 параметра:
 * $publicKey - публичный ключ  (строка https://epos.az/users/profile)
 * $privateKey -  приватный ключ (строка https://epos.az/users/profile)
 * $params -  массив параметров для использования методов API. Посмотреть можно по ссылке https://epos.az/pay2me.
 * Далее подключаем нужный метод, в ответ мы получим готовый ответ в виде JSON
 *
 * Пример
 *
 * Получение статуса платежа
 * $params = array(
          'id'=> '1000',
      );

   $model = new EposApi($publicKey, $privateKey , $params);
   echo $model->statusPayments();

 * Произвести платеж
 * $params = array(
            'amount'=> '0.01',
            'phone'=> '994111111111',
            'cardType'=> '1',
            'successUrl'=>'http://site.com/success',
            'errorUrl'=> 'http://site.com/error',
            'payFormType'=>'DESKTOP',
            'currency'=>'AZN'
       );
   $model = new EposApi($publicKey, $privateKey , $params);
   echo $model->pay();
 */


class EposApi
{
    protected $urlApi = 'https://epos.az/api';
    protected $params;
    protected $privateKey;


    function __construct($publicKey = 'tqfk6f8PHFFetqjKD7vfFGZGTJFTQp', $privateKey = 'c9GhkCnjiVPNZ848Ix54DBBs7k2D4I', $params = array())
    {
        $this->params = $params;
        $this->privateKey = trim($privateKey);

        $this->params['key'] = trim($publicKey);
    }

    /**
     * Запрос статуса платежа
     * @return mixed
     * Ответ JSON  {"result":"success","info":"Оплата успешно выполнена!","status":"1","cardnumber":"xxxxxx**xxxx"}
     * $params = array('id'=> 10000)
     */
    public function statusPayments($params)
    {
        $this->params = array_merge($this->params, $params);

        $url = $this->urlApi . '/pay2me/status/?' . $this->paramsUrl();
        return $this->_getRequest($url);
    }


    /**
     * Произвести платеж
     * @return mixed
     * Ответ JSON {"result":"success","paymentUrl":"http://site.com/id/ffg3543ergerg","id":"586"}
     * $params = array('amount'=> '0.01','phone'=> '994111111111','cardType'=> '1','successUrl'=>'http://site.com/success','errorUrl'=>'http://site.com/error','payFormType'=>'DESKTOP','currency'=>'AZN');
     */
    public function pay($params)
    {
        $this->params = array_merge($this->params, $params);

        $url = $this->urlApi . '/pay2me/pay/?' . $this->paramsUrl();
        return $this->_getRequest($url);
    }


    /**
     * Запрос кредитной линии, рассрочки ежемесячно
     * @return mixed
     * $params - не передаем
     * Ответ JSON{"result":"success","info":"Запрос успешно выполнен!","instalments":"1,3,18"}
     */
    public function instalments()
    {
        $url = $this->urlApi . '/pay2me/instalments/?' . $this->paramsUrl();
        return $this->_getRequest($url);
    }


    /**
     * Регистрация карты для рекуррентного платежа
     * @return mixed
     * $params = array('amount'=> '0.01','phone'=> '994111111111','cardType'=> '1','returnUrl'=>'http://site.com/success','clientid'=>'10');
     * Ответ JSON {"result":"success","paymentUrl":"http://site.com/id/ffg3543ergerg","id":"586"}
     */
    public function recurrentReg()
    {
        $url = $this->urlApi . '/recurrent/reg/?' . $this->paramsUrl();
        return $this->_getRequest($url);
    }


    /**
     * Рекуррентный платеж
     * $params = array('amount'=> '0.01','phone'=> '994111111111','cardType'=> '1','returnUrl'=>'http://site.com/success','clientid'=> '10','bindingid'=> '10');
     * Ответ JSON {"result":"success","paymentUrl":"http://site.com/id/ffg3543ergerg","id":"586"}
     */
    public function recurrentPay()
    {
        $url = $this->urlApi . '/recurrent/pay/?' . $this->paramsUrl();
        return $this->_getRequest($url);
    }


    /**
     * Двух стадийная оплата
     * $params = array('amount'=> '0.01','phone'=> '994111111111','returnUrl'=>'http://site.com/success');
     * Ответ JSON {"result":"success","paymentUrl":"http://site.com/id/ffg3543ergerg","id":"586"}
     */
    public function twoStagePayment()
    {
        $url = $this->urlApi . '/twoStagePayment/beforePay/?' . $this->paramsUrl();
        return $this->_getRequest($url);
    }


    /**
     * Возврат средств
     * $params = array('id'=> '1002');
     * Ответ JSON {"result":"success","status":"0","info" :"Успешно, в обработке!"}
     */
    public function reversal()
    {
        $url = $this->urlApi . '/pay2me/reversal/?' . $this->paramsUrl();
        return $this->_getRequest($url);
    }


    /**
     * @return bool|string
     * Формируем ссылку с параметрами
     */
    protected function paramsUrl()
    {
        ksort($this->params);
        $sum = '';
        $params = '';

        foreach ($this->params as $k => $v) {
            $sum .= (string)$v;
            $params .= '&' . $k . '=' . urlencode($v);  //формируем параметры для платежной ссылки
        }
        $sum .= $this->privateKey;

        return 'sum=' . strtolower(md5($sum)) . $params;

    }


    private function _getRequest($url)
    {
        $header = array('Referer: xxx',
            'Origin: xxx',
            'Content-Type: application/x-www-form-urlencoded',
            'Connection: keep-alive',
            'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3',
            'Cache-Control: max-age=0',
            'Except:');

        $ch = curl_init($url);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.0; MyIE2; .NET CLR 1.1.4322)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }


}