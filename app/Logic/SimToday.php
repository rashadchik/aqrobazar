<?php

namespace App\Logic;

use Crypt_RSA;

class SimToday
{
    private $username = 'aqrobazarcom';
    private $hash = '71f7cf3145c6a4a211d0465b99b9a7e5';
    private $test_url = "http://94.20.71.249/micropayment/";
    private $pro_url = "https://ssl.simtoday.net/micropayment/";
    private $callback = 'https://aqrobazar.com/sms/complete';
    private $lang = 'az';
    private $status;
    public $action;
    public $operator;
    public $tariff;
    public $orderId;


//{"username":"aqrobazarcom", "hash":"71f7cf3145c6a4a211d0465b99b9a7e5", "action":"get_ui_url","lang":"az", "operator":"1", "tariff":"1005", "order_id":"123","callback_url":"http://partner.site.com/my_script"}

// {"username":"mylogin", "hash":"8d2ca02cbb2668eb8dedcd27a1245ccf742ad2", "action":"get_tariffs", "operator":"1"}

// {"result":"100","transaction":"231a86201128acff113ba42e75438e52","operators":{"1":"Bakcell","2":"Azercell","3":"Nar"}}

    public function __construct()
    {
        $this->status = config('app.env');
        //$this->status = 'local';
    }


    public function getTariff($amount, $orderId)
    {
        $this->action = 'get_tariffs';
        $this->operator = 1;

        $result = $this->query(json_encode($this->setData()));

        $tariffs = (\GuzzleHttp\json_decode($result, true)['tariffs']);
        $tariffId = array_search($amount, $tariffs);

        return $this->getUrl($tariffId, $orderId);
    }



    private function getUrl($tariffId, $orderId)
    {
        $this->action = 'get_ui_url';
        $this->operator = 1;
        $this->tariff = $tariffId;
        $this->orderId = $orderId;

        $result = $this->query(json_encode($this->setData()));

        $response = (\GuzzleHttp\json_decode($result, true)['ui_url']);

        return $response;
    }


    private function query($jsonData)
    {
        require_once '../app/Lib/SimToday/RSA.php';

        $rsa = new Crypt_RSA();
        $rsa->loadKey(file_get_contents('../app/Lib/SimToday/7402ccd7a1245ccbbad2_pub_key'));

        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
        $encryptedData = $rsa->encrypt($jsonData);
        $hexData = bin2hex($encryptedData);
        /*     * ********* */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(Array("data" => $hexData)));
        $response = curl_exec($ch);
        curl_close($ch);
        /*     * ********* */
        return $response;
    }


    private function setData()
    {
        $this->data = [
            'username' => $this->username,
            'hash' => $this->hash,
            'lang' => $this->lang,
            'action' => $this->action,
            'operator' => $this->operator,
            'tariff' => $this->tariff,
            'order_id' => $this->orderId,
            'callback_url' => $this->callback
        ];

        return $this->data;
    }


    private function url()
    {
        if($this->status == "local") {
            $url = $this->test_url;
        }
        else {
            $url = $this->pro_url;
        }

        return $url;
    }

}