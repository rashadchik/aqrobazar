<?php

namespace App\Logic;

trait DataTableIndex
{
    public function index()
    {
        $dataTable = $this->dataTable;

        return $dataTable
            ->with('route', $this->route)
            ->render("admin.dt.index", [
                'title' => $this->title,
                'route' => $this->route,
                'createRoute' => isset($this->createRoute) && $this->createRoute == false ? null : "$this->route.create",
                'reOrderRoute' => "$this->route.reorder",
                'modal' => $this->createRouteViaModal ?? true,
                'largeModal' => $this->largeModal ?? false,
                'filters' => $this->filters ?? []
            ]);
    }
}
