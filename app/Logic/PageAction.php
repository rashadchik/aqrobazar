<?php

namespace App\Logic;

use App\Models\Page;
use App\Models\PageTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

trait PageAction
{
    public function create()
    {
        $fields = $this->crud->fields('create', ['meta' => $this->meta ?? false]);

        return view("admin.dt.create",  ['fields' => $fields, 'title' => "Yeni $this->name", 'route' => "$this->route.store", 'editor' => $this->editor ?? false, 'meta' => $this->meta ?? false]);
    }


    public function edit($id)
    {
        $parameterCrud = $this->paramCrud;
        $fields = [];
        $langs = [];
        $keys = [];

        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.id', $id)
            ->whereNull('pt.deleted_at')
            ->select(
                'pt.*',
                'pt.id as tid',
                'pages.id',
                'pages.parent_id',
                'pages.template_id',
                'pages.visible',
                'pages.target'
            )
            ->firstOrFail();

        /*if(count(config('app.locales')) == 1){
            return $this->singleEdit($page);
        }*/

        $relatedPage = $page->relatedPages;
        $parameters = $parameterCrud->fields('edit', ['data' => $page]);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields('edit', ['data' => $rel, 'lang' => $rel->lang, 'meta' => $this->meta ?? false]);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields('edit', ['lang' => $key, 'meta' => $this->meta ?? false]);

            $keys[$key] = [];
        }

        return view('admin.page.edit-withlocale', ['info' => $page, 'langs' => $langs, 'parameters' => $parameters, 'fields' => $fields, 'relatedPage' => $relatedPage, 'keys' => $keys, 'route' => $this->route, 'editor' => $this->editor ?? false, 'meta' => $this->meta ?? false]);
    }


    public function store(Request $request)
    {
        $lang = $request->lang;

        // check Validation
        $validation = Validator::make($this->requests, Page::rules($lang, null), Page::$messages);
        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }


        // apply
        $this->requests['slug'] = $this->setSlug($request);
        $this->requests['order'] = Page::max('order') + 1;
        $this->requests['template_id'] = $this->templateId ?? $request->template_id;

        //inputs for models
        $pageInputs = array_diff_key($this->requests, array_flip($this->translates));
        $translationInputs = array_diff_key($this->requests, $pageInputs);

        //store
        DB::beginTransaction();


        try{
            $page = Page::create($pageInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }

        try{
            $translationInputs['page_id'] = $page->id;
            PageTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }

        if($request->hasFile('file')){

            $page->addMedia($request->file)
                ->toMediaCollection($this->mediaLibraryCollection ?? 'default', $this->diskName ?? 's3');
        }

        DB::commit();

        return $this->responseJson(0, "$this->name əlavə edildi", $this->route, "myModal");
    }


    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, Page::$parameterRules, Page::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        DB::beginTransaction();

        if($request->hasFile('file')){

            $page->clearMediaCollection($this->mediaLibraryCollection ?? 'default');  //delete old file
            $page->addMedia($request->file)
                ->toMediaCollection($this->mediaLibraryCollection ?? 'default', $this->diskName ?? 's3');
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function storeTrans(Request $request)
    {
        // check Validation
        $validation = Validator::make($this->requests, PageTranslation::rules($request->lang, null), PageTranslation::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        $this->requests['slug'] = $this->setSlug($request);
        PageTranslation::create($this->requests);

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function updateTrans(Request $request, $id)
    {
        $page = PageTranslation::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, PageTranslation::rules($request->lang, $id), PageTranslation::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        if($request->slug != $page->slug){
            $this->requests['slug'] = $this->setSlug($request, $id);
        }

        if(!$request->meta_keywords) {
            $this->requests['meta_keywords'] = null;
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function trash($id)
    {
        $page = PageTranslation::findOrFail($id);

        DB::beginTransaction();


        try{
            $page->delete();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route);
    }


    public function restore($id)
    {
        $page = PageTranslation::onlyTrashed()->findOrFail($id);

        DB::beginTransaction();


        try{
            $page->restore();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route);
    }


    public function destroy($id)
    {
        $page = PageTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = PageTranslation::where('page_id', $page->page_id)->withTrashed()->count();

        if($relatedPages == 1){
            Page::where('id', $page->page_id)->forceDelete();
        }
        else{
            $page->forceDelete();
        }

        return $this->responseJson(0, trans('locale.update_success'),$this->route, "modal-confirm");
    }


    public function order()
    {
        return $this->order->get('page', 'name', 3, true);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'page', true);
    }



    private function setSlug($request, $id = null)
    {
        $slug = str_slug($request->slug ?? $request->name);

        $checkSlug = PageTranslation::where('lang', $request->lang)->where('slug', $slug)->count();

        if($checkSlug > 0){
            $maxId = DB::table('page_translations')->where('id', '<>', $id)->max('id');
            return $slug.'-'.($maxId + 1);
        }
        else{
            return $slug;
        }
    }
}
