<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Cellphone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public $prefix;
    public $lengthPhone;

    public function __construct($prefix, $lengthPhone = 9)
    {
        $this->prefix = $prefix;
        $this->lengthPhone = $lengthPhone;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(array_key_exists($this->prefix, config('config.prefix-number'))){
            return strlen($value) === $this->lengthPhone;
        }
        else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Telefon nömrəsi düzgün deyil!';
    }
}
