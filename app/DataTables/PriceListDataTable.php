<?php

namespace App\DataTables;

use App\Models\PriceList;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PriceListDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('type', function($row) {
                return trans("locale.price-list.$row->type");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => $this->route, 'forceDelete' => true])->render();
            })
            ->rawColumns(['action']);
    }


    public function query(PriceList $model)
    {
        $query = $model->newQuery();
        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'type', 'title' => 'Növ', 'searchable' => false],
            ['data' => 'amount', 'title' => 'Məbləğ'],
            ['data' => 'day', 'title' => 'Gün'],
            ['data' => 'note', 'title' => 'Təsvir', 'orderable' => false],
            ['data' => 'created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'faqdatatable_' . time();
    }
}
