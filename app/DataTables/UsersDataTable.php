<?php

namespace App\DataTables;

use App\Models\Store;
use App\User;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('store', function($row) {
                is_null($row->store) ? $company = 'Seç' : $company = $row->store;

                return '<a href="#" class="editable-store" data-url="'.route('user.fastUpdate', $row->id).'" data-type="select" data-value="'.$row->store_id.'" data-pk="'.$row->id.'" data-token="'.csrf_token().'">'.$company.'</a>';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => $this->route, 'softDelete' => true, 'forceDelete' => false, 'edit' => false])->render();
            })
            ->rawColumns(['store', 'action'])
            ->with('stores', encode(array_add(Store::pluck('name', 'id')->toArray(), '', '---')));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $query = $model->newQuery()
            ->leftJoin('stores', 'stores.id', '=', 'users.store_id')
            ->select('users.*', 'stores.name as store');

        if($this->request()->get('deleted') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('deleted') == 2){
            $query->onlyTrashed();
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '100px', 'title' => ''])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'users.id', 'title' => 'ID', 'visible' => false],
            ['data' => 'name', 'name' => 'users.name', 'title' => 'Ad, soyad', 'orderable' => false],
            ['data' => 'phone', 'name' => 'users.phone', 'title' => 'Mobil', 'orderable' => false],
            ['data' => 'email', 'name' => 'users.email', 'title' => 'Email', 'orderable' => false],
            ['data' => 'store', 'name' => 'stores.name', 'title' => 'Mağaza'],
            ['data' => 'created_at', 'name' => 'users.created_at', 'title' => 'Qeydiyyat Tarixi', 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50,100],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function(settings) {
            
                var api = this.api()
                var json = api.ajax.json();
                
                $("#user .editable-store").editable({
                    showbuttons:false,
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "store_id",
                    },
                    source: json.stores
                });
            }'
        ];
    }


    protected function filename()
    {
        return 'Users' . date('YmdHis');
    }
}
