<?php

namespace App\DataTables;

use App\Models\Article;
use App\Models\ArticleTranslation;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class ArticleDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('published_at', function($row) {
                return filterDate($row->published_at);
            })
            ->editColumn('image', function($row) {
                return '<img src="'.asset($row->getFirstMedia()->getUrl('xs')).'">';
            })
            ->editColumn('title', function($row) {
                return str_limit($row->title, $limit = 50, $end = '...');
            })
            ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->rawColumns(['status', 'image', 'action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', [
                    'route' => $this->route,
                    'row' => $row,
                    'show' => url("/$row->category_slug/$row->slug"),
                    'album' => true,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'largeModal' => true
                ])->render();
            });
    }


    public function query(Article $model)
    {
        $query = $model->newQuery()
            ->join('article_translations as at', 'at.article_id', '=', 'articles.id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pt.page_id', '=', 'pages.id')
            //->where('pages.template_id', '<>', 13)
            ->select('articles.status', 'articles.featured', 'articles.published_at',  'articles.published_by', 'at.*', 'at.id as tid', 'articles.id', 'pt.name as category', 'pt.slug as category_slug');

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('at.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('deleted') == 1){
            $query->whereNull('at.deleted_at');
        }
        elseif($this->request()->get('deleted') == 2){
            $query->whereNotNull('at.deleted_at');
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '150px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'at.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false, 'searchable' => false],
            ['data' => 'published_at', 'name' => 'articles.published_at', 'title' => 'Tarix'],
            ['data' => 'title', 'name' => 'at.title', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'status', 'name' => 'articles.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'lang', 'name' => 'at.lang', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'slug', 'name' => 'at.slug', 'title' => 'Slug', 'orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'published_by', 'name' => 'articles.published_by', 'title' => 'Müəllif','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'at.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'at.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#articles_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }
}
