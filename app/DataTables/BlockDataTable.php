<?php

namespace App\DataTables;

use App\Models\Block;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class BlockDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'block', 'forceDelete' => true])->render();
            })
            ->rawColumns(['action']);
    }


    public function query(Block $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '60px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'blocks.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title', 'name' => 'blocks.title', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'desc', 'name' => 'blocks.desc', 'title' => 'Təsvir', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'blocks.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'blocks.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'blockdatatable_' . time();
    }
}
