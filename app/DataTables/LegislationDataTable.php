<?php

namespace App\DataTables;

use App\Models\City;
use App\Models\Legislation;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class LegislationDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => $this->route, 'forceDelete' => true])->render();
            })
            ->editColumn('file', function($row) {
                if($row->getFirstMediaUrl() != ''){
                    return '<a href="'.asset($row->getFirstMediaUrl()).'" target="_blank">File</a>';
                }
                else{
                    return 'Fayl yüklənməyib';
                }
            })
            ->rawColumns(['action', 'file']);
    }

    public function query(Legislation $model)
    {
        $query = $model->newQuery()
            ->with('media');

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'legislations.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'file', 'name' => 'media.image', 'title' => 'File', 'orderable' => false, 'searchable' => false],
            ['data' => 'title', 'name' => 'legislations.title','title' => 'Başlıq', 'orderable' => true],
            ['data' => 'created_at', 'name' => 'legislations.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'legislations.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'colorsdatatable_' . time();
    }

}
