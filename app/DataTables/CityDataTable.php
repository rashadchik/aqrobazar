<?php

namespace App\DataTables;

use App\Models\City;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CityDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => $this->route, 'forceDelete' => true])->render();
            })
            ->rawColumns(['action']);
    }

    public function query(City $model)
    {
        $query = $model
            ->newQuery()
            ->leftJoin('countries as co', 'co.id', '=', 'cities.country_id')
            ->leftJoin('country_trs as cot', function ($join) {
                $join->on('co.id', '=', 'cot.country_id')->where('lang', 'az');
            })
            ->select('cities.*', 'cot.name as country');

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'cities.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name_az', 'name' => 'cities.name_az','title' => 'Şəhər'],
            ['data' => 'country', 'name' => 'cot.name', 'title' => 'Ölkə'],
            ['data' => 'created_at', 'name' => 'cities.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'cities.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }
}
