<?php

namespace App\DataTables;

use App\Models\Product;
use App\Models\Store;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class StoreDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($row) {
                return '<img src="'.asset($row->getFirstMedia('logo')->getUrl('xs')).'">';
            })
            ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->rawColumns(['status', 'image', 'action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', [
                    'route' => $this->route,
                    'show' => url('stores/'.$row->slug),
                    'row' => $row,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'largeModal' => true
                ])
                ->render();
            });
    }


    public function query(Store $model)
    {
        $query = $model->newQuery()
            ->join('store_translations as st', 'st.store_id', '=', 'stores.id')
            ->join('store_addresses as sa', 'sa.store_id', '=', 'stores.id')
            ->join('cities', 'cities.id', '=', 'sa.city_id')
            ->select('stores.*', 'cities.name_az as city', 'st.id as tid')
            ->with('media');

        if($this->request()->get('deleted') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('deleted') == 2){
            $query->onlyTrashed();
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'title' => '', 'exportable' => false, 'printable' => false])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'stores.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Loqo', 'orderable' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'stores.name', 'title' => 'Mağaza adı', 'orderable' => false],
            ['data' => 'phone', 'name' => 'stores.phone', 'title' => 'Telefon', 'orderable' => false],
            ['data' => 'whatsapp', 'name' => 'stores.whatsapp', 'title' => 'WhatsApp', 'orderable' => false],
            ['data' => 'balance', 'name' => 'stores.balance', 'title' => 'Balans ₼', 'searchable' => false],
            ['data' => 'status', 'name' => 'stores.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'city', 'name' => 'cities.name_az', 'title' => 'Şəhər', 'class' => 'none', 'searchable' => false],
            ['data' => 'slug', 'name' => 'stores.slug', 'title' => 'Slug', 'class' => 'none', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'stores.created_at', 'title' => 'Yaradıldı', 'orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'stores.updated_at', 'title' => 'Yenilənib', 'orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }
}
