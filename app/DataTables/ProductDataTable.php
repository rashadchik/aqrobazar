<?php

namespace App\DataTables;

use App\Models\Product;
use App\Models\Store;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class ProductDataTable extends DataTable
{
    protected $actions = ['excel', 'csv', 'print', 'reload'];

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('name', function($row) {
                return '<a href="#" class="editable-name" data-url="'.route('product.fastUpdate', $row->id).'" data-type="text" data-pk="'.$row->id.'" data-title="'.$row->name.'" data-token="'.csrf_token().'">'.$row->name.'</a>';
            })
            ->editColumn('slug', function($row) {
                return '<a href="#" class="editable-slug" data-url="'.route('product.fastUpdate', $row->id).'" data-type="text" data-pk="'.$row->id.'" data-title="'.$row->slug.'" data-token="'.csrf_token().'">'.$row->slug.'</a>';
            })
            ->editColumn('category', function($row) {
                if(is_null($row->parent)) {
                    return $row->category;
                }
                else {
                    return $row->parent.' / '.$row->category;
                }
            })
            ->editColumn('price', function($row) {
                return $row->price;
            })
            ->editColumn('status', function($row) {
                $status = trans("locale.product-status.$row->status.title");

                return '<a href="#" class="editable-status" data-url="'.route('product.fastUpdate', $row->id).'" data-type="select" data-value="'.$row->status.'" data-pk="'.$row->id.'" data-token="'.csrf_token().'">'.$status.'</a>';
            })
            ->editColumn('store', function($row) {
                $title = $row->store ?? 'Transfer et';
                is_null($row->store) ? $class = 'text-red' : $class = '';
                return '<a href="#" class="editable-store '.$class.'" data-url="'.route('product.fastUpdate', $row->id).'" data-type="select" data-value="'.$row->store_id.'" data-pk="'.$row->id.'" data-token="'.csrf_token().'">'.$title.'</a>';
            })
            ->editColumn('type', function($row) {
                return trans("locale.ad-type.$row->type");
            })
            ->editColumn('is_vip', function($row) {

                if($row->is_fav == 1) {
                    $fav = ' <a href="#" class="open-modal-dialog" data-link="'.route('fav.edit', $row->id).'"><span class="label label-warning"><i class="fa fa-arrow-up"></i></span></a>';
                }
                else {
                    $fav = '';
                }

                if($row->is_vip == 1) {
                    $status = '<a href="#" class="open-modal-dialog" data-link="'.route('vip.edit', $row->id).'"><span class="label label-primary">VIP</span></a>';
                }
                else{
                    $status = 'Adi';
                }

                return $status.$fav;
            })
            ->with('status', encode(array_map(function ($arr) {return $arr['title'];}, trans("locale.product-status"))))
            ->with('stores', encode(Store::pluck('name', 'id')->prepend('---', '')->toArray()))
            ->rawColumns(['status', 'image', 'name', 'slug', 'price', 'store', 'is_vip', 'action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-product-dt', ['route' => $this->route, 'show' => url($row->category_slug.'/'.$row->slug), 'row' => $row, 'softDelete' => true, 'forceDelete' => false, 'largeModal' => true])->render();
            });
    }


    public function query(Product $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages as p', 'p.id', '=', 'products.category_id')
            ->leftJoin('page_translations as pt', 'p.id', '=', 'pt.page_id')
            ->leftJoin('page_translations as pt2', 'p.parent_id', '=', 'pt2.page_id')
            ->leftJoin('cities', 'cities.id', '=', 'products.city_id')
            ->leftJoin('stores', 'stores.id', '=', 'products.store_id')
            ->where('products.status', '<>', 100)
            ->select(
                'cities.name_az as city',
                'products.*',
                'pt.name as category',
                'pt.slug as category_slug',
                'pt2.name as parent',
                'stores.name as store'
            );

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('products.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('deleted') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('deleted') == 2){
            $query->onlyTrashed();
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '60px', 'title' => '', 'exportable' => false, 'printable' => false])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'products.id', 'title' => 'ID', 'printable' => false],
            ['data' => 'name', 'name' => 'products.name', 'title' => 'Elan adı', 'orderable' => false],
            ['data' => 'status', 'name' => 'products.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'price', 'name' => 'products.price', 'title' => 'Qiymət ₼', 'searchable' => false],
            ['data' => 'category', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'is_vip', 'name' => 'products.is_vip', 'title' => 'Elan növü', 'searchable' => false],
            ['data' => 'type', 'name' => 'products.type', 'title' => 'Xidmət növü', 'searchable' => false],
            ['data' => 'store', 'name' => 'stores.name', 'title' => 'Mağaza'],
            ['data' => 'slug', 'name' => 'products.slug', 'title' => 'Slug', 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'city', 'name' => 'cities.name_az', 'title' => 'Şəhər', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'user_phone', 'name' => 'products.user_phone', 'title' => 'Telefon', 'class' => 'none'],
            ['data' => 'user_email', 'name' => 'products.user_email', 'title' => 'E-poçt', 'class' => 'none'],
            ['data' => 'created_at', 'name' => 'products.created_at', 'title' => 'Yaradıldı', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'updated_at', 'name' => 'products.updated_at', 'title' => 'Yenilənib', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50,100],
            'dom' => '<"col-md-6 hidden-xs pull-left"<"row"<"pull-left"l><"pull-left"<"col-md-12"B>>>><"pull-right col-md-6 custom-filter"f>rt<"col-md-3 col-sm-3"<"row"i>><"col-md-9"<"row"p>>',
            'buttons' => ['excel', 'csv', 'print', 'reload'],
            'drawCallback' => 'function(settings) {   
                     
                var api = this.api()
                var json = api.ajax.json();
                                
                $("#product .editable-status").editable({
                    showbuttons:false,
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "status",
                    },
                    source: json.status
                });    
                
                $("#product .editable-store").editable({
                    showbuttons:true,
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "store_id",
                    },
                    source: json.stores
                });            
            }',
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'Products_' . date('YmdHis');
    }
}
