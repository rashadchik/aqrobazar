<?php

namespace App\DataTables;

use App\Models\Payment;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class InvoiceDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('product', function($row) {
                return '<a href="'.url('ads/'.$row->product_id).'" target="_blank">'.$row->product.'</a>';
            })
            ->editColumn('type', function($row) {
                return trans("locale.price-list.$row->type");
            })
            ->editColumn('created_at', function ($row){
                return filterDate($row->created_at, true, 'eFull');
            })
            ->editColumn('amount', function ($row){
                return $row->amount/100;
            })
            ->editColumn('day', function ($row){
                return $row->day.' gün';
            })
            ->editColumn('code', function ($row){
                $text = config('config.payment-code.'.$row->code);

                if($row->code == 0){
                    return '<span class="text text-success">'.$text.'</span>';
                }
                else{
                    return '<span class="text text-danger">'.$text.'</span>';
                }
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'invoices', 'softDelete' => false, 'forceDelete' => false, 'edit' => false])->render();
            })
            ->rawColumns(['product', 'code', 'action']);

    }

    public function query(Payment $model)
    {
        $query = $model->newQuery()
            ->join('products', 'products.id', '=', 'payments.product_id')
            ->leftJoin('users', 'users.id', '=', 'payments.user_id')
            ->select(
        'payments.id',
                'payments.amount',
                'payments.created_at',
                'payments.type',
                'payments.day',
                'payments.card_number',
                'payments.code',
                'users.name as user',
                'products.name as product',
                'products.id as product_id'
            );

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            //->addAction(['width' => '50px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'payments.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'product', 'name' => 'products.name','title' => 'Elan', 'orderable' => false],
            ['data' => 'type', 'name' => 'payments.type', 'title' => 'Növ', 'searchable' => false],
            ['data' => 'day', 'name' => 'payments.day', 'title' => 'Müddət', 'searchable' => false],
            ['data' => 'user', 'name' => 'users.name','title' => 'İstifadəçi', 'orderable' => false],
            ['data' => 'amount', 'name' => 'payments.amount','title' => 'Məbləğ', 'searchable' => false],
            ['data' => 'code', 'name' => 'payments.code','title' => 'Yekun', 'searchable' => false],
            ['data' => 'card_number', 'name' => 'payments.card_number','title' => 'Kartın son 4 rəqəmi:', 'searchable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'name' => 'payments.created_at','title' => 'Tarix', 'searchable' => false, 'class' => 'none'],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50,100],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'paymentsdatatable_' . time();
    }

}
