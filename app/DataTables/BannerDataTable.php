<?php

namespace App\DataTables;

use App\Models\Banner;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class BannerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('device', function($banner) {
                return config("config.banner-devices.$banner->device");
            })
            ->editColumn('status', function($banner) {
                return '<label class="label label-'.config("config.label.$banner->status").'">'.config("config.status.$banner->status").'</label>';
            })
            ->editColumn('type', function($banner) {
                return config("config.banner-type.$banner->type");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['route' => $this->route, 'show' => url('/admin/banner/'.$row->id), 'row' => $row, 'forceDelete' => true, 'largeModal' => false])->render();
            })
            ->rawColumns(['image', 'status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Banner $model)
    {
        $query = $model->newQuery()
                    ->leftJoin('stores', 'stores.id', '=','banners.store_id')
                    ->select('banners.*', 'stores.name as store');



        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '90px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'visible' => false ,'searchable' => false],
            ['data' => 'title', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'device', 'title' => 'Device','searchable' => false],
            ['data' => 'type', 'title' => 'Növü','searchable' => false],
            ['data' => 'width', 'title' => 'Uzunluq' ,'searchable' => false],
            ['data' => 'height', 'title' => 'Hündürlük' ,'searchable' => false],
            //['data' => 'viewed', 'title' => 'Trafik','searchable' => false],
            ['data' => 'public_viewed', 'title' => 'Görülüb','searchable' => false],
            ['data' => 'store', 'name' =>'stores.name', 'title' => 'Mağaza', 'searchable' => false],
            ['data' => 'status', 'title' => 'Status','searchable' => false],
            ['data' => 'link', 'title' => 'Link','orderable' => false, 'class' => 'none' ,'searchable' => false],
            ['data' => 'created_at', 'title' => 'Yaradılıb','orderable' => false, 'class' => 'none' ,'searchable' => false],
            ['data' => 'updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none' ,'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthChange' => false,
            'dom' => '<"col-md-4 hidden-xs pull-left"<"row"<"pull-left"l><"pull-left"<"col-md-12"B>>>><"pull-right col-md-8 custom-filter"f>rt<"col-md-3 col-sm-3"<"row"i>><"col-md-9"<"row"p>>',
            'buttons' => ['reload'],
            'language' => [
                'url' => url('lang.json').'?v=1',
            ]
        ];
    }


}
