<?php

namespace App\DataTables;

use App\Models\Country;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CountryDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('published', function($row) {
                return '<span class="text-'.config("config.alert.$row->published").' text-bold">'.config("config.status.$row->published").'</span>';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => $this->route, 'softDelete' => true, 'forceDelete' => false])->render();
            })
            ->rawColumns(['action', 'published']);
    }

    public function query(Country $model)
    {
        $query = $model
            ->newQuery()
            ->join('country_trs as ct', 'countries.id', '=', 'ct.country_id')
            ->where('ct.lang', 'az')
            ->select('countries.*', 'ct.name');

        if($this->request()->get('deleted') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('deleted') == 2){
            $query->onlyTrashed();
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'countries.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'ct.name','title' => 'Ölkə'],
            ['data' => 'prefix', 'name' => 'countries.prefix','title' => 'Prefix'],
            ['data' => 'c_code', 'name' => 'countries.c_code','title' => 'Kod'],
            ['data' => 'published', 'name' => 'countries.published','title' => 'Status'],
            ['data' => 'created_at', 'name' => 'countries.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'countries.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }
}
