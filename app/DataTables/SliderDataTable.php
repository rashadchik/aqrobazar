<?php

namespace App\DataTables;

use App\Models\Slider;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class SliderDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($slider) {
                return '<img src="'.asset("storage/thumb/".$slider->image).'">';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['route' => $this->route, 'row' => $row, 'softDelete' => true, 'forceDelete' => true])->render();
            })
            ->rawColumns(['image', 'action']);
    }



    public function query(Slider $model)
    {
        $query = $model->newQuery();

        if($this->request()->get('type') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('type') == 2){
            $query->onlyTrashed();
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'order', 'name' => 'sliders.order', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'sliders.image', 'title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'title', 'name' => 'sliders.title', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'summary', 'name' => 'sliders.summary', 'title' => 'Məzmun', 'class' => 'none'],
            ['data' => 'link', 'name' => 'sliders.link', 'title' => 'Link', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'sliders.created_at', 'title' => 'Yaradıldı','searchable' => false],
            ['data' => 'updated_at', 'name' => 'sliders.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'rowReorder' => [
                'dataSrc' => 'order',
                'update' => false
            ],
            'order' => [ [0,'asc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'sliderdatatable_' . time();
    }
}
