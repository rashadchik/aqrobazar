<?php

namespace App\DataTables;

use App\Models\Question;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class FaqDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => $this->route, 'forceDelete' => true])->render();
            })
            ->rawColumns(['action']);
    }


    public function query(Question $model)
    {
        $this->request()->has('type') ? $type = $this->request()->get('type') : $type = 0;

        $query = $model->where('type', $type)->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'questions.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title', 'name' => 'questions.title', 'title' => 'Sual', 'orderable' => false],
            ['data' => 'text', 'name' => 'questions.text', 'title' => 'Cavab', 'orderable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'name' => 'questions.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'questions.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'faqdatatable_' . time();
    }
}
