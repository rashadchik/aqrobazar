<?php

namespace App\DataTables;

use App\Models\Page;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class ProductCategoryDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('visible', function($row) {
                $row->visible == 0 ? $status = 'text-danger' : $status = 'text-success';
                return '<span class="'.$status.' text-bold">'.config("config.menu-visibility-boolean.$row->visible").'</span>';
            })
            ->editColumn('image', function($row) {
                if($row->getFirstMediaUrl('icon') != ''){
                    return '<img src="'.asset($row->getFirstMediaUrl('icon')).'" style="min-width:70px">';
                }
                else{
                    return '';
                }
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', ['show' => url($row->parent_slug.'/'.$row->slug), 'route' => $this->route, 'row' => $row, 'softDelete' => true, 'forceDelete' => true, 'largeModal' => true])->render();
            })
            ->rawColumns(['visible', 'action', 'image']);
    }



    public function query(Page $model)
    {
        $query = $model->newQuery()
            ->join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->leftJoin('page_translations as parent', 'parent.page_id', '=', 'pages.parent_id')
            ->where('pages.template_id', 5)
            ->with('media')
            ->select(
                'pages.visible',
                'parent.name as parent',
                'parent.slug as parent_slug',
                'pt.*',
                'pt.id as tid',
                'pages.id'
            );

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('pt.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('deleted') == 1){
            $query->whereNull('pt.deleted_at');
        }
        elseif($this->request()->get('deleted') == 2){
            $query->whereNotNull('pt.deleted_at');
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'pt.id', 'title' => 'ID', 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Icon', 'orderable' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'pt.name', 'title' => 'Ad'],
            ['data' => 'slug', 'name' => 'pt.slug', 'title' => 'Slug'],
            ['data' => 'visible', 'name' => 'pages.visible', 'title' => 'Görünüş', 'searchable' => false],
            ['data' => 'parent', 'name' => 'parent.name', 'title' => 'Kateqoriya'],
            ['data' => 'lang', 'name' => 'pt.lang', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'pt.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'pt.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [15,25,50],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }
}
