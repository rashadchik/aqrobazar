<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminResetPasswordNotification extends Notification
{
    use Queueable;

    public $token;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Aqrobazar.com-da şifrənin bərpası')
            ->greeting('Salam,')
            ->line('Siz Aqrobazar.com-da şifrənin dəyişdirilməsi üçün sorğu göndərmisiz.')
            ->action('Şifrənin dəyişdirilməsini təsdiq edin', route('admin.password.reset', $this->token))
            ->line('Əgər bu mesajın sizə səhvən göndərildiyini düşünürsünüzsə, sadəcə ona məhəl qoymayın.')
            ->salutation('Hörmətlə,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
