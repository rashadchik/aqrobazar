<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewProduct extends Notification
{
    use Queueable;

    protected $product;
    protected $subject;
    protected $text;

    public function __construct($product, $subject, $text)
    {
        $this->product = $product;
        $this->subject = $subject;
        $this->text = $text;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $route = route('ads.show', $this->product->id);

        $text = $this->text;
        $text .= $route;
        $text .= '<br>';
        $text .= 'Elanın nömrəsi - '.$this->product->pin_code;

        return (new MailMessage)
            ->subject($this->subject)
            ->greeting('Hörmətli istifadəçi,')
            ->line($text)
            ->action('Elana bax', $route)
            ->salutation('Hörmətlə,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
