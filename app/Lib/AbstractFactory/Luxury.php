<?php


namespace App\Lib\AbstractFactory;

class Luxury extends Vehicle
{
    public function call()
    {
        return $this->cars[ array_rand($this->cars) ];
    }
}