<?php


namespace App\Lib\AbstractFactory;


class LowCost extends Vehicle
{
    public function call()
    {
        return $this->cars[ array_rand($this->cars) ];
    }
}