<?php

namespace App\Lib\AbstractFactory;

class VehicleFactory
{
    public static function getVehicles(string $type)
    {
        if ($type == 'luxury') {
            return new Luxury(['Tesla', 'BMW', 'Audi']);
        }
        else {
            return new LowCost(['Fiat', 'Seat', 'Renault']);
        }
    }
}