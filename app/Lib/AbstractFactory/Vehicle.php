<?php

namespace App\Lib\AbstractFactory;

abstract class Vehicle
{
    protected $cars = [];

    public function __construct(array $cars)
    {
        $this->cars = $cars;
    }

    abstract public function call();

}
