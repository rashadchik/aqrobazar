<?php

namespace App\Listeners;

use App\Events\AddModifiedByUser;
use App\Notifications\NewProduct;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendEmailOnUpdateByUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddModifiedByUser  $event
     * @return void
     */
    public function handle(AddModifiedByUser $event)
    {

        $emailTitle = $event->product->name." elanınız yoxlamaya göndərildi!";
        $text = 'Sizin "'.$event->product->name.'" başlıqlı elanınız yoxlamaya göndərildi. Elanınız bütün qaydalara uyğun olarsa, saytda yerləşdiriləcək.';

        $text .= '<br>';


        Notification::route('mail', $event->product->user_email)->notify(new NewProduct($event->product, $emailTitle, $text));
    }
}
