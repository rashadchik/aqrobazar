<?php

namespace App\Listeners;

use App\Events\AddModifiedByUser;
use App\Notifications\NewProduct;
use Illuminate\Support\Facades\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramNotificationOnUpdateByUser
{
    public $chatId;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->chatId = "-318602278";
    }

    /**
     * Handle the event.
     *
     * @param  AddModifiedByUser  $event
     * @return void
     */
    public function handle(AddModifiedByUser $event)
    {
        $notification = new SendTelegramNotification();

        $notification->get($event);
    }
}
