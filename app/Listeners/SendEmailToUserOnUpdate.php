<?php

namespace App\Listeners;

use App\Events\AddModified;
use App\Notifications\NewProduct;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendEmailToUserOnUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddModified  $event
     * @return void
     */
    public function handle(AddModified $event)
    {

        if($event->product->status == 1) {
            $emailTitle = $event->product->name." elanınız dərc edilib!";
            $text = 'Sizin "'.$event->product->name.'" başlıqlı elanınız qəbul edilmişdir və Aqrobazar.com saytında dərc olunmuşdur.';
        }
        elseif($event->product->status == 3) {
            $emailTitle = $event->product->name." elanınız yoxlamadan keçmədi!";
            $text = 'Sizin "'.$event->product->name.'" başlıqlı elanınız yoxlamadan keçmədi. Xahiş olunur, elanı doldurarkən qaydalara riayət edəsiz.';
        }
        else {
            $emailTitle = $event->product->name." elanınız yoxlamaya göndərildi!";
            $text = 'Sizin "'.$event->product->name.'" başlıqlı elanınız yoxlamaya göndərildi. Elanınız bütün qaydalara uyğun olarsa, saytda yerləşdiriləcək.';
        }

        $text .= '<br>';


        Notification::route('mail', $event->product->user_email)->notify(new NewProduct($event->product, $emailTitle, $text));
    }
}
