<?php

namespace App\Listeners;

use App\Events\AddCreated;
use App\Notifications\NewProduct;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendEmailToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddCreated  $event
     * @return void
     */
    public function handle(AddCreated $event)
    {
        $emailTitle = $event->product->name." elanınız əlavə olundu!";

        $text = 'Sizin "'.$event->product->name.'" başlıqlı elanınız yoxlamaya göndərildi. Elanınız bütün qaydalara uyğun olarsa, saytda yerləşdiriləcək.';
        $text .= '<br>';
        $text .= 'Elan təsdiqlənənədək siz ona aşağıdakı ünvanda baxa və düzəliş edə bilərsiniz:';
        $text .= '<br>';

        Notification::route('mail', $event->product->user_email)->notify(new NewProduct($event->product, $emailTitle, $text));
    }
}
