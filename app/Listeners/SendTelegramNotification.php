<?php

namespace App\Listeners;

use App\Events\AddCreated;
use App\Notifications\NewProduct;
use Illuminate\Support\Facades\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Telegram\Bot\Laravel\Facades\Telegram;

class SendTelegramNotification
{
    public $chatId;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->chatId = "-318602278";
    }

    /**
     * Handle the event.
     *
     * @param  AddCreated  $event
     * @return void
     */
    public function handle(AddCreated $event)
    {
        $this->get($event);
    }


    public function get($event)
    {
        $url = 'https://aqrobazar.com/ads/'.$event->product->id;
        $text = $event->telegramTitle .
            "\nID: ".$event->product->id .
            "\nLink: ".$url .
            "\nQiymət:  {$event->product->price}";

        //$event->type == 'create' ? $text .= "\n:  {$event->product->price}";

        if(auth()->guard('web')->check()) {
            $text = $text."\nİstifadəçi: ".auth()->guard('web')->user()->name;
        }

        if(env('APP_ENV') == 'production') {
            Telegram::sendMessage([
                'chat_id' => $this->chatId,
                'parse_mode' => 'HTML',
                'text' => $text,
            ]);
        }
    }
}
