<?php
/**
 * Created by PhpStorm.
 * User: Riko
 * Date: 8/16/2016
 * Time: 5:12 PM
 */

use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Logic\WebCache;

function getConfig()
{
    return WebCache::config()->pluck('value', 'key');
}

function activeUrl($url, $output = 'class=active')
{
    if(request()->url() == $url){
        return $output;
    }
}

function activeQuery($url, $output = 'class=active')
{
    if(request()->url().'?'.request()->getQueryString() == $url){
        return $output;
    }
}

function loading(){
    return "<i class='fa fa-circle-o-notch fa-spin'></i>";
}

function icon($icon, $title = null){
    return "<i class='fa fa-$icon'></i>";
}

function filterDate($date, $tz = true, $time = 'day', $symbol = '/')
{
    $date = with(new Carbon($date));

    if($tz == true){
        $date = $date->timezone('Asia/Baku');
    }

    if($time == 'eFull'){
        $date = $date->format('d/m/Y, H:i');
    }
    elseif($time == 'full'){
        $date = $date->format('Y-m-d H:i');
    }
    elseif($time == 'day'){
        $date = $date->format('Y-m-d');
    }
    elseif($time == 'eDay'){
        $date = $date->format('d/m/Y');
    }
    elseif($time == 'time'){
        $date = $date->format('H:i');
    }
    elseif($time == 'xeditableDate'){
        $date = $date->format('d/m/Y H:i'); //vergul yoxdu
    }

    return str_replace('/', $symbol, $date);
}

function convertDate($date, $time = false, $symbol = '/')
{
    if($time == false){
        list($d, $m, $y) = explode($symbol, $date);
        return "$y-$m-$d";
    }
    else{
        list($d1, $d2) = explode(' ', $date);
        list($d, $m, $y) = explode($symbol, $d1);

        return "$y-$m-$d $d2";
    }
}


function getYoutubeId($url)
{
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
    return $match[1];
}

function pageName($name)
{
    $remove = explode(' ', $name)[0];

    $replace = '<strong>'.$remove.'</strong>';

    $newName = str_replace($remove, $replace, $name);

    return $newName;
}

function clearCache($cache, $lang = true)
{
    if($lang == true){
        foreach(config('app.locales') as $key => $locale){
            Cache::forget($cache.'_'.$key);
        }
    }
    else{
        Cache::forget($cache);
    }
}



function sprint($int, $value)
{
    return sprintf("%".$int."d", $value);
}

function encode($data)
{
    $newData = [];

    foreach($data as $key => $d){
        $newData[] = ['value' => $key, 'text' => $d];
    }

    return json_encode($newData);
}

function secureAsset($link)
{
    if(env('APP_ENV') == 'production') {
        return secure_asset($link);
    }
    else{
        return asset($link);
    }
}

function hashImage($image)
{
    return  md5($image->getClientOriginalName()) . '.' . $image->getClientOriginalExtension();
}

function getExtension($path)
{
    return  pathinfo($path, PATHINFO_EXTENSION);
}


function filterPhone($phone)
{
    if(strlen($phone) == 9) {
        $prefix =  '0'.substr($phone, 0, 2);
        $number = substr($phone, 2);
        return $prefix.' '.$number;
    }

    return $phone;
}

function filterPhoneOnPost($val)
{
    return (int) str_replace([' ', '+(994', ')'], ['', '', ''], $val);
}
