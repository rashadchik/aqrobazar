<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreAddress extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
}
