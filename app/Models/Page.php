<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Page extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];

    const subCategories = [2, 7, 8, 9, 10, 163, 181];

    public static function rules($lang, $id){
        return [
            'name' => 'required|max:255',
            'parent_id' => 'nullable|exists:pages,id',
            'content' => 'nullable',
            'forward_url' => 'nullable',
            'target' => 'boolean',
            'visible' => 'required',
            'image' => 'sometimes|mimes:jpeg,jpg,png,svg|max:10000',
            'meta_description' => 'nullable|max:160',
            'meta_keywords' => 'nullable'
        ];
    }

    public static $parameterRules = [
        'template_id' => 'sometimes|numeric',
        'target' => 'boolean',
        'visible' => 'required',
        'image' => 'sometimes|mimes:jpeg,jpg,png,svg|max:10000',
    ];


    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
    ];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function scopeVisible($query)
    {
        return $query->where('visible', '<>', 0);
    }


    public function children()
    {
        return $this->hasMany(Page::class, 'parent_id', 'id')
            ->join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
            ->where('pages.visible', '<>', 0)
            ->select('pages.id', 'pt.name', 'pt.slug', 'pages.parent_id', 'pages.template_id')
            ->orderBy('pages.order', 'asc');
    }


    public function menuChildren()
    {
        return $this->hasMany(Page::class, 'parent_id', 'id')
            ->select('pages.id', 'pages.parent_id', 'pages.template_id')
            ->whereNotIn('pages.visible', [0]);
    }


    public function products() //product categories count
    {
        return $this->HasMany(Product::class, 'category_id')
            ->where('products.status', 1);
    }


    public function storeProducts()
    {
        /*return $this->HasMany(Product::class, 'category_id')
            ->where('products.status', 1);*/

        return $this->hasManyThrough(Product::class, Store::class)
            ->where('products.status', 1);
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\PageTranslation', 'page_id', 'id');
    }

    public function tr()
    {
        return $this->hasOne(PageTranslation::class, 'page_id')
            ->where('lang', app()->getLocale());
    }

    public function registerMediaConversions(Media $media = null)
    {
        /*$this->addMediaConversion('default-sm')
            ->fit('crop', 400, 293)
            ->optimize()
            ->performOnCollections('default');*/
    }


    public static function productCategories($lang)
    {
        return Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pages.template_id', 5)
            ->where('pages.visible', '<>', 0)
            ->where('pt.lang', $lang)
            ->whereNull('pt.deleted_at')
            ->visible()
            ->select('pages.id', 'pages.parent_id', 'pt.name', 'pt.slug', 'pages.target')
            ->orderBy('pages.order', 'asc')
            ->withCount('menuChildren')
            ->with('children');
    }


    public static function mainCategories($lang)
    {
        return self::productCategories($lang)
            ->with('media')
            ->withCount('products')
            ->get();
    }


    public static function categoriesFooter($lang)
    {
        return self::productCategories($lang)
            ->withCount('children')
            ->get();
    }
}