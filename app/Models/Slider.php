<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static function rules($required){

        $required == true ? $validate = 'required' : $validate = 'sometimes';
        return [
            'title' => 'nullable',
            'image' => $validate.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=1920,min_height=300',
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə edilməyib.",
    ];
}
