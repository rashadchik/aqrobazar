<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleTranslation extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value ?? $this->attributes['title']);
    }

    public static function rules($id){
        return [
            'title' => 'required|max:255',
            'slug' => 'unique:article_translations,slug,'.$id.',id',
            'summary' => 'nullable',
            'content' => 'nullable',
        ];
    }

    public static $messages = [
        'title.required' => "Ad qeyd olunmayıb.",
    ];


    public function relatedPages()
    {
        return $this->hasMany(ArticleTranslation::class, 'article_id', 'article_id');
    }
}
