<?php

namespace App\Models;

use App\Logic\FullTextSearchTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use DB;

class Product extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes, FullTextSearchTrait;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    const editableStatus = [0, 1];

    protected $searchable = [
        'products.name',
        'products.slug',
        'products.summary'
    ];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull', '.');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull', '.');
    }

    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }


    public function setCategoryIdAttribute($value)
    {
        if (request()->property_id > 0) {
            $this->attributes['category_id'] = request()->property_id;
        }
        else {
            $this->attributes['category_id'] = $value;
        }
    }

    public function setSummaryAttribute($value)
    {
        $this->attributes['summary'] = nl2br($value);
    }


    public static $rules = [
        'name' => "required",
        'slug' => 'required',
        'category_id' => 'nullable|exists:pages,id',
        'city_id' => 'sometimes|exists:cities,id',
        'status' => 'required',
        'price' => 'required|min:0.01|max:1000000',
        'summary' => 'nullable'
    ];


    public static $messages = [
        'image.required' => "Şəkil əlavə olunmayıb",
        'image.required_without' => "Şəkil əlavə olunmayıb",
        'page_id.required' => 'Kateqoriya seçilməyib',
        'bar_code.required' => 'Barkod qeyd olunmayıb',
        'bar_code.unique' => 'Barkod başqa məhsulda istifadə olunub'
    ];


    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\ProductTranslation', 'product_id', 'product_id');
    }

    public function scopeByCity($query, $value)
    {
        if ($value > 0) {
            $query->where('products.city_id', $value);
        }
    }

    public function scopeByType($query, $value)
    {
        if ($value > 0) {
            $query->where('products.type', $value);
        }
    }

    public function scopeByPrice($query, $minPrice, $maxPrice)
    {
        if($minPrice > 0 && $maxPrice > 0 & $minPrice <= $maxPrice){
            return $query->whereBetween('products.price', [$minPrice, $maxPrice]);
        }
        elseif($maxPrice > 0){
            return $query->where('products.price', '<=', $maxPrice);
        }
        else{
            return $query->where('products.price', '>=', $minPrice);
        }
    }

    public function scopeByCategories($query, $value, $pageId, $other = false)
    {
        if($other == false) {
            $query->whereIn('products.category_id', $value);
        }
        else {
            $query->where('products.category_id', $pageId);
        }
    }

    public function scopeSearchByText($query, $keyword)
    {
        if (strlen(trim($keyword)) > 1) {
            return $query->search($keyword);
        }
    }

    public function bookmarks()
    {
        $bookmark = new Favourite();

        return $this->HasMany(Favourite::class, 'product_id')
            ->where(function ($query) use($bookmark) {
                return $bookmark->get($query);
            });
    }


    public function hasBookmark()
    {
        return $this->hasOne(Favourite::class, 'product_id');
    }


    public function getPaid($type)
    {
        return $this->hasOne(VipProduct::class, 'product_id')->where('type', $type);
    }

    public function getCity()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function getStore()
    {
        return $this->belongsTo(Store::class, 'store_id')
            ->withCount('getProductCount');
    }

    public function category()
    {
        return $this->belongsTo(Page::class, 'category_id');
    }

    public function getPageTrans()
    {
        return $this->belongsTo(PageTranslation::class, 'category_id', 'page_id')
            ->where('lang', app()->getLocale());
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('lg')
            ->width(800)
            ->watermark(public_path('images/watermark.png'))
            ->watermarkWidth(300)
            ->watermarkPosition(Manipulations::POSITION_CENTER)
            ->optimize()
            ->performOnCollections('default');

        $this->addMediaConversion('md')
            ->fit('crop', 360, 320)
            ->optimize()
            ->performOnCollections('default');

        $this->addMediaConversion('sm')
            ->fit('crop', 138, 135)
            ->optimize()
            ->performOnCollections('default');

    }


    public static function get($lang, $status = 1, $raw = 1, $bookmark = true)
    {
        $products = Product::join('pages as p', 'p.id', '=', 'products.category_id')
            ->join('page_translations as pt', 'pt.page_id', '=', 'p.id')
            ->leftJoin('cities', 'cities.id', '=', 'products.city_id')
            ->leftJoin('countries', 'countries.id', '=', 'cities.country_id')
            ->where('pt.lang', $lang)
            ->where('products.status', $status)
            ->whereNull('pt.deleted_at')
            ->with('media')
            ->select(
                'products.id',
                'products.name',
                'products.type',
                'products.pin_code',
                'products.status',
                'products.slug',
                'products.price',
                'products.is_vip',
                'products.created_at',
                'pt.slug as page_slug',
                'p.parent_id',
                'cities.name_'.$lang.' as city',
                'countries.prefix as country',
                DB::raw("$raw as vip_type")
            );

        if($bookmark == true){
            $products->withCount('bookmarks');
        }

        if($raw == 2) {
            $products->inRandomOrder();
        }
        else {
            $products->orderBy('is_vip', 'desc')
                ->orderBy('is_fav', 'desc')
                ->orderBy('products.order', 'desc');
        }

        return $products;
    }
}
