<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Store extends Model implements HasMedia
{
    const listTitle = 'Mağaza və Şirkətlər';

    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules($id = null)
    {
        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:stores,slug,'.$id,
            'file' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=600,min_height=600',
            'cover' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=1100,min_height=293',
            'phone' => 'required',
            'status' => 'boolean',
            'city_id' => 'required',
            //'location' => 'required',
            'address' => 'required',
            'summary' => 'nullable',
        ];
    }


    public static $parameterRules = [
        'name' => "required",
        'slug' => 'unique:stores,slug',
        'file' => 'sometimes|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=600,min_height=600',
        'cover' => 'sometimes|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=1100,min_height=293',
        'phone' => 'required',
        'status' => 'boolean',
        'city_id' => 'required',
        //'location' => 'required',
        'address' => 'required',
    ];


    public static $messages = [

    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull', '.');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull', '.');
    }

    public function getBalanceAttribute($value)
    {
        return $value / 100;
    }

    public function setBalanceAttribute($value)
    {
        $this->attributes['balance'] = $value * 100;
    }

    public function getAddress()
    {
        return $this->hasOne(StoreAddress::class, 'store_id', 'id')->where('default', 1);
    }

    public function getTrans()
    {
        return $this->hasOne(StoreTranslation::class, 'store_id', 'id');
    }


    public static function getStores($random = false)
    {
        $query =  Store::join('store_translations as st', 'st.store_id', '=', 'stores.id')
            //->join('store_addresses as sa', 'sa.store_id', '=', 'stores.id')
            ->where('stores.status', 1)
            ->select('stores.name', 'stores.slug', 'stores.id', 'st.summary', 'stores.phone')
            ->groupBy('stores.id')
            ->with('media')
            ->withCount('getProductCount');

        $random == false ? $query->orderBy('stores.id', 'desc') : $query->inRandomOrder();

        return $query;
    }

    public static function lists()
    {
        return Store::orderBy('name', 'asc')->pluck('name', 'id');
    }

    public static function getStoreCategories($storeId)
    {
        return Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.parent_id')
            ->join('products', function ($join) {
                $join->on('products.category_id', '=', 'pages.id')
                ->orOn('products.category_id', '=', 'pages.parent_id');
            })
            ->where('products.store_id', $storeId)
            ->whereNull('products.deleted_at')
            ->whereNull('pt.deleted_at')
            ->visible()
            ->select('pages.id', 'pt.name', 'pt.slug')
            ->orderBy('pages.order', 'asc')
            ->groupBy('pt.id')
            ->get();
    }

    public function getProductCount()
    {
        return $this->HasMany(Product::class, 'store_id')
            ->where('products.status', 1);
    }

    public function relatedPages()
    {
        return $this->hasMany(StoreTranslation::class, 'store_id', 'id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('md')
            ->fit('crop', 600, 600)
            ->optimize()
            ->performOnCollections('logo');

        $this->addMediaConversion('sm')
            ->fit('crop', 300, 300)
            ->optimize()
            ->performOnCollections('logo');

        $this->addMediaConversion('xs')
            ->fit('crop', 150, 150)
            ->optimize()
            ->performOnCollections('logo');


        $this->addMediaConversion('lg')
            ->fit('crop', 940, 250)
            ->optimize()
            ->performOnCollections('cover');

        $this->addMediaConversion('md')
            ->fit('crop', 700, 186)
            ->optimize()
            ->performOnCollections('cover');

    }
}
