<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTranslation extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static $rules = [
        'summary' => "required",
    ];

    public static $messages = [
        'summary.required' => "Qısa məlumat qeyd olunmayıb.",
    ];

    public function getStore()
    {
        return $this->hasOne(Store::class, 'id', 'store_id');
    }

    public function relatedPages()
    {
        return $this->hasMany(self::class, 'store_id', 'store_id');
    }
}
