<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Cookie;
use DB;

class Banner extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull', '.');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull', '.');
    }

    public static $rules = [
        'title' => 'required',
        'file' => 'required',
        'device' => 'required',
        'link' => 'nullable',
        'width' => 'nullable',
        'height' => 'nullable',
        'status' => 'boolean',
        'type' => 'required'
    ];

    public static $messages = [

    ];

    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }


    public function scopeSelectData($query)
    {
        return $query->select('id', 'type', 'device', 'width', 'height', 'link', 'title', 'viewed');
    }


    public function scopeUnions($query, $group)
    {
        foreach ($group as $gr){
            $query->unionAll($gr);
        }

        return $query;
    }


    public static function fetch($device, $loadedBanners = null, $storeId = false)
    {
        /*
         *  1 => top banner
         *  2 => top banner sag (Desktop)
         *  4 => sol (Desktop)
         *  5 => sag (Desktop)
         *  6 => sag, sol (Desktop)
         */

        if (is_null($loadedBanners)) {
            $loadedBanners = [
                1 =>  [2, 4, 5, 6],
                2 =>  [2]
            ];
        }

        $prefix = config("config.prefix");
        $bannerQueries = [];
        $bannerData = [];
        $bannerId = [];

        if(!array_key_exists($device, $prefix)){
            $device = key($prefix);
        }

        $bannerDevice = $prefix[$device];
        $storeBannerTypes = [];

        if ($storeId) {
            $storeBannerTypes = Banner::where('store_id', $storeId)->where('device', $bannerDevice)->pluck('type')->toArray();
        }

        foreach ($loadedBanners[$bannerDevice]  as $bannerType){
            $bannerQueries[] = Banner::where('type', $bannerType)
                ->where('device', $bannerDevice)
                ->where(function ($query) use($bannerType, $storeBannerTypes, $storeId) {
                    if (in_array($bannerType, $storeBannerTypes)) {
                        $query->where('store_id', '=', $storeId);
                    }
                    else {
                        $query->whereNull('store_id');
                    }
                })
                ->status()
                ->orderBy('viewed', 'asc')
                ->selectData()
                ->limit(1);
        }

        $banners = Banner::where('type', 1)
            ->where('device', $bannerDevice)
            ->where(function ($query) use($storeBannerTypes, $storeId) {
                if (in_array(1, $storeBannerTypes)) {
                    $query->where('store_id', '=', $storeId);
                }
                else {
                    $query->whereNull('store_id');
                }
            })
            ->status()
            ->selectData()
            ->orderBy('viewed', 'asc')
            ->limit(1)
            ->unions($bannerQueries)
            ->with('media')
            ->get();


        $pluckedBanners = $banners->pluck('viewed', 'type');

        // group banners after fetching
        $unsetBanners = [];

        if (isset($pluckedBanners[6])) {

            if (isset($pluckedBanners[5]) && isset($pluckedBanners[4])) {
                if (($pluckedBanners[6] < $pluckedBanners[4]) && ($pluckedBanners[6] < $pluckedBanners[5])) {
                    $unsetBanners = [4,5];
                }
                else {
                    $unsetBanners[] = 6;
                }
            }
            else {
                if (isset($pluckedBanners[5]) && !isset($pluckedBanners[4]) && $pluckedBanners[5] > $pluckedBanners[6]) {
                    $unsetBanners[] = 5;
                }
                elseif (isset($pluckedBanners[5]) && !isset($pluckedBanners[4]) && $pluckedBanners[5] <= $pluckedBanners[6]) {
                    $unsetBanners[] = 6;
                }

                if (isset($pluckedBanners[4]) && !isset($pluckedBanners[5]) && $pluckedBanners[4] > $pluckedBanners[6]) {
                    $unsetBanners[] = 4;
                }
                elseif (isset($pluckedBanners[4]) && !isset($pluckedBanners[5]) && $pluckedBanners[4] <= $pluckedBanners[6]) {
                    $unsetBanners[] = 6;
                }
            }
        }

        foreach($banners as $banner)
        {
            if (!in_array($banner->type, $unsetBanners)) {
                $bannerData[$banner->type] = $banner;
                $bannerId[] = $banner->id;
            }

            // popup banner

            /*if (request()->cookie('popup'.$banner['id']) == null && $banner['type'] == 14){
                Cookie::queue('popup'.$banner['id'], str_random(60), 180);

                Banner::where('type', $banner['type'])
                    ->where('id', $banner['id'])
                    ->update(['viewed' => DB::raw('viewed + 1'), 'public_viewed' => DB::raw('public_viewed + 1')]);
            }*/
        }

        if(!empty($bannerId)){
            Banner::whereIn('id', array_unique($bannerId))
                //->where('type', '<>', 14)
                ->update(['viewed' => DB::raw('viewed + 1'), 'public_viewed' => DB::raw('public_viewed + 1')]);
        }

        return $bannerData;
    }
}
