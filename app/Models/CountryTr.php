<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryTr extends Model
{
    protected $guarded = ['id'];

    public static $rules = [
        'name.az' => 'required',
        'name.*' => 'sometimes',
    ];

    public static $messages = [
        'name.az.required' => 'Ad qeyd olunmayıb',
    ];

    public static function getLists($prependText = false, $where = false)
    {
        $lists = self::join('countries as c', 'c.id', '=', 'country_trs.country_id')
            ->whereNull('c.deleted_at')
            ->where('c.published', 1)
            ->where('lang', app()->getLocale())
            ->orderBy('name','asc');

        if($where) {
            $lists = $lists->where($where);
        }

        $lists = $lists->pluck("name", "country_id");

        if($prependText) {
            $lists ->prepend($prependText, '');
        }

        return $lists;
    }

}
