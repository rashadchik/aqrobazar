<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Article extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'title' => "required",
            'slug' => 'unique:article_translations,slug,'.$id,
            'file' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=800',
            'published_at' => 'required|date',
            'status' => 'boolean',
            'featured' => 'boolean',
            'summary' => 'nullable',
            'content' => 'nullable',
        ];
    }

    public static $parameterRules = [
        'published_at' => 'required|date',
        'status' => 'boolean',
        'featured' => 'boolean',
        'file' => 'sometimes|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=800'
    ];

    public static $messages = [
        'image.required_without' => "Şəkil əlavə olunmayıb",
        'page_id.required' => 'Kateqoriya seçilməyib'
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function relatedPages()
    {
        return $this->hasMany(ArticleTranslation::class, 'article_id', 'id');
    }

    public function trans()
    {
        return $this->hasOne(ArticleTranslation::class, 'article_id')->orderBy('article_translations.id', 'asc');
    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('lg')
            ->width(1024)
            ->optimize()
            ->performOnCollections('default');

        $this->addMediaConversion('md')
            ->fit('crop', 500, 328)
            ->optimize()
            ->performOnCollections('default');

        $this->addMediaConversion('sm')
            ->fit('crop', 300, 195)
            ->optimize()
            ->performOnCollections('default');

        $this->addMediaConversion('xs')
            ->fit('crop', 200, 131)
            ->optimize()
            ->performOnCollections('default');



        $this->addMediaConversion('lg')
            ->width(1024)
            ->optimize()
            ->performOnCollections('gallery');

        $this->addMediaConversion('md')
            ->fit('crop', 800, 710)
            ->optimize()
            ->performOnCollections('gallery');

        $this->addMediaConversion('sm')
            ->fit('crop', 400, 355)
            ->optimize()
            ->performOnCollections('gallery');

        $this->addMediaConversion('xs')
            ->fit('crop', 200, 177)
            ->optimize()
            ->performOnCollections('gallery');
    }
}

