<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Favourite extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at'];
    public $timestamps = false;

    public function setUpdatedAtAttribute($value)
    {
        // Do nothing.
    }

    public static $rules = [
        'product' => 'required'
    ];

    public static $messages = [

    ];


    public function scopeWhereCondition($query)
    {

        if(is_null($this->user())){

            return $this->scopeGetBookmarks($query);
        }
        else{
            return $query->where(function ($query) {
                return $this->get($query);
            });
        }
    }


    public function scopeGetBookmarks($query)  //get products by only cookie, if user not authenticated
    {
        return $query->where('cookie', $this->getCookie())->whereNotNull('cookie');
    }


    public function get($query)
    {
        if(is_null($this->user())){
            $this->scopeGetBookmarks($query);
        }
        else{
            return $query->where('favourites.user_id', $this->user())->orWhere('favourites.cookie', $this->getCookie());
        }
    }


    private function getCookie()
    {
        return request()->cookie('bookmarkCookie');
    }


    private function user()
    {
        Auth::guard('web')->check() ? $userId = Auth::guard('web')->user()->id : $userId = null;

        return $userId;
    }

}
