<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at'];
    public $timestamps = false;

    public function setUpdatedAtAttribute($value)
    {
        // Do nothing.
    }
}
