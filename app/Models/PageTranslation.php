<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageTranslation extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];


    public static function rules($lang, $id){

        return [
            'name' => 'required|max:255',
            'parent_id' => 'nullable|exists:pages,id',
            'content' => 'nullable',
            'forward_url' => 'nullable',
            'meta_description' => 'nullable|max:160',
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
    ];


    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }


    public function relatedPages()
    {
        return $this->hasMany('App\Models\PageTranslation', 'page_id', 'page_id');
    }


    public function articles($paginate = false, $limit = 3)
    {
        $articles = $this->hasMany('App\Models\ArticleTranslation', 'page_id', 'id')
            ->join('articles', 'articles.id', '=', 'article_translations.article_id')
            ->select('article_translations.name', 'article_translations.slug', 'article_translations.summary', 'articles.image', 'articles.published_at')
            ->where('articles.status', 1)
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc');

        if($paginate == true){
            $articles->paginate($limit);
        }
        else{
            $articles->limit($limit);
        }

        return $articles;
    }
}
