<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Dictionary extends Model
{
    protected $table = 'dictionaries';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public static $rules = [
        'keyword' => 'required|string',
        'content' => 'nullable',
    ];


    public static $messages = [
        'keyword.required' => 'Açar sözü düzgün yazın',
    ];


    public static function getLists($where = false)
    {
        $lists = self::select('keyword', 'content')->orderBy('keyword', 'asc');

        if($where) {
            $lists = $lists->where($where);
        }

        $cachedLists = Cache::rememberForever('dictionary_'.app()->getLocale(), function () use($lists) {
            return $lists->pluck('content', 'keyword');
        });


        return $cachedLists;
    }
}
