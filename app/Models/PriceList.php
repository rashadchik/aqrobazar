<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
    protected $fillable = ['amount', 'note', 'type', 'day'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static $rules = [
        'type' => 'required|numeric',
        'amount' => 'required|numeric|min:1|price_list_amount',
        'note' => 'required'
    ];


    public static $messages = [
        'required' => 'Bütün xanaların doldurulması zəruridir!',
        'price_list_amount' => '"Elanı irəli çək" üçün nəzərdə tutulan maksimal ödəniş məbləği 12 AZN-dən çox olmamalıdır',
    ];
}
