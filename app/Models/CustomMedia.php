<?php

namespace App\Models;

use Spatie\MediaLibrary\Models\Media;

class CustomMedia extends Media
{

    public $table = 'media';

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public static $rules = [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp|max:10000|dimensions:min_width=1024,min_height:300'
    ];

    public static $messages = [
        'file.required' =>  "Şəkil əlavə olunmayıb",
        'file.mimes' => 'Şəkil :values formatda yüklənməlidir',
    ];
}
