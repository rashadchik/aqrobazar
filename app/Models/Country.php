<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static $rules = [
        'prefix' => 'required',
        'c_code' => 'required|numeric',
        'published' => 'boolean'
    ];

    public static function getCountryCodes()
    {
        return Country::where('published', 1)->select('prefix')->pluck('prefix')->toJson();
    }

    public function tr()
    {
        return $this->hasOne(CountryTr::class, 'country_id')
            ->where('lang', app()->getLocale());
    }

    public function cities()
    {
        $locale = app()->getLocale();

        return $this->hasMany(City::class,'country_id')
            ->select("name_$locale as name", 'id', 'country_id')
            ->orderBy("name_$locale", 'asc');
    }
}
