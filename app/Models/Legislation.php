<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Legislation extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static $rules = [
        'title' => 'required',
        'file' => 'sometimes|mimes:pdf|max:10000'
    ];

    public static $messages = [
        'title' => 'Başlıq qeyd olunmayıb',
    ];
}
