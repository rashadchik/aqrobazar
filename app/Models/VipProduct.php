<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VipProduct extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getStartDateAttribute($value)
    {
        return filterDate($value, false, 'full');
    }

    public function getEndDateAttribute($value)
    {
        return filterDate($value, false, 'full');
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class, 'product_id');
    }
}
