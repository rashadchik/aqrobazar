<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = ['id'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static $rules = [
        'name.az' => 'required',
        'name.*' => 'sometimes',
    ];

    public static $messages = [
        'name.az.required' => 'Ad qeyd olunmayıb',
    ];


    public function setNameAttribute($value)
    {
        foreach ($value as $k => $v)
        {
            if($v == ''){
                $this->attributes['name_'.$k] = $value['az'];
            }
            else{
                $this->attributes['name_'.$k] = $v;
            }
        }
    }

    public static function getCities()
    {
        return self::orderBy('name_az', 'id')->pluck('name_az', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id')
            ->with('tr');
    }
}
