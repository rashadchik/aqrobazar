<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\Models\Media;

class AddWatermark extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:watermark';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adding watermark to product images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = 20;

        $aggregator = DB::table('cron_aggregators')->first();

        if(!$aggregator) {
            DB::table('cron_aggregators')->insert(
                [
                    'last_id' => 0
                ]
            );

            $aggregator = DB::table('cron_aggregators')->first();
        }

        $products = Product::where('id', '>', $aggregator->last_id)
            ->select('id')
            ->limit($limit)
            ->orderBy('id', 'asc')
            ->withTrashed()
            ->get();

        //$products = Product::where('id', 150)->select('id')->get();

        if($products->count() > 0) {
            foreach ($products as $product) {

                foreach($product->getMedia() as $img)
                {
                    if(Storage::disk('public')->exists($img->id.'/'.$img->file_name)) {

                        $product->addMediaFromUrl(asset($img->getUrl()))->toMediaCollection();

                        Storage::disk('public')->delete($img->id);
                        Media::where('id', $img->id)->delete();

                        DB::table('cron_aggregators')
                            ->where('id', $aggregator->id)
                            ->update(['last_id' => $product->id]);
                    }
                }

            }
        }

    }
}
