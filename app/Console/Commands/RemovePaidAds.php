<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\VipProduct;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;

class RemovePaidAds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:paid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removing paid ads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $statuses = [
            1 => 'is_fav',
            2 => 'is_vip',
            //3 => 'is_premium'
        ];

        DB::beginTransaction();

        foreach ($statuses as $key => $column) {

            $paidAds = VipProduct::where('end_date', '<=', Carbon::now())
                ->where('type', $key)
                ->pluck('id', 'product_id')
                ->toArray();

            $adsId = array_keys($paidAds);

            if(count($paidAds) > 0) {

                try{
                    VipProduct::destroy($paidAds);
                }
                catch(\Exception $e){
                    DB::rollback();
                }


                try{
                    Product::whereIn('id', $adsId)->update([$column=> 0]);
                }
                catch(\Exception $e){
                    DB::rollback();
                }
            }

        }

        DB::commit();
    }
}
