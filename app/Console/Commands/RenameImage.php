<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\Models\Media;

class RenameImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rename:image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renaming images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = 50;

        $aggregator = DB::table('cron_aggregators')->first();

        if(!$aggregator) {
            DB::table('cron_aggregators')->insert(
                [
                    'last_id' => 0
                ]
            );

            $aggregator = DB::table('cron_aggregators')->first();
        }

        $products = Product::where('id', '>', $aggregator->last_id)
            ->select('id')
            ->limit($limit)
            ->orderBy('id', 'asc')
            ->withTrashed()
            ->get();

        //$products = Product::where('id', 150)->select('id')->get();

        foreach ($products as $product){

            foreach($product->getMedia() as $img) {

                //$extension = pathinfo($path, PATHINFO_EXTENSION); didn't check works or not

                if(strpos($img->file_name, 'jpeg')){
                    $extension = substr($img->file_name, -5);
                }
                else{
                    $extension = substr($img->file_name, -4);
                }

                $fileName = str_replace($extension, '', $img->file_name);
                $hashName = md5($fileName);

                $oldName = $img->id.'/'.$fileName.$extension;
                $newName = $img->id.'/'.$hashName.$extension;

                $oldConversionMdName = $img->id.'/'.'conversions/'.$fileName.'-md.jpg';
                $newConversionMdName = $img->id.'/'.'conversions/'.$hashName.'-md.jpg';

                $oldConversionLgName = $img->id.'/'.'conversions/'.$fileName.'-lg.jpg';
                $newConversionLgName = $img->id.'/'.'conversions/'.$hashName.'-lg.jpg';

                $oldConversionSmName = $img->id.'/'.'conversions/'.$fileName.'-sm.jpg';
                $newConversionSmName = $img->id.'/'.'conversions/'.$hashName.'-sm.jpg';

                if(Storage::disk('public')->exists($oldName) &&
                    Storage::disk('public')->exists($oldConversionMdName) &&
                    Storage::disk('public')->exists($oldConversionLgName) &&
                    Storage::disk('public')->exists($oldConversionSmName) ) {


                    Storage::disk('public')->move($oldName, $newName);
                    Storage::disk('public')->move($oldConversionMdName, $newConversionMdName);
                    Storage::disk('public')->move($oldConversionLgName, $newConversionLgName);
                    Storage::disk('public')->move($oldConversionSmName, $newConversionSmName);

                    Media::where('id', $img->id)->update(['file_name' => $hashName.$extension]);

                    DB::table('cron_aggregators')
                        ->where('id', $aggregator->id)
                        ->update(['last_id' => $product->id]);
                }


                /*if(Storage::disk('public')->exists($newName)) {
                    Storage::disk('public')->move($newName, $oldName);
                }*/
            }

        }
    }
}
