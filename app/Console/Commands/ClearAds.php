<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class ClearAds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:ads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clearing uncompleted ads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $num = 0;
        $products = Product::where('status', 100)->select('id')->get();

        foreach ($products as $product){
            $num++;

            if($num < 100){
                $product->forceDelete();    //this will delete image files
            }
        }
    }
}
