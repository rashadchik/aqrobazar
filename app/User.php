<?php

namespace App;

use App\Models\City;
use App\Models\Store;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public static function rules($id = null){
        return [
            'name' => 'required',
            'email' => 'nullable|email|unique:users,email,'.$id,
        ];
    }


    public static $messages = [
        'required' => 'Bütün xanaların doldurulması zəruridir!',
    ];


    public function registerByFacebook($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();

        if(is_null($check)){
            return static::create($input);
        }

        return $check;
    }

    public function getStore()
    {
        //return $this->hasOne(Store::class, 'id', 'store_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
