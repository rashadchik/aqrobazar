<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use DB;

class SitemapController extends Controller
{
    public function index()
    {
        $date = Config::where('key', 'sitemap')->first();

        return view('admin.sitemap', ['title' => 'Sitemap', 'date' => $date]);
    }


    public function store()
    {
        $config = Config::where('key', 'sitemap')->first();

        // create new sitemap object
        $sitemap = App::make('sitemap');

        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        $sitemap->setCache('laravel.sitemap', 60);
        $lang = 'az';

        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {

            //home page
            $translations = [];

            foreach(config("app.locales") as $locale){
                if($locale != $lang){
                    $translations[] = ['language' => $locale, 'url' => url('/'.$locale)];
                }
            }

            $sitemap->add(url()->to('/'), Carbon::now(), '0.5', 'monthly', [], null, $translations);


            //pages
            $translations = [];

            $pages = DB::table('pages')
                ->join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
                ->whereNull('pt.deleted_at')
                ->select('pt.slug', 'pt.name', 'pt.updated_at', 'pt.lang', 'pages.id', 'pt.page_id')
                ->orderBy('pages.id', 'desc')
                ->get();


            foreach ($pages as $page){

                if($page->lang == $lang){

                    foreach($pages as $transPages){
                        if($page->id == $transPages->page_id && $transPages->lang != $lang){
                            $translations[$page->id][] = ['language' => $transPages->lang, 'url' => url()->to($transPages->lang.'/'.$transPages->slug)];
                        }
                    }

                    $sitemap->add(url()->to($page->slug), $page->updated_at, '0.5', 'monthly', [], $page->name, $translations[$page->id] ?? []);
                }
            }


            //articles
            $translations = [];

            $articles = DB::table('articles as a')
                ->join('article_translations as at', 'at.article_id', '=', 'a.id')
                ->join('page_translations as pt', 'at.page_id', '=', 'pt.id')
                ->leftJoin('media', function ($join) {
                    $join->on('media.model_id', '=', 'a.id')
                        ->where('media.model_type', '=', 'App\Models\Article')
                        ->where('media.collection_name', '=', 'default');
                })
                ->whereNull('a.deleted_at')
                ->whereNull('at.deleted_at')
                ->whereNull('pt.deleted_at')
                ->select('a.id', 'at.slug', 'at.lang', 'media.file_name as image', 'at.title', 'at.summary', 'at.updated_at', 'at.article_id', 'pt.slug as parent')
                ->orderBy('a.id', 'desc')
                ->get();


            foreach ($articles as $article){

                $images = [
                    ['url' => asset("storage/$article->id/$article->image"), 'title' => $article->title, 'caption' => $article->summary],
                ];

                if($article->lang == $lang){

                    foreach($articles as $transArticles){
                        if($article->id == $transArticles->article_id && $transArticles->lang != $lang){
                            $translations[$article->id][] = ['language' => $transArticles->lang, 'url' => url()->to($transArticles->lang.'/'.$transArticles->parent.'/'.$transArticles->slug)];
                        }
                    }

                    $sitemap->add(url()->to($article->parent.'/'.$article->slug), $article->updated_at, '0.5', 'monthly', $images, $article->title, $translations[$article->id] ?? []);
                }
            }


            //products

            $translations = [];

            $products = Product::join('page_translations as pt', 'pt.id', '=', 'products.category_id')
                ->select('products.slug', 'products.name', 'products.updated_at', 'products.summary', 'products.id', 'pt.slug as parent')
                ->with('media')
                ->orderBy('products.id', 'desc')
                ->get();


            foreach ($products as $product){

                $images = [];

                foreach ($product->getMedia() as $img) {
                    $images[] = array(
                        'url' => asset($img->getUrl('lg')),
                        'title' => $product->name,
                        'caption' => $product->summary
                    );
                }

                $sitemap->add(url()->to($product->parent.'/'.$product->slug), $product->updated_at, '0.5', 'monthly', $images, $product->name);
            }
        }

        $sitemap->store('xml', 'storage/sitemap');

        $config->value = Carbon::now();
        $config->save();

        return redirect()->back();
    }
}
