<?php

namespace App\Http\Controllers;

use App\Crud\PaidProductCrud;

class ProductFavouriteController extends Controller
{
    use PaidProductController;

    public $crud;
    public $title = 'İrəli';
    private $route = 'fav';
    private $type = 1;
    private $belongColumn = 'is_fav';

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['show', 'update']]);
        $this->middleware('ajax')->only(['create']);

        $this->crud = new PaidProductCrud();

    }
}
