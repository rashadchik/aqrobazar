<?php

namespace App\Http\Controllers;

use App\Crud\FaqCrud;
use App\DataTables\FaqDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Question;
use Illuminate\Http\Request;
use DB;

class FaqController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'faq';
    public $title = 'FAQ';
    public $name = 'FAQ';
    public $filters = [];
    public $largeModal = false;
    public $createRoute = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new FaqDataTable();
        $this->model = new Question();
        $this->crud = new FaqCrud();
        $this->requests = $request->except('_token', '_method');
    }
}