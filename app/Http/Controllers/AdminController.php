<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Crud\AdminCrud;
use App\DataTables\AdminsDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use Hash;

class AdminController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'admins';
    public $title = 'Adminlər';
    public $name = 'Admin';
    public $filters = ['deleted'];
    public $largeModal = false;
    public $createRoute = true;
    public $rules = 'func';

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'profile', 'updatePhoto']);

        $this->dataTable = new AdminsDataTable();
        $this->model = new Admin();
        $this->crud = new AdminCrud();
        $this->requests = $request->except('_token', '_method');
    }

    public function profile()
    {
        $user = auth()->guard('admin')->user();

        $fields = $this->crud->fields('profile', ['data' => $user]);

        return view('admin.profile', ['fields' => $fields, 'title' => 'Profil', 'user' => $user, 'disabled' => false]);
    }

    public function update(Request $request, $id)
    {
        $model = $this->model;

        $data = $model->findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(), $this->rules($model, $id), $model::$messages);
        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        unset($this->requests['password']);

        if($request->has('password') && trim($request->password) != '')
        {
            $this->requests['password'] = Hash::make($request->password);
        }


        try{
            foreach($this->requests as $key => $put){
                $data->$key = $put;
            }

            $data->save();
        }
        catch(\Exception $e){
            return $this->responseJson(1, $e->getMessage());
        }


        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }

    public function updateProfile(Request $request)
    {
        $user = auth()->guard('admin')->user();

        $data = Admin::findOrFail($user->id);

        $inputs = $request->only('name', 'email');

        $validation = Validator::make($inputs, Admin::rules($user->id), Admin::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        foreach($inputs as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseJson(0, trans('locale.update_success'));
    }


    public function updatePassword(Request $request)
    {
        $user = auth()->guard('admin')->user();

        $inputs = [
            'current_password' => 'required|current_password_match',
            'password'     => 'required|min:6|confirmed',
        ];

        $validation = Validator::make($this->requests, $inputs);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return $this->responseJson(0, trans('locale.update_success'));
    }


    public function updatePhoto(Request $request)
    {
        $user = auth()->guard('admin')->user();

        $inputs = [
            'photo' => 'required|mimes:png,gif,jpeg,jpg,bmp|max:10000|dimensions:min_width=215,min_height=215,max_width:1000,max_height=1000'
        ];

        $request->validate($inputs);

        try{
            $user->clearMediaCollection();  //delete old file
            $user->addMedia($request->photo)
                ->toMediaCollection('avatar');
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }

        return redirect()->back();
    }

}