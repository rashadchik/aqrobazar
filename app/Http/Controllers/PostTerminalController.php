<?php

namespace App\Http\Controllers;

use App\Crud\PostTerminalCrud;
use App\DataTables\PostTerminalDataTable;
use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\PostTerminal;
use Illuminate\Support\Facades\Validator;
use DB;

class PostTerminalController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(Request $request, PostTerminalCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "Post Terminallar";
        $this->requests = $request->except('_token', '_method');
    }


    public function index(PostTerminalDataTable $dataTable)
    {
        return $dataTable->render('admin.post-terminals', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields(null,'create');
        return view('admin.dt.create', ['title' => 'Yeni Post Terminal', 'fields' => $fields, 'route' => 'post-terminal.store']);
    }



    public function store(Request $request)
    {
        // check Validation
        $validation = Validator::make($this->requests, PostTerminal::$rules, PostTerminal::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        PostTerminal::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = PostTerminal::findOrFail($id);

        $fields = $this->crud->fields(null,'edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['post-terminal.update', $id]]);

    }


    public function update(Request $request, $id)
    {
        $data = PostTerminal::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, PostTerminal::$rules, PostTerminal::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


    private function responseSuccess($msg = "", $draw = "#post-terminals", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}
