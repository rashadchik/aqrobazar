<?php

namespace App\Http\Controllers;

use App\Logic\DataTableAction;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\DataTables\SliderDataTable;
use App\Crud\SliderCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    use DataTableAction;

    public $crud, $requests, $dataTable, $model;
    public $route = 'slider';
    public $title = 'Slider';
    public $filters = ['status', 'lang'];
    //public $createRoute = true;
    //public $createRouteViaModal = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new SliderDataTable();
        $this->model = new Slider();
        $this->crud = new SliderCrud();

        $this->requests = $request->except('_token', '_method', 'image');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('slider');
        }
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Slider::rules(true));

        if ($validator->fails()) {
            return $this->responseJson(1, $validator->errors()->first());
        }

        DB::beginTransaction();

        $slider = Slider::create($this->requests);

        $slider
            ->addMedia($request->image)
            ->withResponsiveImages()
            ->toMediaCollection();

        DB::commit();

        return $this->responseJson(0, "$this->title əlavə edildi", $this->route, "myModal");
    }


    public function update(Request $request, $id)
    {
        $img = Slider::findOrFail($id);

        $response = $this->validation(null);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        if($request->hasFile('image')){

            $img->clearMediaCollection();  //delete old file
            $img->addMedia($request->image)
                ->withResponsiveImages()
                ->toMediaCollection();
        }

        foreach($this->requests as $key => $put){
            $img->$key = $put;
        }

        $img->save();

        return $this->responseJson($response);
    }



}