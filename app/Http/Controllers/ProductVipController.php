<?php

namespace App\Http\Controllers;

use App\Crud\PaidProductCrud;

class ProductVipController extends Controller
{
    use PaidProductController;

    public $crud;
    public $title = 'VIP';
    private $route = 'vip';
    private $type = 2;
    private $belongColumn = 'is_vip';

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['show', 'update']]);
        $this->middleware('ajax')->only(['create']);

        $this->crud = new PaidProductCrud();

    }
}
