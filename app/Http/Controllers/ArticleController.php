<?php

namespace App\Http\Controllers;

use App\Crud\ArticleParameterCrud;
use App\Crud\ArticletransCrud;
use App\DataTables\ArticleDataTable;
use App\Logic\DataTableIndex;
use App\Models\Article;
use App\Models\ArticleTranslation;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    use DataTableIndex;

    public $translates = ['title', 'lang', 'slug', 'content', 'summary'];

    public $crud, $paramCrud, $requests, $dataTable, $model;
    public $route = 'articles';
    public $title = 'Xəbərlər';
    public $name = 'Xəbər';
    public $filters = ['deleted', 'lang'];
    public $largeModal = true;
    public $editor = true;
    //public $createRoute = true;
    //public $createRouteViaModal = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'order']);
        $this->middleware('ajax')->except(['index', 'order']);

        $this->dataTable = new ArticleDataTable();
        $this->model = new Article();
        $this->crud = new ArticletransCrud();
        $this->paramCrud = new ArticleParameterCrud();
        $this->requests = $request->except('_token', '_method', 'file');
    }


    public function create()
    {
        $fields = $this->crud->fields('create', ['meta' => $this->meta ?? false]);

        return view("admin.dt.create",  ['fields' => $fields, 'title' => "Yeni $this->name", 'route' => "$this->route.store", 'editor' => $this->editor ?? false, 'meta' => $this->meta ?? false]);
    }



    public function edit($id)
    {
        $parameterCrud = $this->paramCrud;
        $fields = [];
        $langs = [];

        $page = Article::join('article_translations as at', 'at.article_id', '=', 'articles.id')
            ->where('at.id', $id)
            ->whereNull('at.deleted_at')
            ->select(
                'at.*',
                'at.id as tid',
                'articles.id',
                'articles.status',
                'articles.published_at'
            )
            ->firstOrFail();

        /*if(count(config('app.locales')) == 1){
            return $this->singleEdit($page);
        }*/

        $relatedPage = $page->relatedPages;
        $parameters = $parameterCrud->fields('edit', ['data' => $page]);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields('edit', ['data' => $rel, 'lang' => $rel->lang]);
            $langs[$rel->lang] = $rel->lang;
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields('edit', ['lang' => $key]);
        }

        return view('admin.page.edit-withlocale', ['info' => $page, 'langs' => $langs, 'parameters' => $parameters, 'fields' => $fields, 'relatedPage' => $relatedPage, 'route' => $this->route, 'editor' => $this->editor ?? false, 'meta' => $this->meta ?? false]);
    }


    public function store(Request $request)
    {
        $lang = $request->lang;

        // check Validation
        $validation = Validator::make($this->requests, Article::rules($lang), Article::$messages);
        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        //inputs for models
        $pageInputs = array_diff_key($this->requests, array_flip($this->translates));
        $translationInputs = array_diff_key($this->requests, $pageInputs);

        //store
        DB::beginTransaction();


        try{
            $page = Article::create($pageInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }

        try{
            $translationInputs['article_id'] = $page->id;
            ArticleTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }

        if($request->hasFile('file')){

            $page->addMedia($request->file)
                ->toMediaCollection();
        }

        DB::commit();

        return $this->responseJson(0, "$this->name əlavə edildi", $this->route, "myModal");
    }


    public function update(Request $request, $id)
    {
        $page = Article::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, Article::$parameterRules, Article::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        DB::beginTransaction();

        if($request->hasFile('file')){

            $page->clearMediaCollection();  //delete old file
            $page->addMedia($request->file)
                ->toMediaCollection();
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function storeTrans(Request $request)
    {
        // check Validation
        $validation = Validator::make($this->requests, ArticleTranslation::rules(null), ArticleTranslation::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        ArticleTranslation::create($this->requests);

        return $this->responseJson(0, "$this->name əlavə edildi", $this->route, "myModal");
    }


    public function updateTrans(Request $request, $id)
    {
        $page = ArticleTranslation::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, ArticleTranslation::rules($id), ArticleTranslation::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function trash($id)
    {
        $page = ArticleTranslation::findOrFail($id);

        DB::beginTransaction();

        try{
            $page->delete();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }

        DB::commit();

        return $this->responseJson(0, "$this->name silindi, bərpası mümkündür", $this->route, "modal-confirm");
    }


    public function restore($id)
    {
        $page = ArticleTranslation::onlyTrashed()->findOrFail($id);

        DB::beginTransaction();


        try{
            $page->restore();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }

        DB::commit();

        return $this->responseJson(0, "$this->name bərpa edildi", $this->route);
    }


    public function destroy($id)
    {
        $page = ArticleTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = ArticleTranslation::where('article_id', $page->article_id)->withTrashed()->count();

        if($relatedPages == 1){
            Article::where('id', $page->article_id)->forceDelete();
        }
        else{
            $page->forceDelete();
        }

        return $this->responseJson(0, "$this->name birdəfəlik silindi, bərpası mümkün deyil", $this->route, "modal-confirm");
    }
}