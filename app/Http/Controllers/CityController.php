<?php

namespace App\Http\Controllers;

use App\Crud\CityCrud;
use App\DataTables\CityDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\City;
use Illuminate\Http\Request;
use DB;

class CityController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'city';
    public $title = 'Şəhərlər';
    public $name = 'Şəhər';
    public $filters = [];
    public $largeModal = false;
    public $createRoute = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin')->except('getCitiesByCountry');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new CityDataTable();
        $this->model = new City();
        $this->crud = new CityCrud();
        $this->requests = $request->except('_token', '_method');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('city', false);
        }
    }


    public function getCitiesByCountry(Request $request)
    {
        $output = '';
        $lang = app()->getLocale();
        $countryId = (int) $request->value;
        $selected = (int) $request->selected;

        $data = City::where('country_id', $countryId)->select('id', "name_$lang as name")->get();

        if($data->count()) {
            foreach ($data as $row) {
                $selected == $row->id ? $sel = 'selected' : $sel = '';

                $output .= '<option value="'.$row->id.'" '.$sel.'>'.$row->name.'</option>';
            }
        }

        if($request->response) {
            return response()->make($data);
        }
        else{
            return $output;
        }
    }
}