<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use App\Logic\ImageRepo;
use App\Logic\Order;
use Illuminate\Http\Request;
use App\Models\Partner;
use App\DataTables\PartnerDataTable;
use App\Crud\PartnerCrud;
use Illuminate\Support\Facades\Validator;
use DB;

class PartnerController extends Controller
{
    private $crud, $image, $requests, $order, $title;

    public function __construct(Request $request, ImageRepo $imageUpload, PartnerCrud $crud, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->image = $imageUpload;
        $this->crud = $crud;
        $this->title = "Tərəfdaşlar";
        $this->order = $order;
        $this->requests = $request->except('_token', '_method');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('partners', false);
        }
    }


    public function index(PartnerDataTable $dataTable)
    {
        return $dataTable->render('admin.partners', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');
        return view('admin.dt.create', ['title' => 'Yeni tərəfdaş', 'fields' => $fields, 'route' => 'partners.store']);
    }



    public function store(Request $request)
    {
        $validation = Validator::make($this->requests, Partner::rules( 'required|'), Partner::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $upload = $this->image->store($request->image, $this->resize());
        $this->requests['image'] = $upload;

        $this->requests['order'] = Partner::max('order')+1;

        Partner::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Partner::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['partners.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = Partner::findOrFail($id);

        $validation = Validator::make($this->requests, Partner::rules( null), Partner::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        if($request->hasFile('image')){
            $upload = $this->image->store($request->image, $this->resize());
            $this->image->deleteFile($data->image); //delete old file
            $this->requests['image'] = $upload;
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


    public function order()
    {
        return $this->order->get('partners', $this->title, 'name', false, 1);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'partners', false);
    }


    public function destroy($id)
    {
        $data = Partner::findOrFail($id);
        $data->delete();
        $this->image->deleteFile($data->image); //delete image

        return $this->responseSuccess("", "#partners", "#modal-confirm");
    }


    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => false, 'size' => [150, null]], 'thumb' => null ];

        return $resizeImage;
    }

    private function responseSuccess($msg = "", $draw = "#partners", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}
