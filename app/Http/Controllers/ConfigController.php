<?php

namespace App\Http\Controllers;

use App\Crud\ConfigCrud;
use App\DataTables\ConfigDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Config;
use Illuminate\Http\Request;
use DB;

class ConfigController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model;
    public $route = 'config';
    public $title = 'Konfiqurasiya';
    public $name = 'Konfiqurasiya';
    public $filters = [];
    public $largeModal = false;
    public $createRoute = false;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new ConfigDataTable();
        $this->model = new Config();
        $this->crud = new ConfigCrud();
        $this->requests = $request->except('_token', '_method');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('config', false);
        }
    }
}