<?php

namespace App\Http\Controllers;

use App\DataTables\InvoiceDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'invoice';
    public $title = 'Qaimələr';
    public $name = 'Qaimə';
    public $filters = [];
    public $createRoute = false;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'show']);

        $this->dataTable = new InvoiceDataTable();
        $this->model = new Payment();
        $this->crud = false;
        $this->requests = $request->except('_token', '_method');
    }


    public function show($id)
    {
        $invoice = Payment::findOrFail($id);

        return view('admin.invoice-detail', ['invoice' => $invoice, 'title' => 'Qaimə #'.$invoice->id]);
    }


    public function pdf($id)
    {
        $invoice = Order::findOrFail($id);

        $pdfForm = \PDF::loadView('pdf.invoice-detail-pdf', ['invoice' => $invoice, 'title' => 'Sifariş #'.$invoice->id]);

        return $pdfForm->setPaper('a4', 'landscape')->download('Invoice#'.$invoice->id.'.pdf');
    }
}
