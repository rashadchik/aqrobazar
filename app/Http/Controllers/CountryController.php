<?php

namespace App\Http\Controllers;

use App\Crud\CountryCrud;
use App\DataTables\CountryDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Country;
use App\Models\CountryTr;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'country';
    public $title = 'Ölkələr';
    public $name = 'Ölkə';
    public $filters = ['deleted'];
    public $largeModal = false;
    public $createRoute = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new CountryDataTable();
        $this->model = new Country();
        $this->crud = new CountryCrud();
        $this->requests = $request->except('_token', '_method', 'name');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('country', false);
        }
    }


    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), Country::$rules);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }


        DB::beginTransaction();


        $country = Country::create([
            'prefix' => $request->prefix,
            'c_code' => $request->c_code,
            'published' => $request->published
        ]);

        foreach ($request->name as $lang => $name) {
            CountryTr::create([
                'country_id' => $country->id,
                'lang' => $lang,
                'name' => $name
            ]);
        }

        DB::commit();

        return $this->responseJson(0, "$this->name əlavə edildi", $this->route, "myModal");
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), Country::$rules);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        $country = Country::findOrfail($id);


        DB::beginTransaction();


        try{
            foreach($this->requests as $key => $put){
                $country->$key = $put;
            }

            $country->save();
        }
        catch(\Exception $e){
            return $this->responseJson(1, $e->getMessage());
        }


        foreach ($request->name as $lang => $name) {
            CountryTr::where('country_id', $country->id)->update(['name' => $name]);
        }


        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }
}