<?php

namespace App\Http\Controllers;

use App\Crud\PriceListCrud;
use App\DataTables\PriceListDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\PriceList;
use Illuminate\Http\Request;
use DB;

class PriceListController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'price';
    public $title = 'Price List';
    public $name = 'Qiymət';
    public $filters = [];
    public $largeModal = false;
    public $createRoute = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new PriceListDataTable();
        $this->model = new PriceList();
        $this->crud = new PriceListCrud();
        $this->requests = $request->except('_token', '_method');
    }
}