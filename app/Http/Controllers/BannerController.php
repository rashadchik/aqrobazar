<?php

namespace App\Http\Controllers;

use App\Crud\BannerCrud;
use App\DataTables\BannerDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Banner;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'banner';
    public $title = 'Bannerlər';
    public $name = 'Banner';
    public $filters = [];
    public $largeModal = false;
    public $createRoute = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin')->except(['show']);
        $this->middleware('ajax')->except(['index', 'show']);

        $this->dataTable = new BannerDataTable();
        $this->model = new Banner();
        $this->crud = new BannerCrud();
        $this->requests = $request->except('_token', '_method', 'file');
    }


    public function show($id)
    {
        $banner = Banner::findOrFail($id);

        $path = $banner->getFirstMediaUrl();

        if(getExtension($path) == 'html') {
            $html = file_get_contents($path);
        }
        else {
            $html = '<img src="'.asset($path).'" style="max-width:100%">';
        }

        return $html;
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Banner::$rules);

        if ($validator->fails()) {
            return $this->responseJson(1, $validator->errors()->first());
        }

        if ($request->type == 6) {
            $max = Banner::whereIn('type', [4, 5, $request->type])->status()->where('device', $request->device)->max('viewed');
        }
        else {
            $max = Banner::whereIn('type', [$request->type, 6])->status()->where('device', $request->device)->max('viewed');
        }

        DB::beginTransaction();

        try{
            $this->requests['viewed'] = $max ?? 0;

            $banner = Banner::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }

        $banner->addMedia($request->file)
            ->toMediaCollection();

        DB::commit();

        return $this->responseJson(0, "$this->name əlavə edildi", $this->route, "myModal");
    }


    public function update(Request $request, $id)
    {
        $find = Banner::findOrFail($id);

        Banner::$rules['file'] = 'sometimes';

        $validator = Validator::make($request->all(), Banner::$rules);

        if ($validator->fails()) {
            return $this->responseJson(1, $validator->errors()->first());
        }

        if ($request->type == 6) {
            $max = Banner::whereIn('type', [4, 5, $request->type])->status()->where('device', $request->device)->max('viewed');
        }
        else {
            $max = Banner::whereIn('type', [$request->type, 6])->status()->where('device', $request->device)->max('viewed');
        }

        if($find->getOriginal('status') == 0 && $request->status == 1)
        {
            $this->requests['viewed'] = $max ?? $find->viewed;
        }


        DB::beginTransaction();


        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }
        }
        catch(\Exception $e){
            DB::rollBack();
            return $this->responseJson(1, $e->getMessage());
        }

        $find -> save();


        if($request->has('file')) {

            try{
                $find->clearMediaCollection();  //delete old file
                $find->addMedia($request->file)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->responseJson(1, $e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }
}