<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use Spatie\MediaLibrary\Models\Media;

class UserProductUploadController extends Controller
{

    public function upload(Request $request)
    {
        $photoLimit = 20;
        $videoLimit = 1;

        if($request->method_option == 'POST'){
            $model = Product::where('token', $request->token)->findOrFail($request->model_id);
        }
        else{
            $model = Product::findOrFail($request->model_id);
        }

        $rules = [
            'file' => 'required|mimes:png,gif,jpeg,jpg,mp4|max:31000'
        ];

        $messages = [
            'file.mimes' => 'Fayl :values formatda yüklənməlidir',
        ];


        $validator = Validator::make(request()->all(), $rules, $messages);

        if ($validator->fails()) {
            $response = ['error' => true, 'message' => $validator->messages()->first(), 'code' => 400];
            return  response()->json($response, 400);
        }

        $count = $model->getMedia()->count();

        if($count >= $photoLimit){
            $response = ['error' => true, 'message' => 'Şəkil limiti: '.$photoLimit, 'code' => 400];

            return  response()->json($response, 400);
        }


        if($request->file->getClientOriginalExtension() == 'mp4'){

            $countVideo = $model->getMedia('video')->count();

            if($countVideo >= $videoLimit){
                $response = ['error' => true, 'message' => 'Video limiti: '.$videoLimit, 'code' => 400];

                return  response()->json($response, 400);
            }

            $media = $model
                ->addMedia($request->file)
                ->toMediaCollection('video');
        }
        else{
            $media = $model
                ->addMedia($request->file)
                ->usingFileName(hashImage($request->file))
                ->toMediaCollection();
        }


        $response = ['error' => false, 'message' => 'Success', 'deleteRoute' => route('file.delete'), 'file_id' => $media->id, 'code' => 200];

        return response()->json($response, $response['code']);
    }


    public function deleteFile(Request $request)
    {
        $media = Media::findOrFail($request->id);

        $media->delete();

        $response = ['error' => false, 'message' => 'Şəkil silindi', 'code' => 200];

        return response()->json($response, $response['code']);
    }


    public function get($modelId)
    {
        $product = Product::findOrFail($modelId);

        $imageAnswer = [];

        foreach ($product->getMedia() as $image) {
            $imageAnswer[] = [
                'file_id' => $image->id,
                'path' => $image->getUrl('sm'),
                'deleteRoute' => route('file.delete')
            ];
        }

        if($product->getFirstMedia('video')){
            $imageAnswer[] = [
                'file_id' => $product->getFirstMedia('video')->id,
                'path' => '',
                'deleteRoute' => route('file.delete')
            ];
        }

        $response = ['error' => false, 'message' => '', 'images' => $imageAnswer, 'deleteRoute' => route('file.delete')];

        return response()->json($response, 200);
    }
}
