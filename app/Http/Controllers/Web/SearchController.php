<?php

namespace App\Http\Controllers\Web;

use App\Logic\Share;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\Product;
use DB;
use Input;
class SearchController extends Controller
{
    use Share;

    public function __construct()
    {
        $this->middleware('web');
        $this->loadData();
    }

    public function search()
    {
        $mainCategories = Page::mainCategories($this->lang);
        $keyword = trim(strip_tags(request()->get('keyword')));

        $city = request()->get('city', null);
        $serviceType = request()->get('service_type', null);
        $minPrice = request()->get('min_price') * 100 ?? 0;
        $maxPrice = request()->get('max_price') * 100 ?? 0;

        $products = Product::get($this->lang)
            ->searchByText($keyword)
            ->byCity($city)
            ->byType($serviceType)
            ->byPrice($minPrice, $maxPrice)
            ->simplePaginate(16);

        if (request()->ajax()) {
            $view = view('web.elements.products-ajax', ['products' => $products] )->render();
            return response()->json(['html'=>$view]);
        }

        $banners = Banner::fetch($this->device);

        return view("web.search", ['keyword' => $keyword, 'products' => $products->appends(Input::except('page')), 'mainCategories' => $mainCategories, 'banners' => $banners]);
    }
}
