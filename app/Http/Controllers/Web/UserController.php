<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Logic\Share;
use App\Rules\Cellphone;
use Illuminate\Http\Request;
use Hash;

class UserController extends Controller
{
    use Share;

    public function __construct(Request $request)
    {
        $this->middleware('auth:web');
        $this->loadData();
    }


    public function show()
    {
        $user = auth()->guard('web')->user();

        return view('web.user.profile', ['user' => $user]);
    }



    public function update(Request $request)
    {
        $user = auth()->guard('web')->user();

        //$phone = str_replace([' ', '+(994', ')'], ['', '', ''], $request->phone);
        //$prefix = (int) substr($phone, 0, 2);
        //$request['phone'] = $phone;

        $inputs = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->full_phone,
            'city_id' => $request->city,
        ];

        $rules = [
            'name' => 'required|min:3|max:40',
            'email' => 'required|email',
            'phone' => ['nullable'],
            'city_id' => 'nullable|numeric|exists:cities,id',
        ];

        $request->validate($rules);


        $user->where('id', $user->id)->update($inputs);

        request()->session()->flash('success', "Əməliyyat uğurlu alındı");

        return redirect()->back();
    }


    public function updatePassword(Request $request)
    {
        $user = auth()->user();

        $inputs = [
            'current_password' => 'required|current_password_match',
            'password'     => 'required|min:6|confirmed',
        ];

        $messages = [
            'password.min' => 'Şifrə :min simvoldan az olmamalıdır',

        ];
        $request->validate($inputs, $messages);

        $updated = $user->update([ 'password' => Hash::make($request->password) ]);

        if ($updated) {
            request()->session()->flash('success', "Şifrəniz yeniləndi");
        }
        else{
            request()->session()->flash('error', 'Daxili xəta baş verdi');
        }

        return redirect()->back();
    }
}
