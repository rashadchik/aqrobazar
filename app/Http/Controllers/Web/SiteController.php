<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Logic\Share;
use App\Models\Banner;
use App\Models\Config;
use App\Models\Page;
use App\Logic\Pages;
use App\Models\Product;
use App\Models\Store;
use App\Models\Vacancy;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Models\Dictionary;
use Mail;
use App\Mail\ContactForm;

class SiteController extends Controller
{
    use Share, Pages;

    public function __construct()
    {
        $this->middleware('web');
        $this->loadData();
    }


    public function index()
    {
        $vip = Product::get($this->lang, 1, 2)
            ->where('is_vip', 1)
            ->limit($this->device == 'mobile' ? 4 : 8);

        /*$premium = Product::get($this->lang, 1, 3)
            ->where('is_premium', 1)
            ->limit(16);*/

        $products = Product::get($this->lang, 1, 1)
            ->where('is_vip', 0)
            ->limit(32)
            ->unionAll($vip)
            //->unionAll($premium)
            ->get();

        $premiumCount = Product::where('is_premium', 1)->count();
        $vipCount = Product::where('is_vip', 1)->count();

        $stores = Store::getStores(true)->limit($this->device == 'mobile' ? 2 : 4)->get();

        $banners = Banner::fetch($this->device);

        $data = [
            'products' => $products,
            'premiumCount' => $premiumCount,
            'vipCount' => $vipCount,
            'stores' => $stores,
            'banners' => $banners
        ];

        return view('web.index', $data);
    }


    public function all($byUserId = null)
    {
        $mainCategories = Page::mainCategories($this->lang);

        $vipProductsQuery = Product::get($this->lang,1, 2)
            ->where('is_vip', 1)
            ->limit($this->device == 'mobile' ? 4 : 8);

        $productsQuery = Product::get($this->lang)
            ->where('is_vip', 0);

        if(!is_null($byUserId)) {
            $vipProductsQuery->where('user_id', $byUserId);
            $productsQuery->where('user_id', $byUserId);

        }

        $vipProducts = $vipProductsQuery->get();
        $products = $productsQuery->paginate(32);


        if (request()->ajax()) {
            $view = view('web.elements.products-ajax', ['products' => $products] )->render();
            return response()->json(['html'=>$view]);
        }

        $banners = Banner::fetch($this->device);

        return view("web.products", ['products' => $products, 'banners' => $banners, 'mainCategories' => $mainCategories, 'title' => 'Bütün Elanlar', 'vipProducts' => $vipProducts]);
    }


    public function vip()
    {
        $mainCategories = Page::mainCategories($this->lang);

        $vipProducts = Product::get($this->lang,1, 1)
            ->where('is_vip', 1)
            ->limit(32)
            ->get();

        if (request()->ajax()) {
            $view = view('web.elements.products-ajax', ['products' => $vipProducts] )->render();
            return response()->json(['html'=>$view]);
        }

        $banners = Banner::fetch($this->device);

        $data = [
            'vipProducts' => $vipProducts,
            'banners' => $banners,
            'mainCategories' => $mainCategories,
            'title' => 'VIP Elanlar'
        ];

        return view("web.vip-products", $data);
    }


    public function showUserProducts($id)
    {
        return $this->all($id);
    }


    public function showPage($slug1, $slug2 = null, $slug3 = null, $slug4 = null)
    {

        $slugs = array_filter([$slug1, $slug2, $slug3, $slug4]);

        $checkPages = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->whereIn('pt.slug', $slugs)
            ->where('pt.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->orderByRaw('FIELD(pt.slug, "'.end($slugs).'")')
            ->select('pages.template_id', 'pages.visible', 'pt.*', 'pt.id as tid', 'pages.id', 'pages.parent_id')
            ->get();


        $getPage = $checkPages->last();
        $relatedPages = $this->menu->relatedPages($getPage);

        if(count($slugs) != $checkPages->count()){
            if(!is_null($slug2) && $checkPages->count() > 0 && $checkPages->first()->template_id == 4){   //maybe it is an article
                return $this->articleSingle($checkPages, end($slugs));
            }
            elseif(!is_null($slug2) && $checkPages->count() > 0 && $checkPages->first()->template_id == 5){   //maybe it is a product

                if(end($slugs) == 'other') {
                    return $this->products($getPage, $relatedPages, true);
                }
                else{
                    return $this->productSingle($checkPages, end($slugs));
                }
            }
            else{
                return view('errors.404');
            }
        }

        if (!is_null($getPage->forward_url)) {
            return redirect()->away($getPage->forward_url);
        }

        if ($getPage->template_id == 0) {
            return $this->staticPage($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 1) {
            return $this->about($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 3) {
            return $this->contact($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 4) {
            return $this->news($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 5) {
            return $this->products($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 6) {
            return $this->faq($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 7) {
            return $this->legislation($getPage, $relatedPages);
        }
        else{
            return view('errors.404');
        }
    }


    public function postContact(Request $request)
    {
        $dictionary = Dictionary::where('keyword', 'email_sent')->where('lang_id', $this->lang)->first();
        $email = Config::where('key', 'email')->firstOrFail();

        $inputs = [
            'full_name' => 'required|min:2',
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'text' => 'required|min:20'
        ];

        $request->validate($inputs);

        try {
            Mail::to($email->value)->send(new ContactForm($request->all(), 'contact', 'Əlaqə bölməsindən göndərilən məktub'));

            $response = ["code" => 200, "msg" => $dictionary->content];

            return response()->json($response, $response['code']);

        } catch (\Exception $e) {
            throw new DataTableException($e->getMessage());
        }
    }


    public function fetchSubCategory(Request $request)
    {
        $output = '';
        $value = (int) $request->value;
        $selected = (int) $request->selected;

        $data = Page::productCategories($this->lang)->where('pages.parent_id', $value)->get();

        if ($data->count()) {
            foreach ($data as $row) {
                if (in_array($value, Page::subCategories)) {
                    $output .= ' <optgroup label="'.$row->name.'">';

                    foreach ($row->children as $rowChild) {
                        $selected == $rowChild->id ? $sel = 'selected' : $sel = '';
                        $output .= '<option value="'.$rowChild->id.'" '.$sel.'>'.$rowChild->name.'</option>';
                    }

                    $selected == $row->id ? $sel = 'selected' : $sel = '';
                    $output .= '<option value="'.$row->id.'" '.$sel.'>Digər '.mb_strtolower($row->name).'</option>';
                }
                else {
                    $selected == $row->id ? $sel = 'selected' : $sel = '';
                    $output .= '<option value="'.$row->id.'" '.$sel.'>'.$row->name.'</option>';
                }
            }
            if (!in_array($value, Page::subCategories)) {
                $output .= '<option value="">Digər</option>';
            }
        }

        return $output;
    }
}