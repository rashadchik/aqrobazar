<?php

namespace App\Http\Controllers\Web;

use App\Logic\Share;
use App\Http\Controllers\Controller;
use App\Mail\ContactForm;
use App\Models\Banner;
use App\Models\Config;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\Store;
use App\Rules\Cellphone;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Input;
use Mail;

class StoreController extends Controller
{
    use Share;

    public function __construct()
    {
        $this->middleware('web');
        $this->loadData();
    }

    public function all()
    {
        $stores = Store::getStores(true)->paginate(30); //do not shuffle, if total count of stores more than 30, just use paginate without random

        if (request()->ajax()) {
            $view = view('web.elements.stores-ajax', ['stores' => $stores] )->render();
            return response()->json(['html'=>$view]);
        }

        $data = [
            'pageTitle' => Store::listTitle,
            'stores' => $stores,
            'banners' => Banner::fetch($this->device)
        ];

        return view('web.store.all', $data);
    }


    public function show($slug, $cat = false)
    {
        $store = Store::where('slug', $slug)->firstOrFail();

        $productQuery = Product::get($this->lang, 1, 1)
            ->where('products.store_id', $store->id);

        if($cat) {
            $page = PageTranslation::where('slug', $cat)->firstOrfail();
            $children = Page::where('parent_id', $page->id)->orWhere('id', $page->id)->pluck('id')->toArray();
            $productQuery->byCategories($children, $page->id);
        }

        $products = $productQuery->paginate(24);
        $productsCount = $productQuery->count();

        if (request()->ajax()) {
            $view = view('web.elements.products-ajax', ['products' => $products] )->render();
            return response()->json(['html'=>$view]);
        }

        $categories = Store::getStoreCategories($store->id);

        $data = [
            'category' => 'Mağaza və Şirkətlər',
            'store' => $store,
            'categories' => $categories,
            'products' => $products,
            'productsCount' => $productsCount,
            'banners' => Banner::fetch($this->device, null, $store->id)
        ];


        $store->view_count = $store->view_count + 1;
        $store->save();

        return view('web.store.show', $data);
    }


    public function postApply(Request $request)
    {
        $email = Config::where('key', 'contact_email')->firstOrFail();


        //$phone = floatval(filterPhoneOnPost($request->phone));
        //$prefix = (int) substr($phone, 0, 2);

        $inputs = [
            'full_name' => $request->full_name,
            'phone' => $request->phone,
            'email' => $request->email
        ];

        $validations = [
            'full_name' => 'required|min:3',
            'phone' => ['required', 'numeric'],
            'email' => 'required|email',
        ];

        $validation = Validator::make($inputs, $validations);

        if ($validation->fails()) {
            $response = ["code" => 1, "msg" => $request->phone];
            return response()->json($response, 500);
        }

        Mail::to($email->value)->send(new ContactForm($request->all(), 'store-apply', 'Mağaza üçün müraciət'));


        $response = ["code" => 0, "msg" => "Müraciətiniz qeydə alındı, sizinlə əlaqə saxlanılacaq."];

        return response()->json($response, 200);
    }
}
