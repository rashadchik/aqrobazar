<?php

namespace App\Http\Controllers;

use App\Logic\Share;
use App\Models\Banner;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Favourite;
use Cookie;
use DB;
use Auth;

class BookmarkController extends Controller
{
    use Share;

    public function __construct(Request $request)
    {
        $this->middleware('web');
        $this->loadData();
        $this->guard = Auth::guard('web');
    }


    public function index()
    {
        $bookmark = new Favourite();

        $getProducts = Product::get($this->lang, 1, 1, false)
            ->join('favourites', function ($join) use($bookmark) {
                $join->on('favourites.product_id', '=', 'products.id');

                $join->where(function ($query) use($bookmark) {
                    return $bookmark->get($query);
                });
            })
            ->paginate(20);

        if (request()->ajax()) {
            $view = view('web.elements.products-ajax', ['products' => $getProducts] )->render();
            return response()->json(['html'=>$view]);
        }

        $banners = Banner::fetch($this->device);

        return view('web.bookmark-products', ['products' => $getProducts, 'hideSearch' => true, 'title' => null, 'productListTitle' => 'Seçilmiş elanlar', 'bookmark' => true, 'banners' => $banners]);
    }


    public function store(Request $request)
    {
        $product = Product::findOrFail($request->product);

        if($request->cookie('bookmarkCookie') == null){
            $cookie = str_random(60);
            Cookie::queue('bookmarkCookie', $cookie, 30);
        }
        else{
            $cookie = $request->cookie('bookmarkCookie');
        }


        //eger mehsul artiq favorit olunubsa
        $checkProduct = Favourite::where('product_id', $product->id)->whereCondition()->first();

        if($checkProduct){
            return $this->destroy($checkProduct);
        }


        //userin login etmezden evvel favorit mehsullari varsa, onlari tapiriq. Daha sonra bu mehsullara userin id-si daxil olunacaq
        if($this->guard->check()){
            $userId = $this->guard->user()->id;

            $products = Favourite::getBookmarks()->pluck('id')->toArray();
        }
        else{
            $userId = null;
            $products = 0;
        }


        $fields = [
            'user_id' => $userId,
            'product_id' => $product->id,
            'cookie' => $cookie
        ];


        DB::beginTransaction();

        Favourite::create($fields);

        if(is_array($products) && count($products) > 0){
            Favourite::whereIn('id', $products)->update(['user_id' => $userId]);
        }

        DB::commit();

        $response = ["code" => 200, "msg" => "Elan seçilmişlərə əlavə olundu.", 'type' => 1, 'spanMsg' => 'Seçilmişlərdədir'];
        return response()->json($response, $response['code']);

    }


    public function destroy($product)
    {
        $product->delete();

        $response = ["code" => 200, "msg" => "Elan seçilmişlərdən silindi.", 'type' => 2, 'spanMsg' => 'Seçilmişlərə əlavə et'];
        return response()->json($response, $response['code']);
    }
}
