<?php

namespace App\Http\Controllers;

use App\Crud\PageCrud;
use App\Crud\PageTransCrud;
use App\DataTables\PageDataTable;
use App\Logic\DataTableIndex;
use App\Logic\Order;
use App\Logic\PageAction;
use App\Models\Page;
use Illuminate\Http\Request;
use DB;

class PageController extends Controller
{
    use DataTableIndex, PageAction;

    public $crud, $paramCrud, $requests, $dataTable, $model, $order, $translates;
    public $route = 'page';
    public $title = 'Səhifələr';
    public $name = 'Səhifə';
    public $filters = ['deleted', 'lang'];
    public $largeModal = true;
    public $editor = true;
    public $meta = true;
    //public $createRoute = true;
    //public $createRouteViaModal = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'order']);

        $this->dataTable = new PageDataTable();
        $this->model = new Page();
        $this->crud = new PageTransCrud();
        $this->paramCrud = new PageCrud();
        $this->order = new Order();
        $this->requests = $request->except('_token', '_method', 'file');
        $this->translates = ['name', 'lang', 'slug', 'content', 'forward_url', 'meta_description', 'meta_keywords'];

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages');
        }
    }
}