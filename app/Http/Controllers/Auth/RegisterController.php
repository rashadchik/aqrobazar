<?php

namespace App\Http\Controllers\Auth;

use App\Logic\Share;
use App\Rules\Cellphone;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, Share;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/published';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->loadData();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //$data['phone']  = $this->filterPhone($data['phone']);
        //$prefix = (int) substr($data['phone'], 0, 2);

        return Validator::make($data, [
            'name' => ['required','string','max:40'],
            'email' => ['required','email','unique:users'],
            //'phone' => ['required', 'numeric', new Cellphone($prefix)],
            'sex' => ['required','boolean'],
            'password' => ['required','string','min:6','confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //$data['phone']  = $this->filterPhone($data['phone']);

        $newUser =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            //'phone' => $data['phone'],
            'sex' => $data['sex'],
            'password' => Hash::make($data['password']),
        ]);

        return $newUser;
    }


    public function showRegistrationForm()
    {
        return view('auth.custom-register');
    }


    private function filterPhone($val)
    {
        return filterPhoneOnPost($val);
    }
}
