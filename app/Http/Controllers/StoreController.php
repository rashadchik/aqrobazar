<?php

namespace App\Http\Controllers;

use App\Crud\StoreCrud;
use App\Crud\StoreTransCrud;
use App\DataTables\StoreDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Logic\PageAction;
use App\Models\Store;
use App\Models\StoreAddress;
use App\Models\StoreTranslation;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    use DataTableIndex, PageAction;

    public $crud, $transCrud, $requests, $dataTable, $model, $translates, $address, $parameters;
    public $route = 'store';
    public $title = 'Mağazalar';
    public $name = 'Mağaza';
    public $filters = ['deleted'];
    public $largeModal = true;
    public $editor = true;
    public $meta = true;
    public $createRoute = true;
    //public $createRouteViaModal = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new StoreDataTable();
        $this->model = new Store();
        $this->crud = new StoreCrud();
        $this->transCrud = new StoreTransCrud();
        $this->meta = false;

        $this->requests = $request->except('_token', '_method', 'file', 'cover');
        $this->parameters = ['name', 'slug', 'phone', 'balance', 'status', 'catalog_link', 'whatsapp'];
        $this->translates = ['summary'];
        $this->address = ['city_id', 'address', 'schedule'];
        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('store', false);
        }
    }


    public function store(Request $request)
    {
        // check Validation
        $validation = Validator::make($request->all(), Store::rules(), Store::$messages);
        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        $this->requests['slug'] = str_slug($request->slug ?? $request->name );
        $parameters = array_diff_key($this->requests, array_flip(array_merge($this->translates, $this->address)));
        $translations = array_diff_key($this->requests, array_flip(array_merge($this->parameters, $this->address)));
        $addresses = array_diff_key($this->requests, array_flip(array_merge($this->parameters, $this->translates)));

        //store
        DB::beginTransaction();


        try{
            $store = Store::create($parameters);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        try{
            $translations['store_id'] = $store->id;
            $translations['lang'] = 'az';

            StoreTranslation::create($translations);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        try{
            $addresses['store_id'] = $store->id;

            StoreAddress::create($addresses);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        try{
            $store->addMedia($request->file)
                ->toMediaCollection('logo');

            $store->addMedia($request->cover)
                ->toMediaCollection('cover');
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        DB::commit();

        return $this->responseJson(0, "$this->name əlavə edildi", $this->route, "myModal");
    }


    public function edit($id)
    {
        $parameterCrud = $this->crud;
        $fields = [];
        $langs = [];

        $page = Store::join('store_translations as st', 'st.store_id', '=', 'stores.id')
            ->join('store_addresses as sa', 'sa.store_id', '=', 'stores.id')
            ->where('st.id', $id)
            ->select(
                'st.*',
                'st.id as tid',
                'stores.id',
                'stores.status',
                'stores.name',
                'stores.slug',
                'stores.phone',
                'stores.balance',
                'stores.catalog_link',
                'stores.whatsapp',
                'sa.address',
                'sa.city_id',
                'sa.schedule'
            )
            ->firstOrFail();

        $relatedPage = $page->relatedPages;
        $parameters = $parameterCrud->fields('edit', ['data' => $page, 'lang' => true]);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->transCrud->fields('edit', ['data' => $rel, 'lang' => $rel->lang, 'meta' => $this->meta ?? false]);
            $langs[$rel->lang] = $rel->lang;
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->transCrud->fields('edit', ['lang' => $key, 'meta' => $this->meta ?? false]);
        }

        return view('admin.page.edit-withlocale', ['info' => $page, 'langs' => $langs, 'parameters' => $parameters, 'fields' => $fields, 'relatedPage' => $relatedPage, 'route' => $this->route, 'editor' => $this->editor ?? false, 'meta' => $this->meta ?? false]);
    }


    public function update(Request $request, $id)
    {
        $page = Store::join('store_addresses as sa', 'sa.store_id', '=', 'stores.id')
            ->select('stores.*', 'sa.address', 'sa.city_id', 'sa.schedule')
            ->findOrFail($id);

        // check Validation
        Store::$parameterRules['slug'] = Store::$parameterRules['slug'].','.$id;

        $validation = Validator::make($request->all(), Store::$parameterRules, Store::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        $parameters = array_diff_key($this->requests, array_flip($this->address));
        $addresses = array_diff_key($this->requests, array_flip($this->parameters));

        DB::beginTransaction();


        if($request->hasFile('file')){

            $page->clearMediaCollection('logo');
            $page->addMedia($request->file)
                ->toMediaCollection('logo');
        }

        if($request->hasFile('cover')){

            $page->clearMediaCollection('cover');
            $page->addMedia($request->cover)
                ->toMediaCollection('cover');
        }

        if($request->slug != $page->slug){
            $parameters['slug'] = str_slug($request->slug ?? $request->name );
        }


        foreach($parameters as $key => $put){
            $page->$key = $put;
        }


        $addressModel = $page->getAddress;

        foreach($addresses as $key => $put){
            $addressModel->$key = $put;
        }

        $page->save();
        $addressModel->save();

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function updateTrans(Request $request, $id)
    {
        $page = StoreTranslation::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, StoreTranslation::$rules, StoreTranslation::$messages);

        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    //multilang desteklemir, chunki translation tb-da deleted_at yoxdu
    public function trash($id)
    {
        $store = StoreTranslation::findOrFail($id);

        DB::beginTransaction();

        try{
            $store->getStore()->delete();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route);
    }


    public function restore($id)
    {
        $store = StoreTranslation::findOrFail($id);

        DB::beginTransaction();


        try{
            $store->getStore()->onlyTrashed()->restore();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route);
    }


    public function destroy($id)
    {
        $store = StoreTranslation::findOrFail($id);

        $store->getStore()->onlyTrashed()->forceDelete();

        return $this->responseJson(0, trans('locale.update_success'),$this->route, "modal-confirm");
    }
}