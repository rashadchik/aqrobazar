<?php

namespace App\Http\Controllers;

use App\Crud\LegislationCrud;
use App\DataTables\LegislationDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Legislation;
use Illuminate\Http\Request;
use DB;

class LegislationController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model;
    public $route = 'legislation';
    public $title = 'Qanunvericilik';
    public $name = 'Sənəd';
    public $filters = [];
    public $largeModal = false;
    public $createRoute = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new LegislationDataTable();
        $this->model = new Legislation();
        $this->crud = new LegislationCrud();
        $this->requests = $request->except('_token', '_method', 'file');
    }
}