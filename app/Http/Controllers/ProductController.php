<?php

namespace App\Http\Controllers;

use App\Crud\ProductCrud;
use App\DataTables\ProductDataTable;
use App\Events\AddModified;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Product;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'product';
    public $title = 'Elanlar';
    public $name = 'Elan';
    public $filters = ['deleted'];
    public $largeModal = true;
    public $createRoute = false;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new ProductDataTable();
        $this->model = new Product();
        $this->crud = new ProductCrud();
        $this->requests = $request->except('_token', '_method', 'property_id');
    }


    public function update(Request $request, $id)
    {
        $data = Product::findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(), Product::$rules, Product::$messages);
        if($validation->fails()){
            return $this->responseJson(1, $validation->errors()->first());
        }

        DB::beginTransaction();


        try{

            if($this->requests['category_id'] == '') {
                $this->requests['category_id'] = $request->main_category_id;
            }

            foreach($this->requests as $key => $put){
                $data->$key = $put;
            }
        }
        catch(\Exception $e){
            return $this->responseJson(1, $e->getMessage());
        }


        if($data->status != $data->getOriginal('status')) {
            try{
                event(new AddModified($data));
            }
            catch(\Exception $e){
                return redirect()->back()->withErrors(['error' => $e->getMessage()])->withInput($request->all());
            }
        }

        $data->save();

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function fastUpdate($id, Request $request)
    {
        $value = $request->value;
        $type = $request->type;

        $rules = [
            'status' =>     ['value' => "required"],
            'store_id' =>   ['value' => "nullable"],
        ];

        $validation = Validator::make($request->all(), $rules[$type]);

        if($validation->fails()){
            return $this->responseJson(400, $validation->errors()->first());
        }

        $product = Product::findOrFail($id);

        $product->{$type} = $value;

        if($type == 'status') {
            try{
                event(new AddModified($product));
            }
            catch(\Exception $e){
                return $this->responseJson(400, $e->getMessage());
            }
        }

        $product->save();

        return $this->responseJson(200, trans('locale.update_success'));
    }
}