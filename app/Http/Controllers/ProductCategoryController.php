<?php

namespace App\Http\Controllers;

use App\Crud\ProductCategoryCrud;
use App\Crud\ProductCategoryTransCrud;
use App\DataTables\ProductCategoryDataTable;
use App\Logic\DataTableIndex;
use App\Logic\Order;
use App\Logic\PageAction;
use App\Models\Page;
use Illuminate\Http\Request;
use DB;

class ProductCategoryController extends Controller
{
    use DataTableIndex, PageAction;

    public $crud, $paramCrud, $requests, $dataTable, $model, $order, $mediaLibraryCollection, $diskName;
    public $route = 'category';
    public $title = 'Məhsulların kateqoriyaları';
    public $name = 'Kateqoriya';
    public $filters = ['deleted', 'lang'];
    public $largeModal = true;
    public $templateId = 5;

    //public $createRoute = true;
    //public $createRouteViaModal = true;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'order']);

        $this->dataTable = new ProductCategoryDataTable();
        $this->model = new Page();
        $this->crud = new ProductCategoryTransCrud();
        $this->paramCrud = new ProductCategoryCrud();
        $this->order = new Order();
        $this->meta = true;
        $this->mediaLibraryCollection = 'icon';
        $this->diskName = 'public';
        $this->translates = ['name', 'lang', 'slug', 'content', 'forward_url', 'meta_description', 'meta_keywords'];
        $this->requests = $request->except('_token', '_method', 'file');
    }
}