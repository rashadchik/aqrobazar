<?php

namespace App\Http\Controllers;

use App\Models\PriceList;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\VipProduct;
use DB;
use Carbon\Carbon;

trait PaidProductController
{
    public function create($id)
    {
        $product = Product::where('id', $id)->where($this->belongColumn, 0)->firstOrFail();
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => $product->name, 'route' => ["$this->route.store", $id], 'editor' => false, 'meta' => false ]);
    }


    public function edit($id)
    {
        $product = Product::where('id', $id)->where($this->belongColumn, 1)->firstOrFail();

        $paidProduct = $product->getPaid($this->type)->firstOrFail();

        $fields = $this->crud->fields('edit', ['data' => $paidProduct]);

        return view('admin.dt.edit')->with(['fields' => $fields, 'title' => $product->name, 'route' => ["$this->route.update", $paidProduct->id], 'editor' => false, 'meta' => false ]);
    }


    public function store(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $startDate = filterDate(Carbon::now(), true, 'full');
        $endDate = new Carbon($startDate);
        $endDate->addDays($request->count_day);

        DB::beginTransaction();

        try{
            VipProduct::create([
                'product_id' => $id,
                'type' => $this->type,
                'start_date' => $startDate,
                'end_date' => $endDate
            ]);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        try{
            $product->{$this->belongColumn} = 1;
            $product->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->responseJson(1, $e->getMessage());
        }


        DB::commit();


        if($this->route == 'vip'){
            $msg = "Elan <b>$product->name</b> VIP oldu.";

        }
        else{
            $msg = "Elan <b>$product->name</b> irəli çəkildi.";
        }

        return $this->responseJson(0, $msg, "product", "myModal");
    }


    public function update(Request $request, $id)
    {
        $paidProduct = VipProduct::findOrFail($id);

        $paidProduct->end_date = $request->end_date;
        $paidProduct->save();

        return $this->responseJson(0, trans('locale.update_success'), "product", "myModal");
    }


    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $vip = $product->getPaid($this->type)->first();

        DB::beginTransaction();


        if($vip) {

            try{
                $vip->delete();
            }
            catch(\Exception $e){

                DB::rollback();

                return $this->responseJson(0, $e->getMessage());
            }

            try{
                $product->{$this->belongColumn} = 0;
                $product->save();
            }
            catch(\Exception $e){

                DB::rollback();

                return $this->responseJson(0, $e->getMessage());
            }
        }


        DB::commit();

        if($this->route == 'vip') {
            $msg = "<b>$product->name</b> adlı elan artıq VIP deyil.";
        }
        else{
            $msg = "Usta <b>$product->name</b> adlı elan artıq irəlidə deyil.";
        }

        return $this->responseJson(0, $msg, "product", "modal-confirm");
    }
}
