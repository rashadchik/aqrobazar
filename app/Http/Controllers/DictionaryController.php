<?php

namespace App\Http\Controllers;

use App\Crud\DictionaryCrud;
use App\DataTables\DictionaryDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\Models\Dictionary;
use Illuminate\Http\Request;
use DB;

class DictionaryController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $createRoute;
    public $route = 'dictionary';
    public $title = 'Lüğət';
    public $name = 'Söz';
    public $filters = [];
    public $largeModal = false;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->dataTable = new DictionaryDataTable();
        $this->model = new Dictionary();
        $this->crud = new DictionaryCrud();
        $this->createRoute = $request->has('admin') ? true : true;

        $this->requests = $request->except('_token', '_method');

        if (in_array(strtolower($request->method()), ['put', 'patch', 'post'])) {
            clearCache('dictionary');
        }
    }


    public function store(Request $request)
    {
        $inputs = $request->input('content');
        $keyword = $request->input('keyword');
        $editor = $request->input('editor', 0);

        $request->validate(Dictionary::$rules, Dictionary::$messages);

        DB::beginTransaction();

        foreach($inputs as $lang => $input){

            try{
                Dictionary::create(['keyword' => $keyword, 'content' => $input, 'lang_id' => $lang, 'editor' => $editor]);
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->responseJson(1, $e->getMessage());
            }

        }

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }


    public function update(Request $request, $id)
    {
        $word = Dictionary::findOrFail($id);

        $inputs = $request->input('content');

        $request->validate(['content' => 'nullable']);

        $find = Dictionary::where('keyword', $word->keyword)->pluck('lang_id')->toArray();


        DB::beginTransaction();


        foreach($inputs as $lang => $input){

            if(in_array($lang, $find))
            {
                try{
                    Dictionary::where('keyword', $word->keyword)->where('lang_id', $lang)->update(['content' => $input]);
                }
                catch(\Exception $e){
                    DB::rollback();

                    return $this->responseJson(1, $e->getMessage());
                }
            }
            else{
                try{
                    Dictionary::create(['keyword' => $word->keyword, 'content' => $input, 'lang_id' => $lang]);
                }
                catch(\Exception $e){
                    DB::rollback();

                    return $this->responseJson(1, $e->getMessage());
                }
            }

        }

        DB::commit();

        return $this->responseJson(0, trans('locale.update_success'), $this->route, "myModal");
    }
}