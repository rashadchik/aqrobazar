<?php

namespace App\Http\Controllers;

use App\Crud\AdminCrud;
use App\DataTables\UsersDataTable;
use App\Logic\DataTableAction;
use App\Logic\DataTableIndex;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use DataTableIndex, DataTableAction;

    public $crud, $requests, $dataTable, $model, $order;
    public $route = 'user';
    public $title = 'İstifadəçilər';
    public $name = 'User';
    public $filters = ['deleted'];
    public $largeModal = false;
    public $createRoute = false;
    public $rules = 'func';

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'profile', 'updatePhoto']);

        $this->dataTable = new UsersDataTable();
        $this->model = new User();
        $this->crud = new AdminCrud();
        $this->requests = $request->except('_token', '_method');
    }


    public function fastUpdate($id, Request $request)
    {
        $value = $request->value;
        $type = $request->type;

        $rules = [
            'store_id' =>         ['value' => "nullable"],
        ];

        $validation = Validator::make($request->all(), $rules[$type]);

        if($validation->fails()){
            return $this->responseJson(400, $validation->errors()->first());
        }

        $data = User::findOrFail($id);

        $data->{$type} = $value;
        $data->save();


        return $this->responseJson(200, trans('locale.update_success'));
    }
}