<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Config;
use App\Mail\ContactForm;
use Mail;
use App\Mail\ApplicationForm;

class ContactController extends Controller
{

    private $lang;

    public function __construct()
    {
        $this->lang = app()->getLocale();
    }



    public function resume(Request $request)
    {

        $config = Config::findOrFail(1);
        $education = $request->input('education');

        for ($i = 1;$i <=$request->input('lang_numb');$i++){
            if($request->input('lang'.$i)) {
                $request['lang'] = array_merge($request->input('lang'), $request->input('lang' . $i));
            }
        }

        for ($i = 1;$i <=$request->input('skill_numb');$i++){
            if($request->input('skils'.$i)){
                $request['skils'] = array_merge($request->input('skils'),$request->input('skils'.$i));
            }
        }

        $foreign_lang = $request->input('foreign_lang');
        $comp_skills = $request->input('comp_skills');
        $courses_title= $request->input('courses_title');

        $content = $request->except('_token', '_method');
        $request['education'] = array_filter($education, function($education){return !is_null($education); } );
        $request['foreign_lang'] = array_filter($foreign_lang, function($foreign_lang){return !is_null($foreign_lang); } );
        $request['comp_skills'] = array_filter($comp_skills, function($comp_skills){return !is_null($comp_skills); } );
        $request['courses_title'] = array_filter($courses_title, function($courses_title){return !is_null($courses_title); } );


        $rules = [
            'name' => "required",
            'surname' => 'required',
            'date' => 'required',
            'family_status' => 'required',
            'job' => 'required',
            'phone1' => 'nullable|numeric',
            'phone2' => 'required|numeric',
            'email' => 'required|email',
            'image' => 'nullable|mimes:png,jpeg,zim|max:10000',
        ];

        $request->validate($rules);

        $email = Config::where('key', 'email')->firstOrFail();

        $pdf = \PDF::loadView('web.pdf.form', ["content" => $request->all(), "title" => 'Onlayn Müraciət']);

        return $this->sendMail($pdf, $request->all(),[$request->file('image')],$email->value);

    }

    private function sendMail($pdf, $form,$files, $email)
    {
        Mail::to($email)->send(new ApplicationForm($pdf, $form, array_filter($files),'Onlayn Müraciət'));

        request()->session()->flash('success', true);

        return redirect()->back();

    }

}
