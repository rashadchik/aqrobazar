<?php

namespace App\Http\Controllers;

use App\Logic\Millikart;
use App\Logic\SimToday;
use App\Models\Payment;
use App\Models\PriceList;
use App\Models\Product;
use App\Models\VipProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Auth;

class PaymentController extends Controller
{
    public $provider;
    public $sim;

    public function __construct(Millikart $provider, SimToday $sim)
    {
        $this->middleware('web');
        $this->provider = $provider;
        $this->sim = $sim;
    }


    public function paymentForm($id)
    {
        $product = Product::find($id);

        if(!$product){
            return 'Elan tapılmadı';
        }

        if(str_contains(\request()->url(), 'favourite')){
            $type = 1;
        }
        elseif(str_contains(\request()->url(), 'vip')){
            $type = 2;
        }
        else{
            $type = 3;
        }

        $title = trans('locale.price-list.'.$type);
        $price = $this->priceList($type);

        return view('web.elements.ads-payment-form', ['price' => $price, 'title' => $title, 'product' => $product, 'type' => $type]);
    }


    private function priceList($type)
    {
        return PriceList::where('type', $type)->select('amount', 'note')->orderBy('amount', 'asc')->get();
    }


    public function pay(Request $request)
    {
        $type = (int) $request->type;
        $card = (int) $request->card;
        $amount = (int) $request->amount;
        $product = (int) $request->product;
        $reference = str_random(40);
        $user = Auth::guard('web')->user()->id ?? null;

        $price = PriceList::where('type', $type)->where('amount', $amount)->first();

        if($card > 2 || $card < 1){
            throw new \Exception('invalid card');
        }

        if(!$price){
            throw new \Exception('invalid amount');
        }

        $product = Product::find($product);

        if(!$product){
            throw new \Exception('invalid ads');
        }

        //create payment
        $payment = Payment::create([
            'product_id' => $product->id,
            'user_id' => $user,
            'amount' => $amount * 100,
            'card_type' => $card,
            'type' => $price->type,
            'day' => $price->day,
            'description' => $price->note,
            'payment_key' => $reference
        ]);


        if($payment->card_type == 2){
            $response = $this->sim->getTariff($amount, $payment->id);
        }
        else{
            $response = $this->provider->getURL($amount, $reference, str_slug($price->note));
        }

        return redirect()->away($response);
    }


    public function completeCard()
    {
        $reference = request()->get('reference');

        $data = $this->provider->request($reference);

        $xml = file_get_contents($data);
        $xml = simplexml_load_string($xml);

        $cardNumber = (int) substr($xml->pan, -4);

        return $this->complete($xml->reference, $xml->code, $cardNumber, 1);
    }


    public function completeSms(Request $request)
    {
        $data = \GuzzleHttp\json_decode($request->data);

        return $this->complete($data->transaction, $data->chargingResponse->code, $data->msisdn, 2, $data->order_id);
    }


    public function show($reference)
    {
        $payment = Payment::where('payment_key', $reference)->first();

        if (!$payment) {
            return view('errors.404');
        }

        if ($payment->code != 0) {
            return 'Ödəniş uğursuz alındı';
        }

        return 'Ödəniş uğurlu alındı';
    }


    private function complete($paymentKey, $code, $cardNumber, $paymentType, $orderId = null)
    {
        $payment = Payment::whereNull('card_number');

        if ($paymentType == 1) {
            $payment = $payment->where('payment_key', $paymentKey)->first();
        }
        else {
            $payment = $payment->where('id', $orderId)->first();
        }

        $product = Product::find($payment->product_id);

        if (!$payment) {
            throw new \Exception('invalid Reference');
        }
        elseif (!$product) {
            throw new \Exception('invalid Product');
        }

        if ($code != 0) {
            return redirect()->route('payment.show', $paymentKey);
        }

        $startDate = filterDate(Carbon::now(), true, 'full');
        $endDate = new Carbon($startDate);
        $endDate->addDays($payment->day);

        DB::beginTransaction();


        $payment->payment_key = $paymentKey;
        $payment->code = $code;
        $payment->card_number = $cardNumber;
        $payment->save();


        VipProduct::create([
            'payment_id' => $payment->id,
            'product_id' => $payment->product_id,    //her ehtimala qarshi, product_id hem de bu table-da saxlanilacaq
            'type' => $payment->type,
            'start_date' => $startDate,
            'end_date' => $endDate
        ]);


        if($payment->type == 1){
            $product->is_fav = 1;
        }
        elseif($payment->type == 2){
            $product->is_vip = 1;
        }
        elseif($payment->type == 3){
            $product->is_premium = 1;
        }
        else{
            throw new \Exception('invalid Payment');
        }

        $product->save();


        DB::commit();

        return redirect()->route('payment.show', $paymentKey);
    }
}
