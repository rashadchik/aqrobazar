<?php

namespace App\Http\Controllers;

use App\Logic\Dropzone;
use App\Models\Product;
use Illuminate\Http\Request;
use App\DataTables\GalleryDataTable;
use Illuminate\Support\Facades\Validator;
use Spatie\MediaLibrary\Models\Media;

class GalleryController extends Controller
{
    private $title, $dropzone;

    public function __construct(Request $request, Dropzone $dropzone)
    {
        $this->middleware('auth:admin');

        $this->requests = $request->except('_token', '_method');
        $this->title = 'Qalereya';
        $this->dropzone = $dropzone;
    }


    public function index(GalleryDataTable $dataTable, $route, $id)
    {
        $model = config('config.model.'.$route);

        $data = $model::findOrFail($id);

        $collection = 'gallery';
        $conversion = 'xs';

        if ($route == 'product') {
            $collection = 'default';
            $conversion = 'sm';
        }

        return $dataTable->data($data)->with(['collection' => $collection, 'conversion' => $conversion ])->render('admin.galleries', ['title' => $this->title, 'data' => $data, 'route' => $route]);
    }


    public function store(Request $request, $route, $id)
    {
        $model = config('config.model.'.$route);

        $data = $model::findOrFail($id);

        $collection = 'gallery';

        if ($route == 'product') {
            $collection = 'default';
        }

        $upload = $this->dropzone->upload($data, $request->file, $collection);

        return $upload;
    }


    public function destroy($id)
    {
        $media = Media::findOrFail($id);
        $model = $media->model_type::find($media->model_id);
        $model->deleteMedia($media->id);

        return $this->responseJson(0,"Şəkil silindi", "galleries", "modal-confirm");
    }
}
