<?php

namespace App\Http\Controllers;

use App\Events\AddCreated;
use App\Events\AddModifiedByUser;
use App\Logic\Share;
use App\Models\Banner;
use App\Models\CountryTr;
use App\Models\Page;
use App\Models\Product;
use App\Notifications\NewProduct;
use App\Rules\Cellphone;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Notification;

class UserProductController extends Controller
{

    use Share;

    public $requests;

    public function __construct(Request $request)
    {
        $this->middleware('auth:web')->only(['delete']);
        $this->loadData();
        $this->requests = $request->except(['_token', '_method', 'pin_code', 'property_id', 'file', 'adType', 'conditions', 'country', 'full_phone']);
    }


    public function show($id)
    {
        $product = Product::join('pages as p', 'p.id', '=', 'products.category_id')
            ->join('page_translations as pt', 'pt.page_id', '=', 'p.id')
            ->select('p.parent_id', 'pt.slug as page_slug', 'products.slug')
            ->where('pt.lang', $this->lang)
            ->findOrFail($id);

        $url = \App\Logic\Menu::fullNestedSlug($product->parent_id, $product->page_slug, $product->slug);

        return redirect()->to($url);

    }


    public function stat()
    {
        $user = auth()->guard('web')->user();

        $groupCount = Product::where('user_id', $user->id)
            ->select(DB::raw('count(*) as count'), 'status')
            ->groupBy('status')
            ->pluck('count', 'status');

        $banners = Banner::fetch($this->device);

        return view('web.user.stat', ['user' => $user, 'count' => $groupCount, 'banners' => $banners]);
    }


    public function products($type)
    {
        $user = auth()->guard('web')->user();

        if($type == 'published'){
            $status = 1;
            $products = Product::get($this->lang);
        }
        elseif($type == 'pending'){
            $status = 0;
            $products = Product::get($this->lang, $status);
        }
        elseif($type == 'expired'){
            $status = 2;
            $products = Product::get($this->lang, $status);
        }
        elseif($type == 'rejected'){
            $status = 3;
            $products = Product::get($this->lang, $status);
        }
        else{
            return view('errors.404');
        }

        if($user->id != 1) {
            $products->where('user_id', $user->id);
        }

        $getProducts = $products->paginate(24);

        if (request()->ajax()) {
            $view = view('web.elements.products-ajax', ['products' => $getProducts, 'activeEdit' => true] )->render();
            return response()->json(['html'=>$view]);
        }

        $banners = Banner::fetch($this->device);

        $title = trans('locale.product-status.'.$status.'.title');

        return view('web.bookmark-products', ['user' => $user, 'products' => $getProducts, 'banners' => $banners, 'title' => $title, 'activeEdit' => true]);
    }



    public function create()
    {
        //return view('errors.under-construction');
        $categories = Page::productCategories($this->lang)
            ->whereNull('pages.parent_id')
            ->pluck('pt.name', 'pages.id')
            ->prepend('Siyahıdan seçin', '');

        $token = str_random();

        $product = Product::where('token', old('token'))->where('status', 100)->first();

        if(!is_null(old('token')) && $product) {

            $model = $product;
        }
        else{
            $model = new Product();
            $model->status = 100;
            $model->token = $token;
            $model->save();
        }


        $banners = Banner::fetch($this->device);

        $data = [
            'title' => 'Elan yerləşdir',
            'categories' => $categories,
            'countries' => CountryTr::getLists(),
            'model' => $model,
            'token' => $token,
            'banners' => $banners,
            'hideCreateRoute' => true
        ];

        return view('web.create', $data);
    }


    public function store(Request $request)
    {
        $token = $request->token;

        $this->validation($request, 'store');

        $product = Product::where('token', $token)->where('status', 100)->first();

        if(!$product){
            return redirect() ->back()->withErrors(['error' => 'Error token'])->withInput(['inputs' => $request->all()]);
        }

        $checkUser = User::where('email', $request->user_email)->first();

        if(auth()->guard('web')->check() || !is_null($checkUser) ){
            $product->user_id = auth()->guard('web')->user()->id ?? $checkUser->id;
            $product->store_id = $checkUser->store_id;
        }


        DB::beginTransaction();

        try{
            $product->pin_code = rand(1000009, 9999998);
            $product->slug = $product->id.'-'.str_slug($request->name);
            $product->status = 0;
            $product->main_category_id = $request->category_id;
            $product->token = null;

            foreach ($this->requests as $key => $put){
                $product->$key = $put;
            }

            if (!$product->getFirstMedia()) {
                return redirect()->back()->withErrors(['error' => 'Ən azı 1 şəkil yüklənməlidir.'])->withInput($request->all());
            }
            $product->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withErrors(['error' => 'Elanın yerləşdirilməsi baş tutmadı, xahiş olunur, yenidən cəhd edin.'])->withInput($request->all());
        }

        try{
            event(new AddCreated($product));
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withErrors(['error' => 'Error Mail: Elanın yerləşdirilməsi baş tutmadı, xahiş olunur, yenidən cəhd edin.'])->withInput($request->all());
        }


        DB::commit();

        return redirect()->route('ads.show', $product->id);
    }


    public function edit(Request $request, $slug)
    {
        $token = str_random();
        $pinCode = (int) $request->get('pin_code');

        $product = Product::where('slug', $slug)->whereIn('status', Product::editableStatus)->firstOrFail();

        if(auth()->guard('web')->check() && auth()->guard('web')->user()->id != $product->user_id){
            return view('errors.404');
        }
        elseif($pinCode != $product->pin_code){
            return view('errors.404');
        }

        $page = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
            ->where('pages.id', $product->category_id)
            ->select('pt.name', 'pt.slug', 'pages.id', 'pages.parent_id')
            ->firstOrFail();

        $mainCategory = $this->menu->getData($page, 0);

        $categories = Page::productCategories($this->lang)
            ->whereNull('pages.parent_id')
            ->pluck('pt.name', 'pages.id');

        $product->category_id == $mainCategory->id ? $selected = '' : $selected = $product->category_id;
        $banners = Banner::fetch($this->device);

        $data = [
            'title' => $product->name,
            'categories' => $categories,
            'product' => $product,
            'model' => $product,
            'pinCode' => $pinCode,
            'page' => $page,
            'mainCategory' => $mainCategory,
            'countries' => CountryTr::getLists(),
            'method' => 'PUT',
            'selected' => $selected,
            'token' => $token,
            'banners' => $banners
        ];

        return view('web.create', $data);
    }


    public function delete($id)
    {
        $product = Product::findOrFail($id);

        $this->deleteProduct($product);

        return redirect()->back();
    }


    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $pinCode = (int) $product->pin_code;

        if($pinCode != $product->pin_code){
            return view('errors.404');
        }

        $request['full_phone'] = $request->user_phone;

        $this->validation($request, 'update');

        if(!$product->getFirstMedia()){
            return redirect()->back()->withErrors(['error' => 'Ən azı 1 şəkil yüklənməlidir.'])->withInput($request->all());
        }

        $status = $product->status;

        DB::beginTransaction();

        $product->main_category_id = $request->category_id;

        try {
            foreach($this->requests as $key => $put) {
                $product->$key = $put;
            }

            $product->status = 0;
            $product->save();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(['error' => 'Elanın yenilənməsi baş tutmadı, xahiş olunur, yenidən cəhd edin.'])->withInput($request->all());
        }

        if($status == 1) {
            try {
                event(new AddModifiedByUser($product));
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withErrors(['error' => 'Elanın yenilənməsi baş tutmadı, xahiş olunur, yenidən cəhd edin.'])->withInput($request->all());
            }
        }


        DB::commit();


        return redirect()->route('ads.show', $product->id);
    }


    public function confirm(Request $request, $id)
    {
        $product = Product::where('pin_code', $request->pin_code)->find($id);

        if($product){

            if($request->type == 1){
                return redirect()->to('ads/'.$product->slug.'/edit?pin_code='.$product->pin_code);
            }
            elseif($request->type == 2){
                $this->deleteProduct($product);
                return redirect()->route('home')->with('success', 'Elan silindi');
            }
            else{
                return redirect()->to('home');
            }

        }
        else{
            return redirect()->back()->withErrors(['error' => 'Pin kod düzgün deyil']);
        }
    }


    private function filterPhone($val)
    {
        return (int) str_replace([' ', '(', ')', '+994'], ['', '', '', ''], $val);
    }


    private function deleteProduct($product)
    {
        $product->status = 3;
        $product->save();
    }


    private function validation($request, $method)
    {
        $this->requests['category_id'] = $request->property_id ?? $request->category_id;

        if($method == 'store') {
            $request['user_phone'] = $request->full_phone;
            $this->requests['user_phone']= $request->full_phone;
        }

        //$prefix = (int) substr($request['user_phone'], 0, 2);

        $inputs = [
            'category_id' => 'required|exists:pages,id',
            'name' => 'required|max:40',
            'type' => 'required|numeric|between:1,2,3,4',
            'price' => 'required|numeric|min:0.01,max:1000000',
            'summary' => 'string|max:1000',
            'user_name' => 'required|max:40',
            'city_id' => 'required|numeric|exists:cities,id',
            'user_email' => 'required|email',
            'user_phone' => ['required', 'numeric'],
        ];

        /*if($request->hasFile('photos') && $method == 'store')
        {
            $inputs['photos'] = 'required';
            $count = count($request->file('photos'));

            foreach(range(0, $count) as $c) {
                $inputs['photos.' . $c] = 'mimes:jpeg,jpg,png|max:8192|images_count:'.$count.',4';
            }
        }*/



        $messages = [
            'photos.required' => 'Şəkil seçilməyib.',
            'photos.*.max' => 'Hər şəklin həcmi 8 mb-dan artıq olmamalıdır',
            'images_count' => 'Ən azı 4 şəkil yüklənməlidir',
        ];

        return $request->validate($inputs, $messages);
    }
}
