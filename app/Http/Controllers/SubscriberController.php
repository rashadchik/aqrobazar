<?php

namespace App\Http\Controllers;

use App\Crud\SubscriberCrud;
use App\Exceptions\DataTableException;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\DataTables\SubscriberDataTable;
use Illuminate\Support\Facades\Validator;
use App\Models\Dictionary;

class SubscriberController extends Controller
{
    public $title, $crud;

    public function __construct(Request $request, SubscriberCrud $crud)
    {
        $this->middleware('auth:admin')->except(['store']);
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "İzləyicilər";
    }


    public function index(SubscriberDataTable $dataTable)
    {
        return $dataTable->render('admin.subscribers', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.dt.create', ["fields" => $fields, 'route' => 'subscribers.store', 'title' => 'Yeni izləyici']);
    }


    public function store(Request $request)
    {
        $inputs = [
            'full_name' => $request->input('full_name'),
            'email' => $request->input('email'),
            'status' => 0
        ];

        $validator = Validator::make($inputs, Subscriber::$rules);

        if ($validator->fails()) {
            throw new DataTableException($validator->errors()->first());
        }

        Subscriber::create($inputs);

        $dictionary = Dictionary::where('keyword', 'subscribe_success')->where('lang_id', app()->getLocale())->first();

        return $this->responseSuccess($dictionary->content);
    }


    public function update($id)
    {
        $user = Subscriber::findOrFail($id);
        $user->status = 1;
        $user->save();

        return $this->responseSuccess();
    }


    public function destroy($id)
    {
        $user = Subscriber::findOrFail($id);
        $user->delete();

        return $this->responseSuccess("İzləyici siyahıdan silindi", "#subscribers", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#subscribers", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}
