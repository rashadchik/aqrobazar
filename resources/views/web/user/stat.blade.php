@extends ('layouts.web', ['page_heading' => $user->name])

@section ('content')

    <div class="col-xs-12">
        @component('web.elements.user-profile', ['user' => $user] )
            @slot('content')
                <div class="account_content">
                    <div class="account_content_inner">
                        @foreach(trans('locale.product-status') as $key => $tab)
                            <div class="item clearfix">
                                <span class="lbl"><a href="{{ route('product.list', $tab['route']) }}">{{ $tab['title'] }}</a></span>
                                <span class="val"><a href="{{ route('product.list', $tab['route']) }}">{{ $count[$key] ?? 0 }}</a></span>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection