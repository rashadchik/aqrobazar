@extends ('layouts.web', ['page_heading' => $user->name])

@section ('content')

    <div class="col-xs-12">
        @component('web.elements.user-profile', ['user' => $user] )
            @slot('content')
                <div class="account_content">
                    <div class="account_content_inner">
                        <div class="item clearfix">
                            <span class="lbl">Ad, soyad:</span>
                            <span class="val">{{ $user->name }}</span>
                        </div>
                        <div class="item clearfix">
                            <span class="lbl">E-poçt:</span>
                            <span class="val">{{ $user->email }}</span>
                        </div>
                        <div class="item clearfix">
                            <span class="lbl">Əlaqə nömrəsi:</span>
                            <span class="val">{!! $user->phone ?? '<span class="text text-danger">Qeyd olunmayıb</span>' !!}</span>
                        </div>
                        <div class="item clearfix">
                            <span class="lbl">Şəhər:</span>
                            @if($user->city)
                                <span class="val">{{ $user->city->name_az }}, {{ $user->city->country->tr->name }}</span>
                            @else
                                <span class="val"><span class="text text-danger">Qeyd olunmayıb</span></span>
                            @endif
                        </div>
                        <div class="item clearfix">
                            <span class="lbl">Şifrə:</span>
                            <a href="#" title="" class="new_password open_lr" data-type="change_password"><i class="fa fa-pencil" aria-hidden="true"></i>Şifrəni dəyiş</a>
                        </div>
                    </div>
                    <button type="button" class="btn_edit open_lr" data-type="edit_user"><i class="fa fa-pencil" aria-hidden="true"></i>Düzəliş et</button>
                </div>


                <div class="popup">
                    <div class="popup_inner" data-id="change_password">
                        <span class="btn_close"></span>
                        <h1>Şifrəni dəyiş</h1>
                        <div class="lr_wrapper">
                            <form method="POST" action="{{ route('user.password.update') }}">
                                @csrf
                                <div class="ipt_group">
                                    <input type="password" name="current_password" placeholder="Cari Şifrə" class="ipt_style password" required>
                                </div>
                                <div class="ipt_group">
                                    <input type="password" name="password" placeholder="Yeni şifrə" class="ipt_style password" required>
                                </div>
                                <div class="ipt_group">
                                    <input type="password" name="password_confirmation" placeholder="Yeni şifrənin təsdiqi" class="ipt_style password" required>
                                </div>
                                <div class="clearfix">
                                    <button type="submit" class="center" name="">Təsdiq et</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="popup_inner" data-id="edit_user">
                        <span class="btn_close"></span>
                        <h1>Düzəliş et</h1>
                        <div class="lr_wrapper">
                            <form method="POST" action="{{ route('user.update', auth()->guard('web')->user()->id) }}">
                                @csrf
                                <input type="hidden" name="_method" value="PUT">
                                <div class="ipt_group">
                                    <input type="text" name="name" placeholder="Ad, soyad" class="ipt_style user" value="{{ $user->name }}" required>
                                </div>
                                <div class="ipt_group">
                                    <input type="email" name="email" placeholder="E-poçt" class="ipt_style email" value="{{ $user->email }}" required>
                                </div>
                                <div class="ipt_group">
                                    <input type="text" id="user-phone" name="phone" placeholder="Əlaqə nömrəsi" class="ipt_style phone" value="{{ $user->phone }}">
                                    <span id="user-phone-output" style="color:red"></span>
                                </div>
                                <div class="ipt_group">
                                    {!! Form::select('city', \App\Models\City::getCities(), null, ['class' => 'ipt_style', 'placeholder' => 'Şəhəri seçin']) !!}
                                </div>
                                <div class="clearfix">
                                    <button type="submit" class="center" name="">Təsdiq et</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection

@push('scripts')
    <script>

        var userInput = document.querySelector("#user-phone"),
            userOutput = document.querySelector("#user-phone-output");

        var userIti = window.intlTelInput(userInput, {
            hiddenInput: "full_phone",
            onlyCountries: {!! $countryCodes !!},
            utilsScript: "../js/utils.js"
        });

        var userHandleChange = function() {
            var text = (userIti.isValidNumber()) ? userIti.getNumber() : "Yanlış nömrə";
            var textNode = document.createTextNode(text);
            userOutput.innerHTML = "";
            userOutput.appendChild(textNode);

            if(userIti.isValidNumber()) {
                userOutput.innerHTML = "";
            }
        };

        // listen to "keyup", but also "change" to update when the user selects a country
        userInput.addEventListener('change', userHandleChange);
        userInput.addEventListener('keyup', userHandleChange);

    </script>
@endpush
