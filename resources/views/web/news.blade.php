@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs')
        <!-- NewsList Begin -->
        <section class="news_list">
            <div class="container">
                <div class="row">
                    <!-- NewsList Begin -->
                    @foreach($articles as $article)
                        <div class="col-sm-4 col-xs-6 col-mob-12">
                            <article>
                                <figure>
                                    <img src="{{ asset($article->getFirstMedia()->getUrl('md')) }}" alt="{{ $article->title }}">
                                </figure>
                                <div class="inner">
                                    <h2><a href="{{ route('showPage', [$page->slug, $article->slug]) }}">{{ $article->title }}</a></h2>
                                    <p>{{ str_limit($article->summary, 150)}}</p>
                                </div>
                                <div class="bottom clearfix">
                                    <span class="date"><i class="fa fa-calendar" aria-hidden="true"></i>{{ filterDate($article->published_at, false, 'eDay') }}</span>
                                    <a href="{{ route('showPage', [$page->slug, $article->slug]) }}" title="{{ $article->title }}">{{ $dictionary['read_more'] }}</a>
                                </div>
                            </article>
                        </div>
                @endforeach
                <!-- NewsList End -->
                </div>
                <div class="col-xs-12">
                    <!-- Pagination Begin -->
                    <nav class="pag clearfix">
                        {{ $articles->links() }}
                    </nav>
                    <!-- Pagination End -->
                </div>
            </div>
        </section>
        <!-- NewsList End -->
    </div>

@endsection
