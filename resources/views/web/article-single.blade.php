@extends ('layouts.web', ['page_heading' => $article->title, 'page_image' => $article->getFirstMedia()->getUrl('lg')])

@section ('content')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs')
        <!-- Page Begin -->
        <section class="news_detail">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text">
                        {!! $article->content !!}
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    <div id="newsgallery" class="owl-carousel">
                        @foreach($article->getMedia('gallery') as $img)
                            <figure>
                                <a href="{{ asset($img->getUrl('lg')) }}" title="{{ $article->title }}" class="zoom-photo">
                                    <img src="{{ asset($img->getUrl('sm')) }}" alt="{{ $article->title }}">
                                </a>
                            </figure>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <!-- NewsDetail End -->
    </div>

@endsection
