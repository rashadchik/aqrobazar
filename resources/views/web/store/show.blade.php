@extends ('layouts.web', ['page_heading' => $store->name])

@section ('content')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs', [
            'breadcrumbs' => [
                $category => route('store.all'),
                $store->name => route('store.show', $store->slug)
            ]
        ])

        <section class="store_detail">
            @if($store->getFirstMedia('cover'))
                <div class="cover">
                    <img src="{{ asset($store->getFirstMedia('cover')->getUrl('lg')) }}" alt="{{ $store->name }}" class="img_cover">
                </div>
            @endif
            <div class="store_info clearfix">
                <figure class="store_logo">
                    <img src="{{ asset($store->getFirstMedia('logo')->getUrl('sm')) }}" alt="{{ $store->name }}" style="border-radius: 50%">
                </figure>
                <div class="col">
                    <h1>{{ $store->name }}</h1>
                    <p class="desc">{{ $store->getTrans->summary }}</p>
                    <span class="view">{{ $store->view_count }}</span>
                </div>
                <div class="col">
                    <ul class="list-unstyled">
                        <li><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i> {{ $store->getAddress->address }}</li>
                        <li><i class="fa fa-phone fa-fw" aria-hidden="true"></i>{{ $store->phone }}</li>
                        <li><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>{{ $store->getAddress->schedule }}</li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="@if($store->catalog_link) col-md-9 @else col-md-12 @endif">
                @if($categories->count())
                    <nav class="store_menu clearfix">
                        @foreach($categories as $category)
                            <a href="{{ route('store.showByCat', [$store->slug, $category->slug]) }}" title="{{ $category->name }}">{{ $category->name }}</a>
                        @endforeach
                    </nav>
                @endif
            </div>
            @if($store->catalog_link)
                <div class="col-md-3">
                    <a class="download_catalog" href="{{ url($store->catalog_link) }}" target="_blank">Kataloqu yüklə</a>
                </div>
            @endif
        </div>
        @if($store->whatsapp)
            <a class="whatsapp" href="https://wa.me/{{ $store->whatsapp }}">Whatsapp ilə əlaqə saxlayın</a>
        @endif

        @include('web.elements.products', [
            'productLists' => $products,
            'productListTitle' => 'Elanlar',
            'margin' => true,
            'loadMore' => true,
            'productsCount' => $productsCount
        ])
    </div>

@endsection

@push('scripts')
    @if($products->count() > 23)
        <script type="text/javascript">
            loadMore({{ request()->get('page') ?: 1 }}, false);
        </script>
    @endif
@endpush
