@extends ('layouts.web', ['page_heading' => $pageTitle ] )

@section ('content')
    <div class="col-xs-12">
        @include('web.elements.breadcrumbs', [
            'breadcrumbs' => [
                $pageTitle => route('store.all')
            ]
        ])
    </div>

    <div class="col-xs-12">
        @include('web.elements.stores-section', ['stores' => $stores, 'loadMore' => true])
    </div>

@endsection

@push('scripts')
    @if($stores->count() >= 18)
        <script type="text/javascript">
            loadMore({{ request()->get('page') ?: 1 }}, false);
        </script>
    @endif
@endpush