<!DOCTYPE html>
<html lang="az">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover">
    <meta name="theme-color" content="#EDF1F3">
    <meta property="author" content="AqroMarket">
    <title>AqroMarket</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link href="{{ asset('aqrostatic/css/style.min.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6"><img src="{{ asset('aqrostatic/images/logo.svg') }}" /></div>
            <ul class="col-md-6">
                <li><b>Ünvan:</b> Novxanı qəsəbəsi, Heydər Əliyev küçəsi 1</li>
                <li><b>Telefon:</b> 050 836 48 48</li>
            </ul>
        </div>
    </div>
</header>
<div class="slider">
    <img src="{{ asset('aqrostatic/images/slider.png') }}?v=1"/>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8"><div class="about">
            <div class="title">Haqqımızda</div>
            <div class="text">
                <p>Aqromarket Azərbaycanda müasir ət kəsimi avadanlıqları ilə təchiz olunmuş kəsim məntəqəsidir. Bu kəsim məntəqəsində bütün ət kəsimi prosesləri avtomatlaşdırılmış şəkildə həyata keçirilməkdədir. Türkiyədən gətirilmiş ət kəsimi avadanlıqları və antibakterial mühitdə kəsilən heyvanlar Azərbaycan Qida Təhlükəsizliyi Agentliyinin nümayəndəsinin laborator yoxlanışından keçdikdən sonra satışa çıxarılır.</p>
                <p>Məntəqə daxilində qurulmuş sistem vasitəsilə kəsilən heyvanların qanı və digər istifadəyə yararsız hissələri ekologiyaya zərər vurmadan xüsusi qaydada utilizasiya olunur. Aqromarket ət kəsim məntəqəsi, müştəri məmnuniyyətini üstün tutaraq məntəqəyə gələn qonaqlara kiçik kiçik kafesində ödənişsiz xidmət göstərməkdədir.</p>
                <p>Aqromarket müştərilərini yüksək keyfiyyətli ət və ət məhsulları ilə təmin etmək istəyən fiziki şəxs və ya iaşə obyektləri ilə əməkdaşlıq etməkdə maraqlıdır. Keyfiyyətə üstünlük verənlər müraciət formasını dolduraraq ət kəsim məntəqəsinə müraciət edə bilərlər.</p>
            </div>
        </div></div>
        <div class="col-md-4">
            <div class="form">
                <h3 class="title">Müraciət Formu</h3>
                <form method="post" action="">
                    <div class="input">
                        <input id="name" name="name" required type="text" />
                        <label for="name">Ad, Soyad</label>
                    </div>
                    <div class="input">
                        <input id="phone" data-mask="phone" name="phone" required type="text" />
                        <label for="phone">Nömrə</label>
                    </div>
                    <div class="input">
                        <input id="email" name="email" required type="email" />
                        <label for="email">E-poçt</label>
                    </div>
                    <div class="input">
                        <textarea id="comment" name="comment" required></textarea>
                        <label for="comment">Mesajınız</label>
                    </div>
                    <div class="btn text-right">
                        <button type="submit">Təsdiq et</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="gallery container">
    <h3 class="ttitle">Qalereya</h3>
    <div class="row items">
        @for($i=1; $i<13; $i++)
            <div class="col-md-3"><a href="{{ asset('storage/aqromarket/1024x870/'.$i.'.png') }}" data-fancybox="gallery"><img src="{{ asset('storage/aqromarket/400x340/'.$i.'.png') }}" /></a></div>
        @endfor
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 advantage">
            <h3 class="ttitle">Üstünlükərimiz</h3>
            <ul class="row">
                <li class="col-md-6">
                    <div class="title lab">Laboratoriya</div>
                    <div class="text">Ət və ət məhsullarımız AQTA-nin nəzarəti altında satılır.</div>
                </li>
                <li class="col-md-6">
                    <div class="title tex">Texnologiya</div>
                    <div class="text">Ət kəsimi prosesi tam avtomatlaşdırılmış formada həyata keçirilir.</div>
                </li>
                <li class="col-md-6">
                    <div class="title gig">Gigiyena</div>
                    <div class="text">Məntəqənin inşası zamanı anti-bakterial materiallardan istifadə olunmuşdur.</div>
                </li>
                <li class="col-md-6">
                    <div class="title ist">İstirahət</div>
                    <div class="text">Müştərilərin gözləməsi üçün məntəqədə ödənişsiz kiçik kafe fəaliyyət göstərir.</div>
                </li>
                <li class="col-md-12">
                    <div class="title qes">Qəssab</div>
                    <div class="text">Ət kəsim prosesinə peşəkar qəssablar nəzarət olunur.</div>
                </li>
            </ul>
        </div>
        <div class="col-md-6 video">
            <h3 class="ttitle">Video</h3>
            <a data-fancybox href="https://www.youtube.com/watch?v=I8Lc3_dn7-c"><img src="{{ asset('aqrostatic/images/aqromarket.png') }}?v=2" /></a>
        </div>
    </div>
</div>
<script src="{{ asset('aqrostatic/js/vendor.min.js') }}"></script>
<script src="{{ asset('aqrostatic/js/script.min.js') }}"></script>
</body>
</html>
