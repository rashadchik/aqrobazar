@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')
    @include('web.elements.breadcrumbs')

    <!-- PdfList Begin -->
    <section class="pdf_list">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    @foreach($legislation as $l)
                        <article>
                            <p>{{ $l->title }}</p>
                            <span class="pdf">
                                <a href="{{ asset($l->getFirstMediaUrl()) }}" title="{{ $l->title }}" download>
                                    <i></i>
                                </a>
					        </span>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- PdfList End -->
@endsection