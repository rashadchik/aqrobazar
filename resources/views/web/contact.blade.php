@extends ('layouts.web', [ 'page_heading' => $page->name ] )

@section ('content')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs')
        <!-- Contact Begin -->
        <section class="contact">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <!-- ContactInfo -->
                    <div class="contact_info">
                        <h2>{{ $dictionary['contact_us'] }}</h2>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i> {{ $dictionary['address'] }}</li>
                            <li><i class="fa fa-phone fa-fw" aria-hidden="true"></i> {{ $config['contact_phone'] }}</li>
                            <li><i class="fa fa-at fa-fw" aria-hidden="true"></i> {{ $config['contact_email'] }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="contact_form">
                        {!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'form']) !!}
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" name="full_name" placeholder="*{{ $dictionary['full_name'] }}" class="ipt_style">
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <input type="text" name="email" placeholder="{{ $dictionary['email'] }}" class="ipt_style">
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <input type="text" name="phone" placeholder="* {{ $dictionary['phone'] }}" class="ipt_style maskphone">
                            </div>
                            <div class="col-xs-12">
                                <textarea name="text" placeholder="*Mesajınız..." class="ipt_style"></textarea>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit">{{ $dictionary['send'] }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="contact_map">
                        <h2>Bizi rahat tapın</h2>
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact End -->
    </div>

@endsection

@push('scripts')

    @include('web.elements.form',['type' => 'contact'])

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $config['google_api_key'] }}&language={{ $lang }}"></script>
    <script type="text/javascript">


        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var markericon = {
                url: '{{ asset('images/icon/map-marker.png') }}',
                //state your size parameters in terms of pixels
                origin: new google.maps.Point(0,0)
            };

            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng({{ $config['location'] }})
            };

            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng({{ $config['location'] }}),
                map: map,
                icon: markericon
            });

            var myoverlay = new google.maps.OverlayView();
            myoverlay.draw = function () {
                this.getPanes().markerLayer.id='markerLayer';
            };
            myoverlay.setMap(map);
        }

    </script>
@endpush