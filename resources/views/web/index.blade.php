@extends ('layouts.web', ['page_heading' => null ] )

@section ('content')

    <div class="col-xs-12">
        <!-- Search Begin -->
        @include('web.elements.search')
        <!-- Search End -->
        @include('web.elements.main-categories')
        <!-- Products Begin -->
        <section class="products">
            <div class="row">
                <div class="col-xs-12">
                    @if($vipCount > 0)
                        @include('web.elements.products-section', ['productLists' => $products, 'productListTitle' => 'VIP Elanlar', 'vipType' => 2, 'showAll' => route('vip.all')])
                    @endif

                    @if($premiumCount > 0)
                        @include('web.elements.products-section', ['productLists' => $products, 'productListTitle' => 'Premium Elanlar', 'vipType' => 3])
                    @endif
                </div>
            </div>
        </section>
        <!-- Products End -->
        <hr>
        @includeIf('web.elements.stores-section', ['stores' => $stores, 'showAll' => true, 'homePage' => true])
        <hr class="mb20">
        <section class="products">
            <div class="row">
                <div class="col-xs-12">
                    @include('web.elements.products-section', ['productLists' => $products, 'productListTitle' => 'Son Elanlar', 'vipType' => 1, 'showAll' => route('ads.all')])
                    <div class="search_result" style="padding-top: 0">
                        <a href="{{ route('ads.all') }}">Hamısını göstər</a>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
