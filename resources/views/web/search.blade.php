@extends ('layouts.web', ['page_heading' => $dictionary['searched_result_title'] ?? 'Axtarışın nəticələri'])

@section ('content')

	<div class="col-xs-12">
		<!-- Search Begin -->
		@include('web.elements.search')

		@if(count($products) == 0)
			<section class="search_result">
				<p>Axtarışa uyğun nəticə tapılmadı</p>
				<a href="{{ route('ads.all') }}">Bütün elanlar</a>
			</section>
		@else
			@include('web.elements.products', [
                'productLists' => $products,
                'productListTitle' => 'Elanlar',
                'noBorder' => true,
                'margin' => true,
                'loadMore' => true
            ])
		@endif
	</div>

@endsection

@push('scripts')
	@if($products->count() > 15)
		<script type="text/javascript">
            loadMore({{ request()->get('page') ?: 1 }}, false);
		</script>
	@endif
@endpush