@extends ('layouts.web', [ 'page_heading' => $page->name ] )

@section ('content')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs')
        <!-- Section Begin -->

        <section class="rules">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="text">
                            {!! $page->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection