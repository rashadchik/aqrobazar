@extends ('layouts.web', ['page_heading' => $page->name ?? $title ])

@section ('content')

    @if(!isset($hideSearch))
        <div class="col-xs-12">
            @if(isset($other) && $other == true)
                @include('web.elements.breadcrumbs', ['data' => ['title' => 'Digər']])
            @else
                @include('web.elements.breadcrumbs')
            @endif
            <!-- Search Begin -->
            @include('web.elements.search', ['padding' => 0])
        </div>
    @endif

    @if($products->count() == 0 && $vipProducts->count() == 0)
        <div class="col-xs-12">
            <section class="search_result">
                <p>Elan tapılmadı</p>
                <a href="{{ route('ads.all') }}">Bütün elanlar</a>
            </section>
        </div>
    @else
        @if(isset($vipProducts) && $vipProducts->count() > 0)
            <div class="col-xs-12">
                @include('web.elements.products', ['productLists' => $vipProducts, 'productListTitle' => 'VIP Elanlar', 'vipType' => 2, 'margin' => true, 'bookmark' => $bookmark ?? false])
            </div>
        @endif

        <div class="col-xs-12">
            @include('web.elements.products', [
                'productLists' => $products,
                'productListTitle' => $productListTitle ?? 'Elanlar',
                'vipType' => 1,
                'bookmark' => $bookmark ?? false,
                'margin' => true,
                'loadMore' => true
            ])
        </div>
    @endif

@endsection

@push('scripts')
    @if($products->count() > 31)
        <script type="text/javascript">
            loadMore({{ request()->get('page') ?: 1 }}, false);
        </script>
    @endif
@endpush