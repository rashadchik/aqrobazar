@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- FAQ Begin -->
    <section class="faq">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>{{ $page->name }}</h1>
                </div>
                <div class="col-xs-12">
                    @foreach($faq as $q)
                        <div class="item">
                            <div class="head">
                                <h2>{{ $q->title }}</h2>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </div>
                            <div class="body">
                                <p>{{ $q->text }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- FAQ End -->
@endsection