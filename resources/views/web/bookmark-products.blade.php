@extends ('layouts.web', ['page_heading' => $page->name ?? $title ])

@section ('content')

    @if(!isset($hideSearch))
        <div class="col-xs-12">
            @include('web.elements.breadcrumbs')
            <!-- Search Begin -->
            @include('web.elements.search', ['padding' => 0])
        </div>
    @endif

    <div class="col-xs-12">
        @if($products->count() == 0)
            <section class="search_result">
                <p>Elan tapılmadı</p>
                <a href="{{ route('ads.all') }}">Bütün elanlar</a>
            </section>
        @else
            @include('web.elements.products', [
                'productLists' => $products,
                'productListTitle' => $productListTitle ?? 'Elanlar',
                'vipType' => 1,
                'bookmark' => $bookmark ?? false,
                'margin' => true,
                'loadMore' => true
            ])
        @endif
    </div>

@endsection

@push('scripts')
    @if($products->count() > 19)
        <script type="text/javascript">
            loadMore({{ request()->get('page') ?: 1 }}, false);
        </script>
    @endif
@endpush