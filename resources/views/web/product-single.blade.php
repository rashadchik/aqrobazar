@extends ('layouts.web', [
    'page_heading' => $product->name,
    'page_image' => asset($product->getFirstMedia()->getUrl('lg')),
    'fb_link' => $fbLink
])

@section ('content')

    @include('widgets.modal-ajax')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs')
    </div>

    @if(!is_null($product->store_id))
        <div class="col-xs-12">
            <div class="store_header">
                <div class="row mobile_flex">
                    <div class="col-md-2 col-xs-3">
                        <img src="{{ asset($product->getStore->getFirstMedia('logo')->getUrl('sm')) }}" alt="{{ $product->getStore->name }}" style="border-radius: 50%">
                    </div>
                    <div class="col-md-5 col-xs-9">
                        <h2 class="title">{{ $product->getStore->name }}</h2>
                        <div class="desc">{{ $product->getStore->getTrans->summary }}</div>
                        <div class="icon view hidden-xs">{{ $product->getStore->view_count }}</div>
                    </div>
                    <div class="col-md-5 cntct_info hidden-xs">
                        <div class="icon map">{{ $product->getStore->getAddress->address }}</div>
                        <div class="icon phone">{{ $product->getStore->phone }}</div>
                        <div class="icon clock">{{ $product->getStore->getAddress->schedule }}</div>
                    </div>
                </div>
                <div class="row row2">
                    <div class="col-md-2 col-xs-12">
                        <a class="store_ads" href="{{ route('store.show', $product->getStore->slug) }}">Elanlar ({{ $product->getStore->get_product_count_count }})</a>
                    </div>
                    <div class="col-md-10">
                        <div class="store_slogan hidden-xs"><a href="{{ route('store.show', $product->getStore->slug) }}" target="_blank" style="color: #19814a">Mağazaya daxil ol</a></div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="col-xs-12">
        <!-- Detail Begin -->
        <section class="detail">
            <h1 class="head_title">{{ $page->name }} </h1>
            <div class="row">
                @if($product->status != 1)
                    <div class="col-md-12">
                        @if($product->status == 0)
                            <div class="alert alert-warning"><b>Elanınız yoxlanılır.</b></div>
                        @elseif($product->status == 2)
                            <div class="alert alert-warning"><b>Elanınız mwddəti bitib. Elan ana səhifədə və axtarışlarda yer almayacaq.</b></div>
                        @else
                            <div class="alert alert-danger"><b>Elanınız yoxlanadan keçmədi və dərc olunmayacaq.</b></div>
                        @endif
                    </div>
                @endif
                <div class="col-md-6 col-xs-12">
                    <!-- ProductGallery Begin -->
                    <div class="product_gallery">
                        <div class="stage">
                            <div class="carousel-stage">
                                <ul class="list-unstyled">
                                    @foreach($product->getMedia() as $img)
                                        <li>
                                            <img src="{{ $img->getUrl('lg') }}" alt="{{ $product->name }}">
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            @if($product->getMedia()->count() > 1)
                            <a href="#" class="prev prev-stage"></a>
                            <a href="#" class="next next-stage"></a>
                            @endif
                        </div>
                        @if($product->getMedia()->count() > 1)
                            <div class="navigation">
                                <div class="carousel-navigation">
                                    <ul class="list-unstyled">
                                        @foreach($product->getMedia() as $img)
                                            <li><img src="{{ $img->getUrl('sm') }}" alt="{{ $product->name }}"></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <a href="#" class="prev prev-stage"></a>
                                <a href="#" class="next next-stage"></a>
                            </div>
                        @endif
                    </div>
                    <!-- ProductGallery End -->
                </div>
                <div class="col-md-6 col-xs-12">
                    <h1 class="title">{{ $product->name }}</h1>
                    <span class="product_price">{{ $product->price }} AZN</span>
                    <span class="line"></span>
                    <ul class="list-unstyled info">
                        <li><strong>Elanın sahibi:</strong> {{ $product->user_name }}</li>
                        <li><strong>Əlaqə Nömrəsi:</strong> {{ filterPhone($product->user_phone) }}</li>
                        <li><strong>Elanın ID nömrəsi:</strong> {{ $product->id }}</li>
                        <li><strong>Xidmətin növü:</strong> {{ trans('locale.ad-type.'.$product->type) }}</li>
                        <li><strong>Şəhər:</strong> {{ $product->city->{'name_'.$lang} }},  {{ $product->city->country->tr->name }}</li>
                        <li><strong>Kateqoriya:</strong> {{ $product->getPageTrans->name }}</li>
                    </ul>
                    @if(!is_null($product->user_id))
                        <p class="link_all"><a href="{{ route('user.products', $product->user_id) }}">İstifadəçinin bütün elanları</a></p>
                    @endif
                    <div class="description">
                        <h4>Elan haqqında məlumat</h4>
                        <div class="text">
                            <p>{!! $product->summary !!}</p>
                        </div>
                    </div>
                    <span class="date">Yeniləndi: {{ $product->updated_at }}</span>
                    <div class="bottom clearfix">
                        <span class="view">{{ $product->seen }}</span>
                        <button type="button" class="add_favorite @if($product->hasBookmark) active @endif" data-id="{{ $product->id  }}">
                            @if($product->hasBookmark)
                                <span>Seçilmişlərdədir</span>
                            @else
                                <span>Seçilmişlərə əlavə et</span>
                            @endif
                        </button>
                        <button type="button" class="complain_btn open_lr" data-type="complain">Elanı şikayət et</button>
                    </div>
                    <ul class="list-unstyled service_action">

                        @if(is_null($product->deleted_at))
                            <li>
                                <button type="button" class="open_lr warning-modal-open" data-type="edit_post"
                                        data-action="{{ route('ads.confirm', $product->id) }}">Düzəliş et</button>
                            </li>
                            <li>
                                <button type="button" class="open_lr warning-modal-open" data-type="delete_post"
                                        data-action="{{ route('ads.confirm', $product->id) }}">Elanı sil</button>
                            </li>

                            @foreach(config('config.price-route') as $key => $route)
                                <li class="red">
                                    <button type="button" class="open-modal-dialog" data-link="{{ route("$route.modal", $product->id) }}">
                                        {{ trans('locale.price-list.'.$key) }}<span>{{ $minAmount[$key] }} AZN-dən</span>
                                    </button>
                                </li>
                            @endforeach
                        @endif

                        <li class="blue">
                            <!-- Your like button code -->
                            <div class="fb-like"
                                 data-href="{{ $fbLink }}"
                                 data-layout="button_count"
                                 data-action="like"
                                 data-size="large"
                                 data-share="true"
                                 data-show-faces="true">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- Detail End -->
        @if($similarProducts->count() > 0)
            @include('web.elements.products', ['productLists' => $similarProducts, 'productListTitle' => 'Oxşar Elanlar', 'padding' => 'pb50'])
        @endif
    </div>

@endsection
