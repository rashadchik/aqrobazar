@extends ('layouts.web', ['page_heading' => $title])

@section ('content')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs', ['page' => $page ?? null, 'data' => ['title' => $title] ])
    </div>
    <!-- AddPost Begin -->
    <div class="col-xs-12">
        <div class="addpost clearfix">
            @if($errors->any())
                <div class="alert alert-danger alert-circled alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ $errors->first() }}</strong>
                </div>
            @endif
            <h1>{{ $title }}</h1>
            <div class="form">
                {!! Form::open(['id' => 'form', 'route' => isset($product) ? ['ads.update', $product->id] : 'ads.store', 'method' => $method ?? 'POST', 'files' => true]) !!}

                {!! Form::hidden('token', old('token', $token)) !!}
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">Kateqoriyanı seçin:</label>
                        {!! Form::select('category_id', $categories, old('category_id', $mainCategory->id ?? null), ['class' => 'ipt_style dynamic', 'data-dependent' => 'subcategory']) !!}
                    </div>
                    <div id="subcategory-box" class="col-sm-6 col-xs-12">
                        <label class="lbl">Alt Kateqoriyanı seçin:</label>
                        <select class="ipt_style" id="subcategory" name="property_id" data-selected="{{ $product->category_id ?? ''}}" disabled></select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">Elanın adı *</label>
                        <input type="text" name="name" class="ipt_style" value="{{ old('name', $product->name ?? null) }}">
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">Xidmətin növünü seçin:</label>
                        {!! Form::select('type', trans('locale.ad-type'), old('type', $product->type ?? null), ['class' => 'ipt_style']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">Qiymət:</label>
                        <input type="number" name="price" min="0.01" max="10000000" step="0.01" class="ipt_style text-right" value="{{ old('price', $product->price ?? null) }}">
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">Şəkil:</label>
                        <div class="dropzone" id="real-dropzone">
                            <div class="dz-default dz-message">
                                <button class="fileupload pull-left" type="button">Əlavə et</button>
                            </div>
                            <div class="fallback">
                                {!! Form::file('file', ['multiple']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 dropzone">
                        <div class="dropzone-previews" id="dropzonePreview"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <label class="lbl">Məzmun:</label>
                        <textarea name="summary" placeholder="Elanla bağlı mətni daxil edin" class="ipt_style">{{ old('summary', $product->summary ?? null) }}</textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <label class="lbl">Ad, Soyad:</label>
                        <input type="text" name="user_name" class="ipt_style" value="{{ old('user_name', isset($product) ? $product->user_name : auth()->guard('web')->user()->name ?? null) }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">Ölkə:</label>
                        {!! Form::select('country', $countries, old('country', isset($product) ? $product->getCity->country->id : auth()->guard('web')->user()->city->country->id ?? 1), ['class' => 'ipt_style dynamic', 'data-dependent' => 'city']) !!}
                    </div>
                    <div id="city-box" class="col-sm-6 col-xs-12">
                        <label class="lbl">Şəhər:</label>
                        <select class="ipt_style" id="city" name="city_id" data-selected="{{ isset($product) ? $product->city_id : old('city_id', auth()->guard('web')->user()->city->id ?? 1) }}"></select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">{{ $dictionary['phone'] }}:</label>
                        <input type="text" name="user_phone" value="{{ old('full_phone', isset($product) ? $product->user_phone : auth()->guard('web')->user()->phone ?? null) }}" class="ipt_style int-phone" style="padding: 0 6px 0 52px !important;" placeholder=" ">
                        <span class="tel-output" style="color:red"></span>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">{{ $dictionary['email'] }}:</label>
                        <input type="email" name="user_email" value="{{ old('user_email', $product->user_email ?? isset($product) ? $product->user_email : auth()->guard('web')->user()->email ?? null) }}" class="ipt_style">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">Elanın növü:</label>
                        <div class="radiobox clearfix">
                            <input type="radio" name="adType" id="simple" class="css-checkbox" checked="checked" value="1">
                            <label for="simple" class="css-label">Sadə elan</label>
                            <input type="radio" name="adType" id="vip" class="css-checkbox" value="2">
                            <label for="vip" class="css-label">VIP elan</label>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <label class="lbl">&nbsp;</label>
                        <div class="conditions checkbox">
                            <input type="checkbox" name="conditions" id="conditions" class="css-checkbox">
                            <label for="conditions" class="css-label">Elanı yükləməklə Tələb və Şərtlərlə razı oluram</label>
                        </div>
                    </div>
                </div>
                <button type="submit" id="submit" data-loading-text="{{ loading() }}">Göndər</button>
                <p id="loading" class="text-center"></p>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- AddPost End -->

@endsection

@push('scripts')

    <script src="{{ asset("js/dropzone.js") }}"></script>

    <script>

        var telInput = document.querySelector(".int-phone"),
            telOutput = document.querySelector(".tel-output");

        var telIti = window.intlTelInput(telInput, {
            hiddenInput: "full_phone",
            onlyCountries: {!! $countryCodes !!},
            utilsScript: "../js/utils.js"
        });

        var handleTelChange = function() {
            var text = (telIti.isValidNumber()) ? telIti.getNumber() : "Yanlış nömrə";
            var textNode = document.createTextNode(text);
            telOutput.innerHTML = "";
            telOutput.appendChild(textNode);

            if(telIti.isValidNumber()) {
                telOutput.innerHTML = "";
            }
        };

        // listen to "keyup", but also "change" to update when the user selects a country
        telInput.addEventListener('change', handleTelChange);
        telInput.addEventListener('keyup', handleTelChange);


        var modelId = '{{ $model->id ?? '' }}';
        var categoryId = '{{ $mainCategory->id ?? '' }}';
        var _token = $('input[name="_token"]').val();
        var _method = $('input[name="_method"]').val();
        var token = $('input[name="token"]').val();

        $(document).ready(function(){

            $('.dynamic').change(function () {

                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var selected = $('#'+dependent).data('selected');

                if(value != ''){

                    $.ajax({
                        url: "/"+dependent,
                        method: "POST",
                        data: {value:value, _token:_token, selected:selected}
                    })
                    .done(function(data) {

                        if(data != ''){

                            $('#'+dependent).html(data).val();

                            //$('#'+dependent+'-box').slideDown();

                            $('#'+dependent+'-box label').css("color", "#333");
                            $('#'+dependent).attr('disabled', false);
                        }
                        else{
                            $('#'+dependent+'-box label').css("color", "#92A1B1");
                            $('#'+dependent).attr('disabled', true).find('option').remove();
                        }
                    });
                }
                else{
                    $('#'+dependent+'-box label').css("color", "#92A1B1");
                    $('#'+dependent).html('').attr('disabled', true).find('option').remove();
                }
            }).change();


            Dropzone.options.realDropzone = {
                params: {
                    _token: _token,
                    token: token,
                    model_id: modelId,
                    method_option: _method
                },
                url: '/file/upload',
                type: 'POST',
                uploadMultiple: false,
                acceptedFiles: '.png,.gif,.jpeg,.jpg, image/*',
                parallelUploads: 3,
                maxFiles: 20,
                maxFilesize: 30,
                previewsContainer: '#dropzonePreview',
                addRemoveLinks: true,
                dictRemoveFile: '<i class="fa fa-remove text-danger"></i>',
                dictCancelUpload: 'İmtina',
                dictFileTooBig: 'Fayl 30 MB-dan artıq ola bilməz',
                //previewTemplate: document.querySelector('#preview-template').innerHTML,

                init:function() {

                    var myDropzone = this;

                    if(modelId != '')   //edit bash tutarsa
                    {
                        $.get('/file/get/' + modelId, function(data) {

                            $.each(data.images, function (key, value) {

                                var file = {name: '', size: '', file_id: value.file_id, deleteRoute: value.deleteRoute};
                                myDropzone.options.addedfile.call(myDropzone, file);
                                myDropzone.options.thumbnail.call(myDropzone, file, value.path);
                                myDropzone.emit("complete", file);
                            });
                        });
                    }


                    this.on("removedfile", function(file) {

                        var url = file.deleteRoute;

                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {_token: _token, _method: 'DELETE', id: file.file_id, dropzone: true},
                            dataType: 'html',
                            success: function(response){

                            }
                        });

                    } );

                    this.on("addedfile", function (file) {
                        $("#submit").attr('disabled', true).button('loading');
                        $("#loading").html('Şəkillər yüklənir... Xahiş olunur, bir az gözləyin');
                    });
                    this.on("queuecomplete", function (file) {
                        $("#submit").attr('disabled', false).button('reset');
                        $("#loading").html('');
                    });
                },
                error: function(file, response) {
                    if(response.code == 400){
                        toastr.error(response.message);
                    }

                    if($.type(response) === "string")
                        var message = response; //dropzone sends it's own error messages in string
                    else
                        var message = response.message;
                    file.previewElement.classList.add("dz-error");
                    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                    _results = [];
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        node = _ref[_i];
                        _results.push(node.textContent = message);
                    }
                    return _results;
                },
                success: function(file, response) {
                    file.deleteRoute = response.deleteRoute;
                    file.file_id = response.file_id;
                    file.code = response.code;
                    file.msg = response.message;
                },
                complete: function complete(file) {

                    if (file._removeLink) {
                        file._removeLink.innerHTML = this.options.dictRemoveFile;
                    }
                    if (file.previewElement) {
                        return file.previewElement.classList.add("dz-complete");
                    }
                }
            };

        });



        $(function(){
            document.querySelectorAll('input[type=file]')[0].setAttribute("accept","image/*");


            $("#form").validate({
                rules: {
                    category_id: {
                        required: true
                    },
                    name: {
                        required: true
                    },
                    price: {
                        required: true
                    },
                    summary: {
                        required: true,
                        maxlength: 1000
                    },
                    user_name: {
                        required: true
                    },
                    user_email: {
                        required: true,
                        email: true
                    },
                    user_phone: {
                        required: true
                    }
                },
                messages: {
                    category_id: {
                        required: '{{ trans('validation.create_product.category') }}'
                    },
                    name: {
                        required: '{{ trans('validation.create_product.name') }}'
                    },
                    summary: {
                        required: '{{ trans('validation.create_product.summary') }}',
                        maxlength: 'Məzmun çox uzundur'
                    },
                    price: {
                        required: '{{ trans('validation.create_product.price') }}'
                    },
                    user_name: {
                        required: '{{ trans('validation.create_product.user_name') }}'
                    },
                    user_email: {
                        required: '{{ trans('validation.custom.email.required') }}',
                        email: '{{ trans('validation.custom.email.email') }}'
                    },
                    user_phone: {
                        required: '{{ trans('validation.custom.phone.required') }}'
                    },
                    city_id: {
                        required: 'Şəhər seçilməyib'
                    }

                },
                success: function(label) {
                    label.html('').removeClass('error-field').addClass('ok');
                },
                errorPlacement: function(error, element) {
                    $(element).next('.help-block').addClass( "help-block-error").html(error.text());
                },
                highlight: function ( element, errorClass, validClass ) {
                    $(element).addClass( "error-field" ).removeClass( "valid-item" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).addClass( "valid-item" ).removeClass( "error-field" );
                }

            });

            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z\s]+$/i.test(value);
            }, "Only alphabetical characters");

        });



    </script>
@endpush