@extends ('layouts.web', ['page_heading' => $title ])

@section ('content')

    @if(!isset($hideSearch))
        <div class="col-xs-12">
            @if(isset($other) && $other == true)
                @include('web.elements.breadcrumbs', ['data' => ['title' => 'Digər']])
            @else
                @include('web.elements.breadcrumbs')
            @endif
            <!-- Search Begin -->
            @include('web.elements.search', ['padding' => 0])
        </div>
    @endif

    @if($vipProducts->count() == 0)
        <div class="col-xs-12">
            <section class="search_result">
                <p>Elan tapılmadı</p>
                <a href="{{ route('ads.all') }}">Bütün elanlar</a>
            </section>
        </div>
    @else
        <div class="col-xs-12">
            @include('web.elements.products', [
                'productLists' => $vipProducts,
                'productListTitle' => $title,
                'bookmark' => false,
                'margin' => true,
                'loadMore' => true
            ])
        </div>
    @endif

@endsection

@push('scripts')
    @if($vipProducts->count() > 31)
        <script type="text/javascript">
            loadMore({{ request()->get('page') ?: 1 }}, false);
        </script>
    @endif
@endpush