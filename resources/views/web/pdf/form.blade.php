<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text-html; charset=utf-8">
    <title>@yield('title')</title>

    <style type="text/css">
        td {
            line-height: 1.8em;
        }

        tr {
            margin-bottom: 2em;
        }

        td.input {
            border-bottom: 0.5px solid #000;
            text-align: center;
            font-weight: normal;
            font-style: italic;
        }

        .title{
            padding-top:60px;
            text-align:center;
            font-style: italic;

            font-size:24px;

        }
        .title2{
            padding-top:30px;
            font-style: italic;
            font-size:18px;
        }

        .font1{
            text-align: center;
            vertical-align: top !important
        }
        .font1 span{
            font-size: 12px;
            margin-top: -20px
        }
        img{
            width:200px;
            margin-bottom: 20px
        }
    </style>

</head>
<body style="font-family:'dejavu sans';">

<table border="0" width="100%" align="center">
    <tr>
        <td align="left">Ad:</td>
        <td>{{ $content['name'] }}</td>
    </tr>
    <tr>
        <td align="left">Soyad:</td>
        <td>{{ $content['surname'] }}</td>
    </tr>
    <tr>
        <td align="left">Doğum tarixi:</td>
        <td>{{ $content['date'] }}</td>
    </tr>

    <tr>
        <td align="left">*Ailə vəziyyəti:</td>
        <td>{{ $content['family_status'] }}</td>
    </tr>

    <tr>
        <td align="left">*Müraciət olunan vəzifə:</td>
        <td>{{ $content['job'] }}</td>
    </tr>

    <tr>
        <td align="left">*Telefon nömrəsi:</td>
        <td>{{ $content['phone2'] }} @if($content['phone1']) && {{$content['phone1'] }} @endif</td>
    </tr>

    <tr>
        <td align="left">*E-poçt:</td>
        <td>{{ $content['email'] }}</td>
    </tr>

    @if($content['superior_features'])
        <tr>
            <td align="left">Başqa gənclərdən fərqli üstün cəhətləri</td>
            <td>{{ $content['superior_features'] }}</td>
        </tr>
    @endif

    @if($content['other_information'])
        <tr>
            <td align="left">Əlavə məlumat</td>
            <td>{{ $content['other_information'] }}</td>
        </tr>
    @endif
</table>


@if(count($content['education']) > 0 )

    <table border="1" width="100%" align="center" style="margin-top: 20px;">

        <tr class="border">
            <th align="center">T.Müəssisə adı</th>
            <th align="center">Daxil olduğu il	</th>
            <th align="center">Bitirdiyi il </th>
            <th align="center">Fakultə(ixtisas)</th>
        </tr>
        @foreach($content['education'] as $key=>$education)
            <tr>
                <td align="center">{{ $education  ?? '---------' }}</td>
                <td align="center">{{ $content['start_date'][$key]  ?? '---------'}}</td>
                <td align="center">{{ $content['end_date'][$key]  ?? '---------'}} </td>
                <td align="center">{{ $content['speciality'][$key]  ?? '---------'}}</td>
            </tr>
        @endforeach

    </table>

@endif

@if(count($content['foreign_lang']) > 0)

    <table border="1" width="100%" align="center" style="margin-top: 20px;">

        <tr class="border">
            <th align="center">Xarici dil		</th>
            <th align="center">---</th>
        </tr>
        @foreach($content['foreign_lang'] as $key=>$foreign_lang)
            <tr>
                <td align="center">{{ $foreign_lang  ?? '---------' }}</td>
                <td align="center">{{ $content['lang'][$key]  ?? '---------'}}</td>
            </tr>
        @endforeach

    </table>

@endif

@if(count($content['comp_skills']) > 0 )

    <table border="1" width="100%" align="center" style="margin-top: 20px;">

        <tr class="border">
            <th align="center">Kompyuter bilikləri	</th>
            <th align="center">---</th>
        </tr>
        @foreach($content['comp_skills'] as $key=>$comp_skills)
            <tr>
                <td align="center">{{ $comp_skills  ?? '---------' }}</td>
                <td align="center">{{ $content['skils'][$key]  ?? '---------'}}</td>
            </tr>
        @endforeach

    </table>

@endif


@if(count($content['courses_title']) > 0 )

    <table border="1" width="100%" align="center" style="margin-top: 20px;">

        <tr class="border">
            <th align="center">ştirak etdiyiniz təlim, məşğələ və kurslar	</th>
            <th align="center">---</th>
        </tr>
        @foreach($content['courses_title'] as $key=>$courses_title)
            <tr>
                <td align="center">{{ $courses_title  ?? '---------' }}</td>
                <td align="center">{{ $content['cources_date'][$key]  ?? '---------'}}</td>
            </tr>
        @endforeach

    </table>

@endif
</body>
</html>
