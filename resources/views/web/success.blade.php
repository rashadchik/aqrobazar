@extends ('layouts.web', ['page_heading' => null ] )

@section ('content')
    <!-- Search Begin -->
    @include('web.elements.search')
    <!-- Search End -->
    @include('web.elements.main-categories')
    @include('web.elements.products', ['productLists' => $products, 'productListTitle' => 'Elanlar', 'border' => true])

@endsection
