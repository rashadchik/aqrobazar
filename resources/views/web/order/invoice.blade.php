@extends ('layouts.web', [ 'page_heading' => $dictionary['basket'] ] )

@section ('content')

    <div class="col-xs-12">
        @include('web.elements.breadcrumbs', ['page' => null, 'title' => $dictionary['basket'] ])
    </div>

    <!-- Section Begin -->
    <div class="col-xs-12">
        <section class="pt50 pb80">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-push-8 col-xs-12">
                        <!-- Faktura -->
                        <div class="faktura">
                            <div class="clearfix">
                                <h1>{{ $dictionary['invoice'] }}</h1>
                                <div class="pull-right">
                                    <a href="{{ route('order.invoice.pdf', $order->id) }}?type=stream" target="_blank"> <i class="fa fa-print" aria-hidden="true"></i></a>
                                    <a href="{{ route('order.invoice.pdf', $order->id) }}"><i class="fa fa-download" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Information -->
                        <div class="information">
                            <div class="clearfix">
                                <h1>{{ $dictionary['delivery_title'] }}</h1>
                            </div>
                            <div class="text">
                                <p>{{ $dictionary['delivery_text'] }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-sm-pull-4 col-xs-12">
                        <!-- StepCompleted -->
                        <!-- StepNav -->
                        <div class="step_nav">
                            <div class="row">
                                <div class="col-sm-3 col-xs-3 active">
                                    <span class="num"><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <span class="txt">{{ $dictionary['personal_info'] }}</span>
                                </div>
                                <div class="col-sm-3 col-xs-3 active">
                                    <span class="num"><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <span class="txt">{{ $dictionary['product_delivery'] }}</span>
                                </div>
                                <div class="col-sm-3 col-xs-3 active">
                                    <span class="num"><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <span class="txt">{{ $dictionary['payment'] }}</span>
                                </div>
                                <div class="col-sm-3 col-xs-3 active">
                                    <span class="num"><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <span class="txt">{{ $dictionary['status'] }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="step_one">
                            <!-- StepSuccess Begin -->
                            <div class="step_success">
                                <i></i>
                                <h2>{{ $dictionary['order_complete_title'] }}</h2>
                                <p>{{ $dictionary['order_complete_text'] }}</p>
                                <a href="{{ route('home') }}">{{ $dictionary['back_home_page'] }}</a>
                            </div>
                            <!-- StepSuccess End -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

