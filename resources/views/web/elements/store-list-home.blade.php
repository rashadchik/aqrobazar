<div class="box">
    <a href="{{ route('store.show', $store->slug) }}" title="{{ $store->name }}">
        <figure>
            <img src="{{ $store->getFirstMedia('logo')->getUrl('md') }}" alt="{{ $store->name }}">
        </figure>
        <div class="body">
            <div class="body_inner">
                <h2>{{ $store->name }}</h2>
            </div>
        </div>
    </a>
</div>