@foreach($products as $productList)
    <div class="col-md-3 col-sm-4 col-xs-6 @if(isset($bookmark) && $bookmark == true) bookmark @endif">
        <div class="box">
            @include('web.elements.product-list', ['product' => $productList])
        </div>
    </div>
@endforeach