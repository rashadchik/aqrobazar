<div class="popup_inner" data-id="registr">
    <span class="btn_close"></span>
    <h1>{{ $dictionary['register'] ?? 'Qeydiyyat' }}</h1>
    <div class="lr_wrapper">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="ipt_group">
                <input type="text" name="name" placeholder="{{ $dictionary['full_name'] }}" class="ipt_style user" value="{{ old('name') }}">
            </div>
            <div class="ipt_group">
                <input type="text" name="email" placeholder="{{ $dictionary['email'] }}" class="ipt_style email" value="{{ old('email') }}">
            </div>
            <div class="ipt_group">
                <div class="radiobox clearfix">
                    <input type="radio" name="sex" id="male" class="css-checkbox" value="1">
                    <label for="male" class="css-label">{{ trans('locale.config.sex.1') }}</label>
                    <input type="radio" name="sex" id="female" class="css-checkbox" value="0">
                    <label for="female" class="css-label">{{ trans('locale.config.sex.0') }}</label>
                </div>
            </div>
            <div class="ipt_group">
                <input type="password" name="password" placeholder="{{ $dictionary['password'] }}" class="ipt_style password">
            </div>
            <div class="ipt_group">
                <input type="password" name="password_confirmation" placeholder="{{ $dictionary['repeat_password'] }}" class="ipt_style password">
            </div>
            <div class="bottom pt0 clearfix">
                <a href="#" class="pull-left open_lr mt10" data-type="login">{{ $dictionary['login'] }}</a>
                <button type="submit">{{ $dictionary['register_button'] }}</button>
            </div>
        </form>
    </div>
</div>