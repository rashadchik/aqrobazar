@if(array_key_exists($type, $banners) && env('APP_ENV') == 'production')

    <div class="bg_banner {{ $side }} hidden-xs">
        @if( getExtension($banners[$type]->getFirstMediaUrl()) == 'html' )
            <div class="bg_banner_inner">
                <iframe scrolling="no" frameborder="0" marginheight="0" marginwidth="0" src="{{ route('banner', $banners[$type]->id) }}"></iframe>
            </div>
        @else
            <div class="bg_banner_inner">
                <iframe scrolling="no" frameborder="0" marginheight="0" marginwidth="0" src="{{ asset($banners[$type]->getFirstMediaUrl()) }}"></iframe>
            </div>
        @endif

        @if(!is_null($banners[$type]->link))
            <a href="//{{ $banners[$type]->link }}" target="_blank"></a>
        @endif

    </div>

@endif