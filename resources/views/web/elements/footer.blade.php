<!-- Footer Begin -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {!! $footerMenu->render() !!}
            </div>
            <div class="col-xs-12">
                <p class="pull-left">© {{ date('Y') }} {{ $config['name'] }} | Bütün Hüquqlar Qorunur </p>
                <p class="pull-right">Bu bir <a href="https://marcom.az" target="_blank">Marcom</a> məhsuludur</p>
            </div>
        </div>
    </div>
</footer>
<a class="gotop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
<!-- Footer End -->
@if(!isset($hideCreateRoute))
<a href="{{ route('ads.create') }}" class="fixed_bottom_btn">ELAN YERLƏŞDİR</a>
@endif