<div class="subscribe">
    <h2>{{ $dictionary['subscribe'] }}</h2>
    {!! Form::open(['url'=>route('subscribe'), 'method'=>'POST', 'id' => 'subscribe-form', 'class' => 'form']) !!}
    {!! Form::text('email', null, ["placeholder" => '* '.$dictionary['email'], 'class' => 'ipt_style', 'required']) !!}

    {!! Form::button('<i class="fa fa-paper-plane" aria-hidden="true"></i>', ['id' => 'loadingSubButton', 'data-loading-text' => loading(), 'autocomplete' => 'off', 'type' => 'submit']) !!}
    {!! Form::close() !!}
</div>