@foreach(config('config.social-network') as $item)
    @if(isset($config[$item]) && trim($config[$item]) != '')
        <a href="//{{ $config[$item] }}" title="{{ $item }}" target="_blank" class="{{ $item }}"></a>
    @endif
@endforeach