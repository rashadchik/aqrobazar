<script type="text/javascript">
    $(function(){

        $("#form").validate({
            rules: {
                full_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 40
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true
                },
                text: {
                    required: true,
                    minlength: 20,
                    maxlength: 1000
                }
            },

            success: function(label) {
                label.html('').removeClass('error').addClass('ok');
            },
            errorPlacement: function(error, element) {
                {{--@if(!isset($withoutMsg))--}}
                {{--$(element).parents('.span').append(error);--}}
                {{--@endif--}}
            },
            highlight: function ( element, errorClass, validClass ) {
                $(element).addClass( "error-item" ).removeClass( "valid-item" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).addClass( "valid-item" ).removeClass( "error-item" );
            },
            submitHandler: function (form) {
                contactForm($(form));
                return false;
            }
        });

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

    });

</script>