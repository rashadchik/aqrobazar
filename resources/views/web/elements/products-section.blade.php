<div class="product_section @if(isset($noBorder)) nobrd @endif @if(isset($margin)) mt20 @endif">
    <!-- ProductHead -->
    <div class="product_head clearfix">
        @if(isset($productsCount))
            <h1 class="lh44">{{ $productListTitle }}</h1>
            <span class="count lh44">{{ $productsCount }} elan tapıldı</span>
        @else
            <h1>{{ $productListTitle }}</h1>
        @endif
        @if(isset($showAll))
            <a href="{{ $showAll }}" class="get_all">Hamısını göstər</a>
        @endif
    </div>
    <!-- ProductList -->
    <div class="product_list row" @if(isset($loadMore)) id="post" @endif>
        @forelse($productLists as $productList)
            @if(!isset($vipType) || $productList->vip_type == $vipType)
                <div class="col-md-3 col-sm-4 col-xs-6 @if(isset($bookmark) && $bookmark == true) bookmark @endif">
                    <div class="box">
                        @include('web.elements.product-list', ['product' => $productList])
                    </div>
                </div>
                @if($device == 'mobile' && $loop->iteration % 2 == 0)
                    <div class="row visible-xs"></div>
                @endif
            @endif
        @empty
            @if(isset($bookmark) && $bookmark == true)
                <h5>Sizin seçilmişləriniz yoxdur</h5>
            @endif
        @endforelse
    </div>

    <div class="clearfix"></div>
    @if(isset($loadMore))
        <div class="row ajax-loading text-center" style="display:none">
            <span class="fa fa-circle-o-notch fa-spin fa-3x"></span>
        </div>
    @endif
</div>