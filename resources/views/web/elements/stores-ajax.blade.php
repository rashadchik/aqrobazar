@foreach($stores as $store)
    <div class="col-sm-4 col-xs-6">
        <div class="box">
            @include('web.elements.store-list', ['store' => $store])
        </div>
    </div>
@endforeach