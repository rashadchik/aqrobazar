<a href="{{ \App\Logic\Menu::fullNestedSlug($product->parent_id, $product->page_slug, $product->slug) }}">

    @if(isset($activeEdit) && $activeEdit == true && $product->status != 3)
        <div class="hover">
            <div>
                <button type="button" class="open_lr warning-modal-open" data-type="delete_post_by_user" data-action="{{ route('ads.delete', $product->id) }}">
                    <i class="delete"></i> Elanı sil
                </button>
            </div>
            @if(array_key_exists($product->status, \App\Models\Product::editableStatus))
                <div>
                    <button type="button" onclick="event.preventDefault(); document.getElementById('edit-ads-{{ $product->id }}').click();"><i class="edit"></i>Düzəliş et</button>
                    <a href="{{ url('ads/'.$product->slug.'/edit') }}?pin_code={{ $product->pin_code }}" id="edit-ads-{{ $product->id }}" style="display: none"></a>
                </div>
            @endif
        </div>
    @endif
    <figure>
        @if($product->is_vip == 1)
            <span class="icon-star active"></span>
        @endif
        @if(env('APP_ENV') == 'production' && $product->getFirstMedia())
            <img src="{{ asset($product->getFirstMedia()->getUrl('md')) }}" alt="{{ $product->name }}">
        @endif
        <span class="price">{{ $product->price }} AZN</span>
    </figure>

    <div class="body">
        <h2 class="product_name">{{ $product->name }}</h2>
        <ul class="list-unstyled">
            <li>Xidmətin növü: {{ trans('locale.ad-type.'.$product->type) }}</li>
        </ul>
        <div class="bottom">
            <span class="date">{{ $product->city }} {{ explode(',', $product->created_at)[0] }}</span>
            <div class="pull-right">
                <img src="{{ asset('images/icon/flags/'.$product->country.'.svg') }}" class="flag">
                <button type="button" class="add_favorite icon-favorite @if($product->bookmarks_count > 0 || (isset($bookmark) && $bookmark == true)) active @endif" data-id="{{ $product->id  }}"></button>
            </div>
        </div>
    </div>
</a>

