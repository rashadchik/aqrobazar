<!-- Account Begin -->
<section class="account">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="profile">
                <figure>
                    <img src="{{ asset('images/profile.png') }}" alt="{{ $user->name }}">
                </figure>
                <span class="name">{{ $user->name }}</span>
            </div>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="account_menu">
                <a href="{{ route('user.profile') }}" @if(basename(request()->url()) == 'profile') class="active" @endif>Şəxsi məlumatlar</a>
                <a href="{{ route('user.stat') }}" @if(basename(request()->url()) != 'profile') class="active" @endif>Elanlar</a>
            </div>

            {{ $content }}
        </div>
    </div>
</section>
<!-- Account End -->
