<!-- MobileCategory Begin -->
<div class="mobile_category">
    <div class="mobile_category_head">
        <span class="close_panel"></span>
    </div>
    {!! $mobileMenu->render() !!}
</div>
<!-- MobileCategory End -->
<!-- Header Begin -->
<header id="header">
    <!-- TopBar -->
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 hidden-xs">
                    <!-- TopMenu -->
                    {!! $topMenu->render() !!}
                </div>
                <div class="col-sm-5 col-xs-12">
                    <div class="pull-right">
                        <!-- PhoneNumber -->
                        <a href="tel:{{ str_replace('-', '', $config['contact_phone']) }}" class="call_center">{{ $config['contact_phone'] }}</a>
                        <!-- Social -->
                        <nav class="social">
                            @include('web.elements.social')
                        </nav>

                        @if($config['hide_locale'] == 0)
                            <!-- Lang -->
                            <div class="lang">
                                @include('web.elements.lang', ['current' => false])
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HeaderInner -->
    <div class="header_inner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BtnMenu -->
                    <button type="button" class="btn_menu"></button>
                    <!-- Logo -->
                    <div class="logo">
                        <a href="{{ route('home') }}"><img src="{{ asset('images/logo.svg') }}?v=4" alt="{{ $config['name'] }} "></a>
                    </div>
                    <!-- HeaderMenu -->
                    <div class="header_menu">
                        <a href="{{ route('bookmarks.index') }}"><i class="icon icon-favorite"></i>Seçdikləriniz</a>
                        @if(auth()->guard('web')->check())
                            <a href="{{ route('user.stat') }}"><i class="icon icon-user"></i>Şəxsi Kabinet</a>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" ><i class="icon icon-exit"></i>{{ $dictionary['logout'] }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @else
                            <a href="#" class="open_lr" data-type="login"><i class="icon icon-user"></i>Giriş</a>
                        @endif
                        <a href="{{ route('ads.create') }}" class="btn_new">Elan yerləşdir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header End -->