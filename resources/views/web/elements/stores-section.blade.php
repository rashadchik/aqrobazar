<div class="store_list p0">
    <div class="row">
        <div class="col-xs-12">
            <div class="store_list_head clearfix">
                <h1>{{ \App\Models\Store::listTitle }}</h1>
                <div class="pull-right">
                    <button class="create_store_btn open_lr" data-type="create_store">Mağaza Yarat</button>
                    @if(isset($showAll))
                        <a href="{{ route('store.all') }}" class="get_all">Hamısını göstər</a>
                    @endif
                </div>
            </div>
        </div>

        <div @if(isset($loadMore)) id="post" @endif>
            @foreach($stores as $store)
                @if(isset($homePage))
                    <div class="col-sm-3 col-xs-6">
                        @include('web.elements.store-list-home', ['store' => $store])
                    </div>
                @else
                    <div class="col-sm-4 col-xs-6">
                        @include('web.elements.store-list', ['store' => $store])
                    </div>
                    @if(($device == 'mobile' && $loop->iteration % 2 == 0) || ($device == 'aqrobazar' && $loop->iteration % 3 == 0))
                        <div class="row"></div>
                    @endif
                @endif
            @endforeach
            <div class="col-xs-12 visible-xs">
                <button class="create_store_btn open_lr" data-type="create_store">Mağaza Yarat</button>
            </div>
        </div>

    </div>
</div>

@if(isset($loadMore))
    <div class="clearfix"></div>
    <div class="row ajax-loading text-center" style="display:none">
        <span class="fa fa-circle-o-notch fa-spin fa-3x"></span>
    </div>
@endif