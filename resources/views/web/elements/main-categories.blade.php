<!-- CategoryList Begin -->
<section class="category_list hidden-xs">
    <div class="row">
        @foreach($mainCategories as $m)
            @if(is_null($m->parent_id))
                <div class="col-md-2 col-sm-3 col-xs-4 col-mob-6">
                    <div class="box">
                        <a href="#" title="{{ $m->name }}">
                            <figure>
                                @if($m->getFirstMediaUrl('icon') != '' && env('APP_ENV') == 'production')
                                    <img src="{{ $m->getFirstMediaUrl('icon') }}" alt="{{ $m->name }}" class="svg">
                                @endif
                            </figure>
                            <h2>{{ $m->name }}</h2>
                        </a>
                        <div class="subblock @if(!in_array($m->id, \App\Models\Page::subCategories) ) p20 @endif">
                            @if(in_array($m->id, \App\Models\Page::subCategories) )
                                <div class="subblock_head">
                                    @php $num = 0; @endphp
                                    @foreach($mainCategories as $child)
                                        @if($child->parent_id == $m->id)
                                            @php $num++; @endphp
                                            <a href="#" title="{{ $child->name }}" @if($num == 1) class="active" @endif>{{ $child->name }}</a>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="inner">
                                    @foreach($mainCategories as $tabBlock)
                                        @if($tabBlock->parent_id == $m->id)
                                            <div class="tabblock">
                                                <ul class="list-unstyled">
                                                    <li><a href="{{ route('showPage', $tabBlock->slug) }}">Hamısı <span> {{ $productCategoriesTotal[$tabBlock->id] }}</span></a></li>
                                                    @php $subCategory = false; @endphp
                                                    @foreach($mainCategories as $tabBlockChild)
                                                        @if($tabBlockChild->parent_id == $tabBlock->id)
                                                            @php $subCategory = true; @endphp
                                                            <li><a href="{{ route('showPage', [$m->slug, $tabBlockChild->slug]) }}" title="{{ $tabBlockChild->name }}">{{ $tabBlockChild->name }} <span>{{ $tabBlockChild->products_count }}</span></a></li>
                                                        @endif
                                                    @endforeach
                                                    @if($subCategory)
                                                        <li><a href="{{ route('showPage', [$tabBlock->slug, 'other']) }}">Digər <span>{{ $tabBlock->products_count }}</span></a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @else
                                <ul class="list-unstyled">
                                    <li><a href="{{ route('showPage', $m->slug) }}">Hamısı <span>{{ $productCategoriesTotal[$m->id] }}</span></a></li>
                                    @foreach($mainCategories as $child)
                                        @if($child->parent_id == $m->id)
                                            <li><a href="{{ route('showPage', [$m->slug, $child->slug]) }}" title="{{ $child->name }}">{{ $child->name }} <span>{{ $child->products_count }}</span></a></li>
                                        @endif
                                    @endforeach
                                    <li><a href="{{ route('showPage', [$m->slug, 'other']) }}">Digər <span>{{ $m->products_count }}</span></a></li>
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</section>
<!-- CategoryList End -->
