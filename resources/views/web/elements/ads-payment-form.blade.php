<span class="btn_close" data-dismiss="modal" aria-hidden="true"></span>
<h1 class="mb25">{{ $title }}</h1>
<div class="lr_wrapper">

    @if($type == 1 && $product->is_fav == 1 )
        <h5 class="text-center help-block">Xidmət artıq aktivdir.</h5>
    @elseif($type == 2 && $product->is_vip == 1 )
        <h5 class="text-center help-block">Xidmət artıq aktivdir.</h5>
    @else
        <form method="POST" action="{{ url('/pay') }}">
            @csrf
            <div class="ipt_group">
                <p class="clr mb25">
                    Siz <a href="{{ route('ads.show', $product->id) }}" title="{{ $product->name }}" target="_blank">{{ $product->name }}</a> elanı üçün ödəniş edirsiniz.
                </p>
            </div>
            <div class="ipt_group">
                <div class="radiobox dblock clearfix">
                    <input type="radio" name="card" id="bank" class="css-checkbox" value="1" checked>
                    <label for="bank" class="css-label">Bank kartı</label>
                    @if($type == 1)
                        <input type="radio" name="card" id="operator" class="css-checkbox" value="2">
                        <label for="operator" class="css-label">Bakcell / Nar - balansdan ödəniş</label>
                    @endif
                </div>
                <div class="radiobox dblock mt15 clearfix">
                    @foreach($price as $p)
                        <input type="radio" name="amount" id="amount-{{ $p->amount }}" value="{{ $p->amount }}" class="css-checkbox" @if($loop->iteration == 1) checked @endif>
                        <label for="amount-{{ $p->amount }}" class="css-label">{{ $p->amount }} AZN / {{ $p->note }}</label>
                    @endforeach
                </div>
            </div>
            <div class="clearfix">
                <input type="hidden" name="product" value="{{ $product->id }}">
                <input type="hidden" name="type" value="{{ $type }}">
                <button type="submit" class="center">Davam et</button>
            </div>
        </form>
    @endif

</div>