<section class="search" @if(isset($padding)) style="padding-top:0 !important;" @endif>
    <div class="row">
        <div class="col-xs-12">
            <form action="{{ route('search') }}" method="GET">
                <input type="text" name="keyword" placeholder="{{ $dictionary['search_placeholder'] ?? 'Axtarmaq istədiyiniz məhsulu yazın...' }}" autocomplete="off" value="{{ request()->get('keyword') }}">
                <div class="price">
                    <span>Qiymət:</span>
                    <input type="text" class="price-input" name="min_price" placeholder="min" value="{{ request()->get('min_price') }}">
                    <em class="line"></em>
                    <input type="text" class="price-input" name="max_price" placeholder="max" value="{{ request()->get('max_price') }}">
                </div>

                <select name="service_type" style="margin-right:10px">
                    <option disabled @if(!request()->has('service_type')) selected @endif>Xidməti seçin</option>
                    <option value="">İstənilən</option>
                    @foreach(trans('locale.ad-type') as $key => $serviceType)
                        <option value="{{ $key }}" @if(request()->get('service_type') == $key) selected @endif>{{ $serviceType }}</option>
                    @endforeach
                </select>

                <select name="city">
                    <option disabled @if(!request()->has('city')) selected @endif>Bölgəni seçin</option>
                    <option value="">Bütün bölgələr</option>
                    @foreach($countries as $country)
                        <optgroup label="{{ $country->tr->name }}">
                            @foreach($country->cities as $city)
                                <option value="{{ $city->id }}" @if(request()->get('city') == $city->id) selected @endif>{{ $city->name }}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>

                <button type="submit">{{ $dictionary['search'] ?? 'Axtar' }}</button>
            </form>
        </div>
    </div>
</section>