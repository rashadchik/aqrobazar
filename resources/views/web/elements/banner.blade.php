@if(array_key_exists($type, $banners) && env('APP_ENV') == 'production')

    @if( getExtension($banners[$type]->getFirstMediaUrl()) == 'html' )
        <iframe scrolling="no" class="iframe" frameborder="0" marginheight="0" marginwidth="0" src="{{ route('banner', $banners[$type]->id) }}" width="{{ $banners[$type]->width }}" height="{{ $banners[$type]->height }}"></iframe>
    @else
        <a href="@if(!is_null($banners[$type]->link))//{{ $banners[$type]->link }}@else javascript:void(0) @endif" @if(!is_null($banners[$type]->link)) target="_blank" @endif style="display: block">
            <img src="{{ asset( $banners[$type]->getFirstMediaUrl()) }}" class="center-block img-responsive">
        </a>
    @endif

@endif