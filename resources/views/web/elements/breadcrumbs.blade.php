<!-- Breadcrumbs Begin -->
<section class="breadcrumbs clearfix">
    @if(isset($page))
        {!! Breadcrumbs::render('breadcrumb', $page, $dictionary['home_page'], isset($data) ? $data : null) !!}
    @elseif(isset($breadcrumbs))

        <a href="{{ route('home') }}">{{ $dictionary['home_page'] }}</a>
        <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>

        @foreach($breadcrumbs as $title => $url)
            @if(!$loop->last)
                <a href="{{ url($url) }}">{{ $title }}</a>
                <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
            @else
                <span>{{ $title }}</span>
            @endif
        @endforeach
    @endif
</section>
<!-- Breadcrumbs End -->

