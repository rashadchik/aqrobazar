@foreach(config("app.locales") as $key => $locale)
    @if($current == true || $key != $lang)
        <a rel="alternate" hreflang="{{ $key }}" href="{{ url($relatedPages[$key] ?? $key) }}" class="lang text-uppercase">{{ $key }}</a>
    @endif
@endforeach