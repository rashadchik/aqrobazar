<!-- Popup Begin -->
<div class="popup">
    @include('web.elements.popup-register')
    @include('web.elements.popup-login')

    <div class="popup_inner" data-id="forgot_password">
        <span class="btn_close"></span>
        <h1>{{ trans('locale.forgot_password') }}</h1>
        <div class="lr_wrapper">
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="ipt_group">
                    <input type="email" name="email" placeholder="{{ $dictionary['email'] }}" class="ipt_style email" required>
                </div>
                <div class="clearfix">
                    <button type="submit" name="">Göndər</button>
                </div>
                <div class="bottom clearfix">
                    <a href="#" title="" class="pull-left open_lr" data-type="forgot_password">Şifrəni unutmusunuz?</a>
                    <a href="#" title="" class="pull-right open_lr" data-type="registr">Qeydiyyat</a>
                </div>
            </form>
        </div>
    </div>

    <div class="popup_inner" data-id="edit_post">
        <span class="btn_close"></span>
        <h1>Elanın düzəlişi</h1>
        <div class="lr_wrapper">
            {!! Form::open(['method'=> 'POST', 'class' => 'warning-modal']) !!}
                @csrf
                <div class="ipt_group">
                    <input type="text" name="pin_code" placeholder="Elanın PIN kodunu daxil edin" class="ipt_style">
                </div>
                <div class="clearfix">
                    <button type="submit" class="center">Düzəliş et</button>
                </div>
                <div class="bottom clearfix">
                    <span class="get_link"><a href="#" class="pull-left open_lr" data-type="forgot_pinkod">PIN kodu unutmusunuz?</a></span>
                </div>
                <input type="hidden" name="type" value="1">
            {!! Form::close() !!}
        </div>
    </div>
    <div class="popup_inner" data-id="delete_post">
        <span class="btn_close"></span>
        <h1>Elanın silinməsi</h1>
        <div class="lr_wrapper">
            {!! Form::open(['method'=> 'POST', 'class' => 'warning-modal']) !!}
                @csrf
                <div class="ipt_group">
                    <input type="text" name="pin_code" placeholder="Elanın PIN kodunu daxil edin" class="ipt_style">
                </div>
                <div class="clearfix">
                    <button type="submit" class="center">Elanı sil</button>
                </div>
                <div class="bottom clearfix">
                    <span class="get_link"><a href="#" class="pull-left open_lr" data-type="forgot_pinkod">PIN kodu unutmusunuz?</a></span>
                </div>
                <input type="hidden" name="type" value="2">
            {!! Form::close() !!}
        </div>
    </div>
    <div class="popup_inner" data-id="forgot_pinkod">
        <span class="btn_close"></span>
        <h1>PIN-kodu unutmusunuz?</h1>
        <div class="lr_wrapper">
            <form>
                <div class="ipt_group">
                    <p class="clr mb25">PIN-kodun sizə göndərilməsi üçün istədiyiniz üsulu seçin.</p>
                </div>
                <div class="ipt_group">
                    <div class="radiobox pb10 clearfix">
                        <input type="radio" name="forgot_pinkod" id="SMS ilə" class="css-checkbox">
                        <label for="SMS ilə" class="css-label">SMS ilə</label>
                        <input type="radio" name="forgot_pinkod" id="E-poçt ilə" class="css-checkbox">
                        <label for="E-poçt ilə" class="css-label">E-poçt ilə</label>
                    </div>
                </div>
                <div class="clearfix">
                    <button type="submit" class="center" name="">Davam et</button>
                </div>
            </form>
        </div>
    </div>
    <div class="popup_inner" data-id="create_store">
        <span class="btn_close"></span>
        <h1>Mağaza yarat</h1>
        <div class="lr_wrapper">
            <form class="submit-form" action="{{ route('store.apply') }}" method="POST">
                @csrf
                <div class="ipt_group">
                    <input type="text" name="full_name" placeholder="Ad, Soyad" class="ipt_style user" required>
                </div>
                <div class="ipt_group">
                    <input type="text" name="phone" class="ipt_style phone int-nat-phone" required>
                    <span id="output" style="color:red"></span>
                </div>
                <div class="ipt_group">
                    <input type="email" name="email" placeholder="E-poçt daxil edin" class="ipt_style email" required>
                </div>
                <div class="clearfix">
                    {!! Form::button('Təsdiq et', ['class' => 'loadingButton center', 'data-loading-text' => loading(), 'type' => 'submit']) !!}
                </div>
            </form>
        </div>
    </div>
    <div class="popup_inner" data-id="complain">
        <span class="btn_close"></span>
        <h1 class="mb25">Elanı şikayət et</h1>
        <div class="lr_wrapper">
            <form>
                <div class="ipt_group">
                    <select class="ipt_style">
                        <option value="">Artıq satılıb</option>
                    </select>
                </div>
                <div class="ipt_group">
                    <textarea name="" placeholder="" class="ipt_style"></textarea>
                </div>
                <div class="clearfix">
                    <button type="submit" class="center" name="">Göndər</button>
                </div>
            </form>
        </div>
    </div>
    <div class="popup_inner" data-id="yatirim">
        <span class="btn_close"></span>
        <h1>YATIRIM ET</h1>
        <div class="lr_wrapper">
            <form>
                <div class="ipt_group">
                    <input type="text" name="" placeholder="Ad Soyad daxil edin" class="ipt_style">
                    <i class="fa fa-user fa-fw" aria-hidden="true"></i>
                </div>
                <div class="ipt_group">
                    <input type="text" name="" placeholder="Əlaqə nömrəsi daxil edin" class="ipt_style">
                    <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                </div>
                <div class="clearfix">
                    <button type="submit" class="center" name="">Göndər</button>
                </div>
            </form>
        </div>
    </div>
    @if(auth()->guard('web')->check())
        <div class="popup_inner" data-id="delete_post_by_user">
            <span class="btn_close"></span>
            <h1>Elanı silmək istədiyinizə əminsinizmi?</h1>
            <div class="lr_wrapper">
                {!! Form::open(['method'=> 'PUT', 'class' => 'warning-modal']) !!}
                    @csrf
                    <div class="clearfix">
                        <button type="submit" class="center">Elanı sil</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    @endif

    <div class="popup_inner no-padding" data-id="success">
        <div class="success_wrapper">
            <span class="btn_close"></span>
            <i></i>
            <h4>Məlumatınız təsdiq üçün göndərildi. <br>Təşəkkür edirik.</h4>
        </div>
    </div>
</div>
<!-- Popup End -->