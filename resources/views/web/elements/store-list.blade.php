<div class="box">
    <a href="{{ route('store.show', $store->slug) }}" title="{{ $store->name }}">
        <figure>
            <img src="{{ $store->getFirstMedia('logo')->getUrl('md') }}" alt="{{ $store->name }}">
        </figure>
        <div class="body">
            <div class="body_inner_inside">
                <h2>{{ $store->name }}</h2>
                <p>{{ str_limit($store->summary, 108) }}</p>
                <span class="phone"><i class="fa fa-phone" aria-hidden="true"></i>{{ $store->phone }}</span>
                <span class="count">{{ $store->get_product_count_count }} elan</span>
            </div>
        </div>
    </a>
</div>