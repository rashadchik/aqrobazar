<div class="popup_inner" data-id="login">
    <span class="btn_close"></span>
    <h1>{{ $dictionary['login'] }}</h1>
    <div class="lr_wrapper">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="ipt_group">
                <input type="text" name="email" placeholder="{{ $dictionary['email'] }}" class="ipt_style email" value="{{ old('email') }}">
            </div>
            <div class="ipt_group">
                <input type="password" name="password" placeholder="{{ $dictionary['password'] }}" class="ipt_style password">
            </div>

            <div class="clearfix">
                <div class="rememberme checkbox">
                    <input type="checkbox" name="remember" id="rememberme" class="css-checkbox">
                    <label for="rememberme" class="css-label">{{ $dictionary['remember_me'] }}</label>
                </div>
                <button type="submit">{{ $dictionary['login'] }}</button>
            </div>

            <div class="bottom clearfix">
                <a href="#" class="pull-left open_lr" data-type="forgot_password">{{ trans('locale.forgot_password') }}</a>
                <a href="#" class="pull-right open_lr" data-type="registr">{{ $dictionary['register'] ?? 'Qeydiyyat' }}</a>
            </div>
        </form>
    </div>
</div>