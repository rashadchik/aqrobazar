<!-- Products Begin -->
<section class="products @if(isset($padding)) {{ $padding }} @endif">
    <div class="row">
        <div class="col-xs-12">
            @include('web.elements.products-section')
        </div>
    </div>
</section>
<!-- Products End -->