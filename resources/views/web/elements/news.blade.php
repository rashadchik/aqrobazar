<!-- MainNews Begin -->
<section class="main_news">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="title_block">
                    <h1>{{ $dictionary['last_news'] ?? 'SON XƏBƏRLƏR' }}</h1>
                    <a href="{{ route('showPage' , $newsPage->slug) }}" class="all">{{ $dictionary['all_news'] ?? 'Bütün xəbərlər' }}</a>
                </div>
            </div>
        </div>
        <!-- NewsList Begin -->
        <div class="row">
            <div class="col-xs-12">
                <div id="mainnews" class="owl-carousel">
                    @foreach($newsPage->articles(3) as $article)
                        <article>
                            <figure>
                                <img src="{{ asset('storage/'.$article->image) }}" alt="{{ $article->name }}">
                            </figure>
                            <div class="lb">
                                <span class="date">{{ blogDate($article->published_at) }}</span>
                                <h2>{{ $article->name }}</h2>
                                <p>{{ str_limit( $article->summary , 200 ) }} </p>
                                <a href="{{ route('showPage', [$newsPage->slug, $article->slug]) }}" class="more text-uppercase">{{ $dictionary['read_more'] }}</a>
                            </div>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- NewsList End -->
    </div>
</section>
<!-- MainNews End -->