<script>
    var photo_counter = 0;
    Dropzone.options.realDropzone = {
        maxFiles: '{{ $maxFile }}',
        uploadMultiple: false,
        parallelUploads: 3,
        maxFilesize: 10,
        previewsContainer: '#dropzonePreview',
        addRemoveLinks: true,
        dictRemoveFile: 'Sil',
        dictFileTooBig: 'Image is bigger than 10 MB',

        // The setting up of the dropzone
        init:function() {

            this.on("removedfile", function(file) {
                var url = file.deleteRoute;

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {_token: '{{csrf_token()}}', _method: 'DELETE', dropzone: true},
                    dataType: 'html',
                    success: function(data){

                        if(file.code === 200)
                        {
                            photo_counter--;
                            $("#photoCounter").text( "(" + photo_counter + ")");
                            $('#galleries').DataTable().draw( false );
                        }
                        else{
                            toastr.error(file.msg);
                        }
                    }
                });

            } );

            this.on("queuecomplete", function () {
                $('#galleries').DataTable().draw( false );
            });
        },
        error: function(file, response) {
            if(response.code == 400){
                toastr.error(response.message);
            }
            else{
                if(file.status == 'error'){
                    toastr.error(response);
                }
            }


            if($.type(response) === "string")
                var message = response; //dropzone sends it's own error messages in string
            else
                var message = response.message;
            file.previewElement.classList.add("dz-error");
            _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        },
        success: function(file,response) {
            file.code = response.code;
            file.deleteRoute = response.deleteRoute;
            file.msg = response.message;
            photo_counter++;
            //$("#photoCounter").text( "(" + photo_counter + ")");
        }
    }


</script>
