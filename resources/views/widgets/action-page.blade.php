@if(is_null($row->deleted_at))
    @if(isset($show) && $show == true)
        <a href="{{ $show }}" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-eye"></i> </a>
    @endif

    @if($route == 'product')
        <div class="input-group">
            <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>&nbsp;<span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu">
                <li><a href="{{ url($row->lang.'/'.$row->page_slug.'/'.$row->slug) }}" target="_blank">Bax </a></li>
                <li><a href="{{ route('gallery.index', [$route, $row->tid]) }}" class="btn btn-xs btn-success" target="_blank">Qalereya </a></li>
            </ul>
        </div>
    @endif

    <a href="#" class="btn btn-xs btn-primary open-modal-dialog" data-link="{{ route("$route.edit", $row->tid) }}" data-large="{{ $largeModal ?? false }}"><i class="fa fa-edit"></i> </a>

    @if(isset($album) && $album == true)
        <a href="{{ route('gallery.index', [$route, $row->tid]) }}" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-image"></i> </a>
    @endif

    <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault(); datatableAction(document.getElementById('soft-delete-{{ $row->tid }}'));">
        <i class="fa fa-trash"></i>
        {!! Form::open(['method' => 'PUT', 'url' => route("$route.trash", $row->tid), 'id' => "soft-delete-{$row->tid}", 'style' => 'display:none', 'class' => 'dtForm']) !!}
        {!! Form::close() !!}
    </a>
@else
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); datatableAction(document.getElementById('restore-delete-{{ $row->tid }}'));">
        <i class="fa fa-repeat"></i>
        {!! Form::open(['method' => 'PUT', 'url' => route("$route.restore", $row->tid), 'id' => "restore-delete-{$row->tid}", 'style' => 'display:none']) !!}
        {!! Form::close() !!}
    </a>

    @if(isset($forceDelete) && $forceDelete == true)
        <a href="#" class="btn btn-xs btn-danger" data-action="{{route("$route.destroy", $row->tid)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
    @endif
@endif