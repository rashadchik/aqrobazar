@if(is_null($row->deleted_at))

    <div class="dropdown pull-right">
        <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="actions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="glyphicon glyphicon-cog"></span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="actions">
            <li><a href="{{ $show }}" target="_blank"><i class="fa fa-eye"></i> Bax</a></li>
            <li><a href="{{ route('gallery.index', [$route, $row->id]) }}" target="_blank"><i class="fa fa-image"></i> Qalereya</a></li>
            <li><a href="#" class="open-modal-dialog" data-link="{{ route("$route.edit", $row->id) }}" data-large="{{ $largeModal ?? false }}"><i class="fa fa-edit"></i> Düzəliş et</a></li>
            @if($row->status == 1)
                @if($row->is_vip == 0)
                    <li><a href="#" class="open-modal-dialog" data-link="{{ route("vip.create", $row->id) }}"><i class="fa fa-diamond"></i> VIP et</a></li>
                @else
                    <li><a href="#" data-action="{{ route("vip.destroy", $row->id) }}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-diamond"></i> STOP VIP</a></li>
                @endif
                @if($row->is_fav == 0)
                    <li><a href="#" class="open-modal-dialog" data-link="{{ route("fav.create", $row->id) }}"><i class="fa fa-arrow-up"></i> İrəli çək</a></li>
                @else
                    <li><a href="#" data-action="{{ route("fav.destroy", $row->id) }}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-arrow-up"></i> STOP irəli</a></li>
                @endif
            @endif
        </ul>
    </div>

    @if(isset($softDelete) && $softDelete == true)
        <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault(); datatableAction(document.getElementById('soft-delete-{{ $row->id }}'));">
            <i class="fa fa-trash"></i>
            {!! Form::open(['method' => 'PUT', 'url' => route("$route.trash", $row->id), 'id' => "soft-delete-{$row->id}", 'style' => 'display:none', 'class' => 'dtForm']) !!}
            {!! Form::close() !!}
        </a>
    @elseif(isset($forceDelete) && $forceDelete == true)
        <a href="#" class="btn btn-xs btn-danger" data-action="{{ route("$route.destroy", $row->id) }}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
    @endif
@else
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); datatableAction(document.getElementById('restore-delete-{{ $row->id }}'));">
        <i class="fa fa-repeat"></i>
        {!! Form::open(['method' => 'PUT', 'url' => route("$route.restore", $row->id), 'id' => "restore-delete-{$row->id}", 'style' => 'display:none']) !!}
        {!! Form::close() !!}
    </a>

    @if(isset($forceDelete) && $forceDelete == true)
        <a href="#" class="btn btn-xs btn-danger" data-action="{{ route("$route.destroy", $row->id) }}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
    @endif
@endif