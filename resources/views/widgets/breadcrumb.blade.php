@if ($breadcrumbs)
    @foreach ($breadcrumbs as $breadcrumb)
        @if (!$breadcrumb->last)
            <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
            <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
        @else
            <span>{{ $breadcrumb->title }}</span>
        @endif
    @endforeach
@endif