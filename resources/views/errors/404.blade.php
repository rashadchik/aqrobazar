@extends ('layouts.web', [
'page_heading' => Cache::get('dictionary_'.app()->getLocale())['nothing_found_title'],
'mobileMenu' => $mobileMenu ?? \App\Logic\Menu::mobileMenu(),
'topMenu' => $topMenu ?? \App\Logic\Menu::topMenu(),
'footerMenu' => $footerMenu ?? \App\Logic\Menu::footerMenu(),
'config' => getConfig(),
'dictionary' => Cache::get('dictionary_'.app()->getLocale()),
'social' => Cache::get('social'),
'lang' => app()->getLocale(),
'notFound' => true
])

@section ('content')
	<!-- Error404 Begin -->
	<section class="error404">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<span>404</span>
					<p>{{ $dictionary['nothing_found_heading'] ?? Cache::get('dictionary_'.app()->getLocale())['nothing_found_heading'] }}</p>
					<a href="{{ route('home') }}">{{ $dictionary['back_home_page'] ?? Cache::get('dictionary_'.app()->getLocale())['back_home_page'] }}</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Error404 End -->
@endsection