<!DOCTYPE html>
<html lang="{{$lang}}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height">
    <meta name="theme-color" content="#ffffff">
    <meta property="author" content="Aqrobazar.com">
    <meta property="fb:app_id" content="671030390001885" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $page_heading }}">
    <meta property="og:description" content="{{ $page->meta_description ?? $page_heading}}">
    <meta property="og:url" content="{{ $fb_link ?? request()->url() }}">
    <meta property="og:image" content="{{isset($page_image) ? secureAsset($page_image):  secureAsset('images/fb-image.png')}}">
    @if(isset($page))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
    @endif
    <title>@if(!is_null($page_heading)){{ $page_heading }} | @endif{{ $config['name'] }}</title>

    <link rel="canonical" href="{{ str_replace('m.', '', request()->url()) }}" />
    @if($device != 'mobile' && env('APP_ENV') == 'production')
        <link rel="alternate" media="only screen and (max-width: 768px)" href="{{ str_replace('https://', 'https://m.', request()->url()) }}">
    @endif

    <link rel="shortcut icon" href="{{ secureAsset('images/favicon/favicon.ico') }}">
    <link href="{{ secureAsset(mix('css/style.css')) }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @include('widgets.analytic', ['code' => $config['google_analytic_key'] ])
</head>
<body>

    <script type="text/javascript">
        var hostname = window.location.origin;
        var isMobile = false; //initiate as false
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
            isMobile = true;
        }

        if (hostname.indexOf("m.") >= 0 && isMobile === false) {
            var domain = window.location.href;
            domain = domain.replace("m.", "");
            window.location.replace(domain);
        }
    </script>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/{{$lang}}_{{strtoupper($lang)}}/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    

    @include('web.elements.popup-form')

    @if(isset($banners))

        @if($device != 'mobile')

            <div class="topbanner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            @include('web.elements.banner', ['type' => 1])
                        </div>
                        <div class="col-md-6">
                            @include('web.elements.banner', ['type' => 2])
                        </div>
                    </div>
                </div>
            </div>

            @if (isset($banners[6]))
                @include('web.elements.side-banner', ['side' => 'left', 'type' => 6])
                @include('web.elements.side-banner', ['side' => 'right same', 'type' => 6])
            @else
                @include('web.elements.side-banner', ['side' => 'left', 'type' => 4])
                @include('web.elements.side-banner', ['side' => 'right','type' => 5])
            @endif

        @else
            @include('web.elements.banner', ['type' => 1])
        @endif
    @endif

    @include('web.elements.header')

    <div id="main_wrapper">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>

    @include('web.elements.footer')

    <script src="{{ secureAsset(mix('js/script.js')) }}"></script>
    <script src="https://intl-tel-input.com/node_modules/intl-tel-input/build/js/intlTelInput.min.js"></script>

    @stack('scripts')

    <script>

        var input = document.querySelector(".int-nat-phone"),
            output = document.querySelector("#output");

        var iti = window.intlTelInput(input, {
            nationalMode: true,
            onlyCountries: {!! $countryCodes !!},
            utilsScript: "../js/utils.js"
        });

        var handleChange = function() {
            var text = (iti.isValidNumber()) ? iti.getNumber() : "Yanlış nömrə";
            var textNode = document.createTextNode(text);
            output.innerHTML = "";
            output.appendChild(textNode);

            if(iti.isValidNumber()) {
                $(".int-nat-phone").val(text);
                output.innerHTML = "";
            }
        };

        // listen to "keyup", but also "change" to update when the user selects a country
        input.addEventListener('change', handleChange);
        input.addEventListener('keyup', handleChange);

    </script>

    @if($errors->any())
        <script>
            $(document).ready( function() {
                toastr.error('{{ $errors->first() }}');
            });

        </script>
    @endif

    @if(session()->has('success'))
        <script>
            toastr.success('{{ session()->get('success') }}');
        </script>
    @endif

</body>
</html>
