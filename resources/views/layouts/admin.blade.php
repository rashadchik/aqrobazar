<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('admin.components.head')
 </head>

<body class="skin-blue sidebar-mini">

    @include('widgets.modal-ajax')
    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="{{ route('admin.dashboard') }}" class="logo">
            <span class="logo-mini"><b>AB</b></span>
            <span class="logo-lg"><b>AQROBAZAR</b></span>
        </a>
        @include('admin.components.header', ['profileRoute' => 'admin.profile'])
    </header>
    <aside class="main-sidebar">
        @include('admin.components.sidebar')
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('admin.components.content-header', ['homeRoute' => 'admin.dashboard'])

        <section class="content">
            @if(isset($errors) && $errors->any())
                @include('widgets.alert', array('class' => 'error', 'message' => $errors->first(), 'dismissable' => true))
            @elseif(request()->session()->has('success'))
                @include('widgets.alert', array('class' => 'success', 'message' => session()->get('success'), 'dismissable' => true))
            @endif
            @yield('content')
        </section>
    </div>
    <!-- /.content-wrapper -->

    @include('admin.components.footer')
    {{--@include('admin.components.control-sidebar')--}}

    <script src="{{ asset(mix('js/app.js')) }}"></script>
    <script src="{{ asset('js/admin/adminlte.min.js') }}"></script>
    {{--<script src="{{ asset('js/admin/demo.js') }}"></script>--}}
    <script src="{{ asset('js/admin/config.js') }}?v=2"></script>

    @stack('scripts')
</body>
</html>
