<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('admin.components.head')
    <link href="{{ asset('css/admin/blue.css') }}" rel="stylesheet">
</head>

<body class="hold-transition login-page">
    @yield('content')

    <script src="{{ asset(mix('js/app.js')) }}"></script>
    <script src="{{ asset('js/admin/icheck.js') }}"></script>

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>

</body>
</html>
