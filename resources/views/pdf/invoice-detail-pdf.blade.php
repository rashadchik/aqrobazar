<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 20px;
            margin: 10px 25px;
        }

        .page-header {
            margin: 10px 0 20px 0;
            font-size: 22px;
        }
        .page-header > small {
            color: #666;
            display: block;
            margin-top: 5px;
            font-size: 16px;
        }
        .invoice {
            width: 100%;
            border: 0;
            margin: 0;
            padding: 0;
        }
        .invoice-col {
            float: left;
            width: 33.3333333%;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            border-top: 1px solid #f4f4f4;
        }
        .table > thead > tr > th {
            border-bottom: 2px solid #f4f4f4;
        }
        /* .text-center in tables */
        table.text-center,
        table.text-center td,
        table.text-center th {
            text-align: center;
        }
        .table.align th {
            text-align: left;
        }
        .table.align td {
            text-align: right;
        }

    </style>
</head>

<body class="skin-blue sidebar-mini" style="font-family:'dejavu sans';">

    <section class="content">
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img src="{{ asset('images/logo.png') }}">
                        <small class="pull-right">Tarix: {{ filterDate($invoice->created_at, true, 'eFull') }}</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <!-- /.col -->
                <div class="col-sm-8 invoice-col">
                    Müştəri
                    <address>
                        <strong>{{ $invoice->customer->name }}</strong><br>
                        {{ $invoice->address }}<br>
                        Mobil: {{ $invoice->customer->phone }}<br>
                        Email: {{ $invoice->customer->email }}
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>{{ $title }}</b><br>
                    @if(!is_null($invoice->payment_key))
                        <b>Transaction ID:</b> {{ $invoice->payment_key }}<br>
                    @elseif(!is_null($invoice->postTerminal))
                        <b>Post Terminal:</b> {{ $invoice->postTerminal->name }}<br>
                    @endif
                    <b>Məbləğ:</b> {{ $invoice->amount }} AZN<br>
                    <b>Sifariş növü:</b> {{ trans("locale.config.payment-method.{$invoice->payment_type}") }}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Say</th>
                            <th>Məhsul</th>
                            <th>Kod #</th>
                            <th>qiymət</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoice->invoices as $product)
                            <tr>
                                <td>{{ $product->count }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->bar_code }}</td>
                                <td>{{ $product->price }} AZN</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-12">
                    <p class="lead">Əlavə qeyd:</p>
                    {{ $invoice->desc }}
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>
    </section>

</body>
</html>
