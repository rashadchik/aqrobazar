@extends ('layouts.web', ['page_heading' => $title])

@section ('content')

    @include('web.elements.breadcrumbs', ['page' => null, 'data' => ['title' => $title] ])

        <section class="addpost">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-xs-12">
                        <div class="form">
                            <form method="post" action="{{ url('/password/reset') }}" class="form-horizontal" role="form">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="form-group @if($errors->has('email')) has-error @endif">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-label" for="email">{{ $dictionary['email'] }}:</label>
                                    </div>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="email" class="form-control" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if($errors->has('password')) has-error @endif">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-label" for="password">{{ $dictionary['password'] }}:</label>
                                    </div>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="password" name="password" class="form-control" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if($errors->has('password_confirmation')) has-error @endif">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-label" for="password_confirmation">{{ $dictionary['repeat_password'] }}:</label>
                                    </div>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="password" name="password_confirmation" class="form-control" required>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 col-xs-offset-4">
                                        <button type="submit" class="btn btn-primary btn-block">Şifrəni dəyiş</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>

@endsection