@component('mail::message')

    #{{ $content['full_name'] }}

    - Email: {{ $content['email'] }}
    - Telefon: {{ $content['phone'] }}

@endcomponent