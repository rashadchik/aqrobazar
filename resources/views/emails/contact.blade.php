@component('mail::message')

    @if(isset($content['text']))
        #{{$content['text']}}
    @endif

    İstifadəçinin məlumatları:

    - Ad, soyad: {{$content['full_name']}}

    @if(isset($content['email']))
        - Email: {{$content['email']}}
    @endif

    @if(isset($content['phone']))
        - Telefon: {{$content['phone']}}
    @endif

    @if(isset($content['subject']))
        - Mövzu: {{$content['subject']}}
    @endif

@endcomponent