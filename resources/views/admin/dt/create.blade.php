@extends('layouts.modal', ['route' => $route, 'editor' => $editor, 'script' => $script ?? true])
@section('title', $title)

@section('content')
    {!! $fields !!}
@endsection