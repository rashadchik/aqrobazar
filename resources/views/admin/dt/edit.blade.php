@extends('layouts.modal', ['route' => $route, 'method' => 'PUT', 'script' => $script ?? true, 'editor' => $editor ?? false])
@section('title', $data->name ?? $title)

@section('content')
    {!! $fields !!}
@endsection

@push('scripts')
    <script>
        $('.category_list').change(function () {

            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var selected = $('#'+dependent).data('selected');
            var route = $('#'+dependent).data('route');

            $('#'+dependent).html('');

            if (value !== '') {
                $.ajax({
                    url: route,
                    method: "POST",
                    data: {value:value, _token:_token, selected:selected}
                }).done(function(data) {
                    $('#'+dependent).html(data).val();
                });
            }
        }).change();
    </script>
@endpush