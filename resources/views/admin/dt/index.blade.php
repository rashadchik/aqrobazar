@extends ('layouts.admin', ['table' => $route])
@section ('title', $title)

@section ('content')

    <style>
        img{max-width:200px}
    </style>
    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['modal' => $modal, 'largeModal' => $largeModal, 'createRoute' => $createRoute, 'route' => $route, 'filters' => $filters ])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => $route, 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    <script src="{{ asset('vendor/dataTables/buttons.server-side.js') }}"></script>

    {!! $dataTable->scripts() !!}

    @if(isset($reOrderRoute))
        <script>
            window.LaravelDataTables['{{ $route }}'].on( 'row-reorder', function ( e, diff ) {

                $.ajax({
                    url: "{{ route('slider.reorder') }}",
                    type: 'POST',
                    data: JSON.stringify(diff),
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                        'content-type': 'application/json'
                    }
                })
                .done(function() {
                    $('#sliders').DataTable().draw(false);

                })
                .fail(function() {
                    alert('Error !');
                });
            });

        </script>
    @endif

@endpush