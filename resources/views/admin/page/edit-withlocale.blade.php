@extends('layouts.modal', ['editor' => $editor, 'script' => $script ?? true, 'route' => null, 'locale' => true])
@section('title', $info->name)

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a href="#pageParam" data-toggle="tab"> Parametrlər</a></li>
        @foreach(config('app.locales') as $key => $locale)
            <li><a href="#{{$key}}" data-toggle="tab"> {{ $key }}</a></li>
        @endforeach
    </ul>
    <br>
    <div class="tab-content">

        <div id="pageParam" class="tab-pane in active">
            {!! Form::open(['url'=>route("$route.update", $info->id), 'method'=>'PUT', 'class' => 'form-horizontal dtForm']) !!}

                {!! $parameters !!}

                @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

            {!! Form::close() !!}
        </div>

        @foreach($relatedPage as $relPage)
            <div id="{{ $relPage->lang }}" class="tab-pane">
                {!! Form::open(['url'=>route("$route.updateTrans", $relPage->id), 'method'=>'PUT', 'class' => 'form-horizontal dtForm']) !!}

                    {!! $fields[$relPage->lang] !!}
                    {!! Form::hidden('lang', $relPage->lang) !!}

                    @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

                {!! Form::close() !!}
            </div>
        @endforeach

        @foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale)

            <div id="{{$key}}" class="tab-pane">

                {!! Form::open(['url'=>route($route.'.storeTrans'), 'method'=>'POST', 'class' => 'form-horizontal dtForm']) !!}

                    {!! $fields[$key] !!}
                    {!! Form::hidden('lang', $key) !!}
                    @if($route == 'page')
                        {!! Form::hidden('page_id', $info->id) !!}
                    @else
                        {!! Form::hidden('article_id', $info->id) !!}
                    @endif


                    @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'del' => true, 'route' => $route ])

                {!! Form::close() !!}

            </div>

        @endforeach
    </div>
@endsection

@push('scripts')
    <script>

        @foreach(config('app.locales') as $key => $locale)

            @if($editor == true)
                CKEDITOR.replace('editor{{ $key }}');
            @endif

            @if(isset($keys))
                $('#meta_keywords{{$key}}').select2({
                    data: {!! json_encode($keys[$key]) !!},
                    tags: true,
                    maximumSelectionLength: 10

                }).on("change", function(e) {
                    var isNew = $(this).find('[data-select2-tag="true"]');
                    if(isNew.length){
                        isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
                    }
                });

                $('#meta_keywords{{$key}}').val({!! json_encode($keys[$key]) !!}).trigger('change');
            @endif
        @endforeach

    </script>
@endpush