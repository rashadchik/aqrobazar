@extends ('layouts.admin', ['table' => 'products'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')
    @include('widgets.edit-sales')

    @component('admin.components.dt', ['create' => 'product.create', 'route' => 'product', 'filter' => config('config.filter-type-2'), 'largeModal' => true, 'locale' => true, 'tags' => \App\Models\Tag::groupBy('keyword')->pluck('keyword', 'keyword')])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'products', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    <link href="{{ asset('css/admin/bootstrap-editable.css') }}" rel="stylesheet">

    <script src="{{ asset('vendor/dataTables/dataTables.buttons.edit-sale.js') }}?v=2"></script>
    <script src="{{ asset('vendor/dataTables/buttons.server-side.js') }}"></script>

    <script>

        $('#filter-tag').on('change', function(e) {
            $('#products').DataTable().draw(false);
            e.preventDefault();
        });

        $("#products").on('preXhr.dt', function(e, settings, data) {
            data.tag = $('#filter-tag').find("option:selected").val();
        });
    </script>

    {!! $dataTable->scripts() !!}
@endpush