@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'videos.create', 'route' => 'videos'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'videos', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush