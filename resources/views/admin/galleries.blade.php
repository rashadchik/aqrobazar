@extends ('layouts.admin')
@section ('title', $title)

@section('content')

    <style>
        img{max-width:200px}
    </style>
    @include('widgets.modal-confirm')
    <h3 class="text-center">{{ $data->trans->name ?? $data->trans->title ?? $data->name }}</h3>
    <h5 class="text-center">Yükləmək üçün aşağıdakı qutuya click edin.</h5>
    @include('admin.gallery.dropzone')

    {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'galleries', 'width' => '100%']) !!}

@endsection

@push('scripts')
    <script src="{{ asset("js/dropzone.js") }}"></script>

    @include('widgets.dropzone', ['maxFile' => 10, 'data' => $data])

    {!! $dataTable->scripts() !!}
@endpush