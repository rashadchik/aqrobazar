@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @component('admin.components.dt', ['create' => 'post-terminal.create', 'route' => 'post-terminal'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'post-terminals', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush