{!! Form::open(['url' => route('gallery.store', [$route, $data->id]), 'method'=>'POST', 'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}

<div class="dz-default dz-message">

</div>

<div class="fallback">
    <input name="file" type="file" multiple />
</div>

<div class="dropzone-previews" id="dropzonePreview"></div>

{!! Form::hidden('csrf-token', csrf_token(), ['id' => 'csrf-token']) !!}

{!! Form::close() !!}