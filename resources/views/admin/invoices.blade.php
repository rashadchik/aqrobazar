@extends ('layouts.admin', ['table' => 'invoices'])
@section ('title', $title)

@section('content')

    @component('admin.components.dt')
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'invoices', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush