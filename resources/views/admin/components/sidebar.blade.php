<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <div class="user-panel">
        <div class="pull-left image">
            @include('widgets.profile-picture', ['class' => 'img-circle'])
        </div>
        <div class="pull-left info">
            <p>{{ auth()->guard('admin')->user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..." autocomplete="off">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div>
    </form>
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
        <li {{ activeUrl(route('admin.dashboard')) }}><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a></li>
        <li {{ activeUrl(route('admins.index')) }}><a href="{{ route('admins.index') }}"><i class="fa fa-user-plus fa-fw"></i> <span>Adminlər</span></a></li>
        <li class="treeview">
            <a href="#"><i class="fa fa-file fa-fw"></i> <span>Səhifələr</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('page.index')) }}><a href="{{ route('page.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Səhifələr</span></a></li>
                <li {{ activeUrl(route('page.order')) }}><a href="{{ route('page.order') }}?lang=az"><i class="fa fa-circle-o fa-fw"></i> <span>Ardıcıllıq</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#"><i class="fa fa-location-arrow fa-fw"></i> <span>Geolokasiya</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    <small class="label pull-right bg-green">yeni</small>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('country.index')) }}><a href="{{ route('country.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Ölkələr</span></a></li>
                <li {{ activeUrl(route('city.index')) }}><a href="{{ route('city.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Şəhərlər</span></a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#"><i class="fa fa-tag fa-fw"></i> <span>Elanlar</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('product.index')) }}><a href="{{ route('product.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Elanlar</span></a></li>
                <li {{ activeUrl(route('category.index')) }}><a href="{{ route('category.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Kateqoriyalar</span></a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#"><i class="fa fa-building fa-fw"></i> <span>Mağazalar</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('store.index')) }}><a href="{{ route('store.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Mağazalar</span></a></li>
            </ul>
        </li>

        <li {{ activeUrl(route('user.index')) }}>
            <a href="{{ route('user.index') }}"><i class="fa fa-users fa-fw"></i> <span>İstifadəçilər</span></a>
        </li>

        <li class="treeview">
            <a href="#"><i class="fa fa-dollar fa-fw"></i> <span>Qaimələr</span></a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('invoice.index')) }}><a href="{{ route('invoice.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Qaimələr</span></a></li>
                <li {{ activeUrl(route('price.index')) }}><a href="{{ route('price.index') }}"><i class="fa fa-circle-o fa-fw"></i><span>Qiymətlər</span></a></li>
            </ul>
        </li>

        <li {{ activeUrl(route('banner.index')) }}><a href="{{ route('banner.index') }}"><i class="fa fa-image fa-fw"></i> <span>Bannerlər</span></a></li>
        <li {{ activeUrl(route('articles.index')) }}><a href="{{ route('articles.index') }}"><i class="fa fa-newspaper-o fa-fw"></i> <span>Xəbərlər</span></a></li>
        <li {{ activeUrl(route('faq.index')) }}><a href="{{ route('faq.index') }}"><i class="fa fa-question-circle fa-fw"></i> <span>FAQ</span></a></li>
        <li {{ activeUrl(route('legislation.index')) }}><a href="{{ route('legislation.index') }}"><i class="fa fa-file-pdf-o fa-fw"></i> <span>Qanunvericilik</span></a></li>
        <li {{ activeUrl(route('dictionary.index')) }}><a href="{{ route ('dictionary.index') }}"><i class="fa fa-text-height fa-fw"></i> <span>Lüğət</span></a></li>

        <li class="treeview">
            <a href="#"><i class="fa fa-cogs fa-fw"></i> <span>Advanced</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(url('api/filemanager')) }}><a href="{{ url('api/filemanager') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>File Manager</span></a></li>
                <li {{ activeUrl(route('config.index')) }}><a href="{{ route('config.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Konfiqurasiya</span></a></li>
                <li {{ activeUrl(route('analytic.index')) }}><a href="{{ route('analytic.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Google Analytics</span></a></li>
                <li {{ activeUrl(route('sitemap.index')) }}><a href="{{ route('sitemap.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Sitemap</span></a></li>
            </ul>
        </li>
    </ul>
    <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->