<div class="login-logo">
    <a href="{{ url('/') }}">
        <img src="{{ asset('images/logo.svg') }}" style="background: #19814a">
    </a>
</div>
@if (session()->has('status'))
    <div class="alert alert-success">
        {{ session()->get('status') }}
    </div>
@endif