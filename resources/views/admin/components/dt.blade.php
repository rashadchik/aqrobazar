<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                @if(isset($tabs) && is_array($tabs))
                    <ul class="nav nav-pills pull-left">
                    </ul>
                @endif

                <div class="col-md-12">
                    <div class="row">
                        @if(isset($createRoute))
                            <div class="pull-right">
                                @if($modal == true)
                                    <button class="btn btn-success btn-block btn-flat open-modal-dialog" data-link="{{ route($createRoute) }}" data-large="{{ $largeModal }}">
                                        <span class="fa fa-plus"></span> {{ trans('locale.create') }}
                                    </button>
                                @else
                                    <a class="btn btn-success btn-sm pull-right" href="{{ route($createRoute) }}" style="color:#fff">
                                        <span class="fa fa-plus"></span> {{ trans('locale.create') }}
                                    </a>
                                @endif
                            </div>
                        @endif

                        @if(isset($filters))
                            <div class="pull-right">
                                <form class="form-inline pull-right" style="margin:0 15px 15px 0" id="search-box">

                                    @if(in_array('deleted', $filters))
                                        <div class="form-group">
                                            {!! Form::select('deleted', config('config.filter-status'), 1, ['id' => 'filter-deleted', 'class' => 'form-control']) !!}
                                        </div>
                                    @endif

                                    @if(in_array('lang', $filters))
                                        <div class="form-group">
                                            {!! Form::label('lang', 'Dil', ['class' => 'control-label']) !!}
                                            {!! Form::select('lang', config('app.locales'), 0, ['id' => 'filter-lang', 'class' => 'form-control']) !!}
                                        </div>
                                    @endif

                                    @if(array_key_exists('custom', $filters))
                                        @foreach($filters['custom'] as $custom)
                                            <div class="form-group">
                                                @if(isset($custom['title']))
                                                    {!! Form::label($custom['name'], $custom['title'], ['class' => 'control-label']) !!}
                                                @endif
                                                {!! Form::select($custom['name'], $custom['data'], $custom['selected'], ['id' => 'filter-'.$custom['name'], 'class' => 'form-control']) !!}
                                            </div>
                                        @endforeach
                                    @endif
                                    @if(in_array('reset', $filters))
                                        <button type="reset" class="btn btn-primary-outline"><span class="fa fa-remove" style="color:#337ab7"></span> </button>
                                    @endif
                                </form>
                            </div>
                        @endif
                    </div>
                </div>

                @if(isset($createRoute))
                    <div class="clearfix"></div>
                @endif

                <div class="tab-content">
                    <div class="tab-pane active"><br>
                        {{ $table }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')

    @if(isset($filters) && array_key_exists('custom', $filters))
        @foreach($filters['custom'] as $custom)
            <script>
                $('#filter-{{ $custom['name'] }}').on('change', function(e) {
                    $('#{{ $route }}').DataTable().draw(false);
                    e.preventDefault();
                });

                $('#{{ $route }}').on('preXhr.dt', function(e, settings, data) {
                    data.{{ $custom['name'] }} = $('#filter-{{ $custom['name'] }}').find("option:selected").val();
                });
            </script>
        @endforeach
    @endif

    <script>
        @if(isset($filters))
            $(document).on("click", "button[type='reset']", function(){
                $('#search-box')[0].reset();
                $('#{{ $route }}').DataTable().clear().draw();
            });
        @endif


        $('#filter-lang').on('change', function(e) {
            $('#{{ $route }}').DataTable().draw(false);
            e.preventDefault();
        });

        $("#{{ $route }}").on('preXhr.dt', function(e, settings, data) {
            data.lang = $('#filter-lang').find("option:selected").val();
        });


        $('#filter-deleted').on('change', function(e) {
            $('#{{ $route }}').DataTable().draw(false);
            e.preventDefault();
        });

        $("#{{ $route }}").on('preXhr.dt', function(e, settings, data) {
            data.deleted = $('#filter-deleted').find("option:selected").val();
        });

    </script>
@endpush