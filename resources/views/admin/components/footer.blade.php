<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Developer: <a href="//aghayev.net" target="_blank">Rəşad Ağayev</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} <a href="#">{{ getConfig()['name'] }}</a></strong>
</footer>