@extends ('layouts.admin', ['table' => 'articles'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'articles.create', 'route' => 'articles', 'filter' => config('config.filter-type-2'), 'largeModal' => true, 'locale' => true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'articles', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush