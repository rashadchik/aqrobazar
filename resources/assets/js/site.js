var loadingIcon = "<i class='fa fa-circle-o-notch fa-spin text-center'></i>";
var requestSent = false;

$(window).load(function() {
    //EqualHeights
    eqHeight(".eq", 480);
    eqHeight(".neq", 992);
    eqHeight(".beq", 992);
    eqHeight(".category_list .box", 10);
    eqHeight(".accounth", 992);
    eqHeight(".news_list article", 480);
    eqHeight(".product_list .box", 480);
    eqHeight(".company_values .box", 767);
    eqHeight(".infrastructure_boxs .box", 767);
    eqHeight(".carousel-stage li", 480);
    set_bg_banner_height();
});

$(function(){

    //only number
    $('.price').on('keydown', '.price-input', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

    // scrollTop init
    var win = $(window);
    var totop = $('#scrollUp');
    win.on('scroll', function() {
        if (win.scrollTop() > 150) {
            totop.fadeIn();
        } else {
            totop.fadeOut();
        }
    });
    totop.on('click', function() {
        $("html,body").animate({
            scrollTop: 0
        }, 500)
    });

    //MaskInput
    $.mask.definitions['9'] = '';
    $.mask.definitions['d'] = '[0-9]';
    $(".maskphone").mask("+(994 dd) ddd dd dd");

    //MobileCategory
    $(".btn_menu").click(function() {
        var type = $(this).attr("data-type");
        $("html").stop(true, true).addClass("noscroll");
        $("body").stop(true, true).addClass("noscroll");
        $(".mobile_category").stop(true, true).addClass("show");
        return false;
    });
    $(".mobile_category .close_panel").click(function(){
        $("html").stop(true, true).removeClass("noscroll");
        $("body").stop(true, true).removeClass("noscroll");
        $(".mobile_category").stop(true, true).removeClass("show");
        return false;
    });

    $(".mobile_category_body li.submenu>a").on("click", function(){
        if($(this).hasClass("active")){
            $(this).stop(true, true).removeClass("active");
            $(this).siblings("ul").stop(true, true).slideUp();
        }
        else{
            $(".mobile_category_body li a").stop(true, true).removeClass("active");
            $(this).stop(true, true).addClass("active");
            $(".mobile_category_body li ul").stop(true, true).slideUp();
            $(this).siblings("ul").stop(true, true).slideDown();
        }
        return false
    });

    $(".mobile_category_body li.submenu .sub>a").on("click", function(){
        if($(this).hasClass("active")){
            $(this).stop(true, true).removeClass("active");
            $(this).siblings("ul").stop(true, true).slideUp();
        }
        else{
            $(".mobile_category_body li.sub a").stop(true, true).removeClass("active");
            $(this).stop(true, true).addClass("active");
            $(".mobile_category_body li.sub ul").stop(true, true).slideUp();
            $(this).siblings("ul").stop(true, true).slideDown();
        }
        return false
    });

    //Popup
    $(".open_lr").click(function() {
        var type = $(this).attr("data-type");
        $("body").stop(true, true).addClass("noscroll");
        $(".popup").stop(true, true).addClass("show");
        $(".popup_inner").stop(true, true).removeClass("show");
        $(".popup_inner[data-id="+type+"]").stop(true, true).addClass("show");
        return false;
    });
    $(".popup .btn_close").click(function(){
        $("body").stop(true, true).removeClass("noscroll");
        $(".popup").stop(true, true).removeClass("show");
        $(".popup_inner").stop(true, true).removeClass("show");
        return false;
    });

    //CategoryList
    $(".category_list .box>a").click(function(){
        var item = $(this);
        if($(this).hasClass("active")){
            $(this).stop(true, true).removeClass("active");
            $(this).siblings(".subblock").stop(true, true).slideUp();
        } else {
            $(".category_list .box>a").stop(true, true).removeClass("active");
            $(".category_list .box .subblock").stop(true, true).slideUp();
            $(this).stop(true, true).addClass("active");
            $(this).siblings(".subblock").stop(true, true).slideDown().css({"top":item.position().top + 130});
        }
        return false
    });

    $(document).click(function(e){
        if (!$(e.target).is(".category_list .box*, .category_list .box")) {
            $(".category_list .box .subblock").stop(true, true).slideUp();
            $(".category_list .box>a").stop(true, true).removeClass("active");
        }
    });

    //NewsGallery
    $("#newsgallery").owlCarousel({
        dots: false,
        nav: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: true,
        margin: 0,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        smartSpeed: 450,
        responsive : {
            // breakpoint from 0 up
            0 : {
                items: 1
            },
            // breakpoint from 480 up
            480 : {
                items: 2,
                margin: 10
            },
            // breakpoint from 768 up
            768 : {
                margin: 30,
                items: 3
            },
            // breakpoint from 992 up
            992 : {
                margin: 30,
                items: 3
            },
            // breakpoint from 1200 up
            1200 : {
                margin: 30,
                items: 3
            }
        }
    });


    //Gallery
    if( $(".zoom-photo").length ){
        $(".zoom-photo").magnificPopup({
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {
                enabled: true,
                navigateByImgClick: false,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            }
        });
    }

    //ImgSvg
    $('img.svg').each(function() {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Remove any invalid XML tags
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and apply on SVG
            $.each(attributes, function() {
                $svg.attr(this.name, this.value);
            });

            // Replace IMG with SVG
            $img.replaceWith($svg);
        }, 'xml');
    });

    //FAQ
    $(".faq .head").on("click", function(){
        if($(this).hasClass("active")){
            $(this).stop().removeClass("active");
            $(this).siblings(".body").stop().slideUp();
        }
        else{
            $(".faq .head").stop().removeClass("active");
            $(this).stop().addClass("active");
            $(".faq .body").stop().slideUp();
            $(this).siblings(".body").stop().slideDown();
        }
    });

    // SubMenuTab
    $(".subblock_head a:first").addClass("active");
    $(".subblock_head a").click(function(){
        var index = $(this).index();
        $(this).siblings("a").removeClass("active");
        $(this).addClass("active");
        $(this).parent().siblings(".inner").children(".tabblock").hide();
        $(this).parent().siblings(".inner").children(".tabblock:eq("+index+")").fadeIn();
        return false;
    });

    //for store page header
    $('.our_ads').owlCarousel({
        margin:20,
        responsiveClass:true,
        nav:true,
        loop: true,
        dots:false,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        responsive:{
            0:{
                items:4,
            }
        }
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > $('#main_wrapper').offset().top) {
            $(".bg_banner").addClass("fixed");
            $("a.gotop").addClass("fixed");
        } else {
            $(".bg_banner").removeClass("fixed");
            $("a.gotop").removeClass("fixed");
        }
    });
    $("a.gotop").click(function () {
        $('html, body').animate({scrollTop:0}, '200');
    });
    $(window).resize(function () {
        set_bg_banner_height();
    });
});
function set_bg_banner_height(){
    $(".bg_banner").height($('body').outerHeight() - $('#header').offset().top - $('#header').outerHeight());
    $(".bg_banner").css('top',$('#main_wrapper').offset().top);
    $('body').css('padding-bottom',$('#footer').outerHeight() + 20);
}
function eqHeight(param, sizes) {
    if ($(param).length){
        var resizeTimer;
        $( window ).resize(function() {
            var width = $(window).width();
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(func, 500);
        });
        func()
    }

    function func(){
        var width = $(window).width();
        $(param).removeAttr('style');
        if (width < sizes){
            $(param).height("auto");
        }else{
            $(param).equalHeights();
        }
    }
}


$(document).ready(function(){
    $("body").on('click', '.add_favorite', function (event){

        event.preventDefault();
        if(requestSent) {return;}

        var product = $(this).data('id');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var _this = $(this);

        $.ajax({
            url:  '/bookmarks',
            type: 'POST',
            timeout: 60000,
            headers: { 'X-CSRF-Token' : csrf_token},
            data: { product: product, _token: csrf_token},
            dataType: "json",
            beforeSend: function(response) {
                //
            }
        })
            .done(function(response) {
                requestSent = false;

                if(response.code == 200){

                    if(response.type == 1){
                        _this.addClass('active');
                    }
                    else{
                        _this.removeClass('active');
                        _this.closest('div.bookmark').hide('slow');
                    }

                    _this.find('span').html(response.spanMsg);

                    toastr.success(response.msg);
                }
                else{
                    toastr.error(response.msg);
                }

            })
            .fail(function(xhr, textStatus) {
                requestSent = false;
                toastr.error('Error');
            });
    });
});


$("#subscribe-form").on("submit",function (event){
    event.preventDefault();
    subscribe($(this));
});

function contactForm(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,
            beforeSend: function(response) {
                $("#loadingButton").button('loading');
            }
        })
        .done(function(response) {
            $("#loadingButton").button('reset');
            requestSent = false;
            $('button[type=submit]').attr('disabled',true);

            action.data('submitted', true);
            toastr.success(response.msg);

        })
        .fail(function(xhr, ajaxOptions, thrownError) {
            $("#loadingButton").button('reset');
            requestSent = false;

            var json = JSON.parse(xhr.responseText);
            toastr.error(json.msg);
        });
    }
}

function subscribe(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            data: action.serialize(),
            dataType: "json",

            beforeSend: function(response) {
                $("#loadingSubButton").button('loading');
            },

            success: function(response){

                $("#loadingSubButton").button('reset');

                if(response.success == 1) {

                    $('#subscribe-form')[0].reset();
                }

                $("#subscribeBox").html(response.msg);
            },

            error: function(res){
                $("#loadingSubButton").button('reset');
                $("#subscribeBox").html('<p>Unexpected Error!</p>');
            }
        });
    }
}


$("body").on('click', '.open-modal-dialog', function (e){
    e.preventDefault();
    if(requestSent) {return;}
    modal($(this), $(this).attr('data-link'));
});

function modal(data, route) {
    requestSent = true;

    $('#myModal').modal('show');

    if(data.attr('data-large') == true){
        $('.modal-dialog').addClass("modal-lg");
    }
    else{
        $('.modal-dialog').removeClass( "modal-lg" );
    }

    $.ajax({
        url: route,
        type: "GET",
        timeout: 20000,

        beforeSend: function() {
            $("#modal-body").html(loadingIcon);
        },
        success: function(data){
            requestSent = false;
            $("#modal-body").html(data);
        },
        error: function() {
            requestSent = false;
            $("#modal-body").html('<h3>Unexpected Error!</h3>');
        }
    });
}

$(".warning-modal-open").click(function() {
    $('.warning-modal').attr('action', $(this).data('action'));
});


function loadMore(page, and){
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height() / 1.5 && !requestSent) {
            page++;
            loadMoreData(page, and);
        }
    });
}


function loadMoreData(page, and){

    requestSent = true;

    var newUrl = 'page=' + page;

    if(and === false){
        newUrl = document.location.href + '?' + newUrl;
    }
    else{
        newUrl = document.location.href + '&' + newUrl;
    }

    $.ajax(
        {
            url: newUrl,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-loading').show();
            }
        })
        .done(function(data)
        {
            $('.ajax-loading').hide();


            if(data.html == ""){
                requestSent = true;
                return;
            }
            else{
                $("#post").append(data.html);
                eqHeight(".product_list .box", 480);
            }

            requestSent = false;

        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                $('.ajax-loading').html("Daxili xəta baş verdi");
                requestSent = false;
            }
        );
}


$(".submit-form").on("submit",function (event){
    event.preventDefault();

    var btn = $(this).find($(".loadingButton"));

    if ($(this).data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  $(this).attr('action'),
            type: $(this).attr('method'),
            //data: action.serialize(),
            data: new FormData($(this)[0]),
            dataType: "json",
            processData: false,
            contentType: false,
            beforeSend: function(response) {
                btn.button('loading');
            }
        })
            .done(function(response) {
                requestSent = false;


                btn.button('reset');

                setTimeout(function() {
                    btn.prop('disabled',true);
                    $(this).data('submitted', true);
                }, 200);

                if(response.type === 'event') {
                    $('#event').modal("toggle");
                }
                else if(response.type === 'apply'){
                    $('#apply').modal("toggle");
                }

                toastr.success(response.msg);

            })
            .fail(function(xhr, ajaxOptions, thrownError) {
                btn.button('reset');
                requestSent = false;

                var json = JSON.parse(xhr.responseText);
                toastr.error(json.msg);
            });
    }
});