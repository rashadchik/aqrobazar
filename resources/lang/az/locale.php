<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 5/11/18
 * Time: 19:40
 */

return [
    "config" => [
        'sex' => ['Qadın', 'Kişi'],
        "minDates" => ['Baz', 'B.e', 'Ç.a', 'Çər', 'C.a', 'Cüm', 'Şən'],
        "dates" => ['Bazar günü', 'Bazar ertəsi', 'Çərşənbə axşamı', 'Çərşənbə', 'Cümə axşamı', 'Cümə', 'Şənbə'],
        "months" => [1 => 'Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'İyun', 'İyul', 'Avqust', 'Sentyabr', 'Oktyabr', 'Noyabr', 'Dekabr'],
        "minMonths" => ['Yan', 'Fev', 'Mar', 'Apr', 'May', 'İyn', 'İyl', 'Avq', 'Sen', 'Okt', 'Noy', 'Dek'],
        "eventDates" => ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'],
        'subCopy' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n". 'into your web browser: [:actionURL](:actionURL)',
    ],
    'ad-type' => [1 => 'Satılır', 'İcarəyə verilir', 'Axtarılır', 'Təklif olunur'],
    'forgot_password' => 'Şifrəni Unutdun?',
    'product-status' => [
        1 => ['title' =>'Aktiv', 'route' => 'published'],
        0 => ['title' =>'Yoxlanılır', 'route' => 'pending'],
        //2 => ['title' =>'Müddəti bitmiş', 'route' => 'expired'],
        3 => ['title' =>'Dərc olunmamış', 'route' => 'rejected'],
    ],
    'price-list' => [1 => 'Elanı irəli çək', 'Elanı VIP et' /*'Premium et'*/]
];
