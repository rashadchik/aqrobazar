<?php

return [
    'status' => ['Gizli', 'Aktiv'],
    'alert' => ['danger', 'success'],
    "slider-type" => [1 => "Slider", "Kompaniya"],
    "article-status" => ["Qaralama", "Dərc olunub"],
    "label" => ["danger", "success"],
    "menu-target" => [1 => "Self", 0 => "Blank"],
    "menu-visibility" => [0 => "Hidden", "Header and Footer", "Header", "Footer"],
    "menu-visibility-boolean" => [0 => "Gizli", "Aktiv"],
    "menu-visibility-label" => [0 => "danger", "success", "warning", "warning", "info"],
    "role" => [1 => "Administrator", "Editor"],
    "blog_page" => [0 => "No", "Yes"],
    "social-network" => ['facebook', 'twitter', 'instagram', 'youtube', 'linkedin'],
    "subscriber-label" => ["0" => "warning", "1" => "success"],
    "subscriber-status" => ["0" => "pending", "1" => "active"],
    "yes-no" => [0 => "minus", "plus"],
    'prefix-number' => [50 => 50, 51 => 51, 55 => 55, 70 => 70, 77 => 77],
    'slug_replacement' => ['az' => 'azerbaijani', 'en' => 'azerbaijani', 'ru' => 'russian'],
    "model" => [
        'articles' => 'App\Models\Article',
        'page' => 'App\Models\Page',
        'pageTranslation' => 'App\Models\PageTranslation',
        'articleTranslation' => 'App\Models\ArticleTranslation',
        'product' => \App\Models\Product::class,
    ],
    "template" => ['Sadə səhifə', 'Haqqımızda', 3 => 'Əlaqə', 'Xəbərlər', 6 => 'FAQ', 'Qanunvericilik'],
    'sex' => ['male' => 1, 'female' => 0], //facebook auth
    'payment-method-class' => [1 => 'row-item card', 'row-item', 'row-item-other clearfix'],
    'filter-status' => ['Bütün', 'Aktiv', 'Silinən'],
    'sort-product' => [1 => 'Yenidən köhnəyə', 'Köhnədən Yeniyə', 'Ucuzdan Bahaya', 'Bahadan Ucuza'],
    'price-route' => [1 => 'favourite', 'vip' /*'premium'*/],
    'payment-code' => [
        -1 => 'Naməlum əməliyyat',
        0 => 'Uğurlu əməliyyat',
        1 => 'Uğursuz əməliyyat',
        2 => 'Əməliyyat sona çatdırılmayıb',
        3 => 'Əməliyyat sona çatdırılmayıb',
        4 => 'Uğursuz əməliyyat',
        5 => 'Əməliyyat geri qaytarılıb',
        7 => 'Timeout',
        9 => 'Canceled by customer',
        10 => 'Əməliyyat geri qaytarılıb',
        11 => 'Əməliyyat sona çatdırılmayıb',
        12 => 'Əməliyyat sona çatdırılmayıb (3DS yoxlanışı attempt kimi keçmişdir)',
        13 => 'Əməliyyat sona çatdırılmayıb (OTP kod daxil edilməyib)'
    ],

    "banner-devices" => [1 => 'Desktop', 'Mobile'],
    "banner-type" => [
        1 => 'Üst Banner',
        2 => 'Üst Banner Sağ (Desktop)',
        4 => 'Sol (Desktop)',
        5 => 'Sağ (Desktop)',
        6 => 'Sağ + sol (Desktop)'
    ],

    "prefix" => ['aqrobazar' => 1, 'mobile' => 2],
];
