<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedSmallInteger('amount');
            $table->unsignedTinyInteger('type');
            $table->unsignedTinyInteger('card_type');
            $table->unsignedBigInteger('card_number')->nullable();
            $table->string('description');
            $table->string('payment_key', 40)->unique();
            $table->unsignedSmallInteger('code')->default(2);
            $table->timestamp('created_at');

            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
