<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('main_category_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('name');
            $table->string('slug')->unique()->nullable();
            $table->string('summary', 1000)->nullable();
            $table->boolean('status')->index()->default(0);
            $table->boolean('type')->index()->default(1);
            $table->unsignedInteger('seen')->default(0);
            $table->unsignedInteger('price')->default(0);
            $table->unsignedInteger('order')->unique()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('main_category_id')->references('id')->on('pages')->onDelete('set null');
            $table->foreign('category_id')->references('id')->on('pages')->onDelete('set null');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
