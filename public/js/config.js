$(".add_favorite").on("click", function (event){

    event.preventDefault();
    if(requestSent) {return;}

    var product = $(this).data('id');
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var _this = $(this);

    $.ajax({
        url:  '/bookmarks',
        type: 'POST',
        timeout: 60000,
        headers: { 'X-CSRF-Token' : csrf_token},
        data: { product: product, _token: csrf_token},
        dataType: "json",
        beforeSend: function(response) {
            //
        }
    })
        .done(function(response) {
            requestSent = false;

            if(response.code == 200){

                if(response.type == 1){
                    _this.addClass('active');
                }
                else{
                    _this.removeClass('active');
                    _this.closest('div.bookmark').hide('slow');
                }

                _this.find('span').html(response.spanMsg);

                toastr.success(response.msg);
            }
            else{
                toastr.error(response.msg);
            }

        })
        .fail(function(xhr, textStatus) {
            requestSent = false;
            toastr.error('Error');
        });
});